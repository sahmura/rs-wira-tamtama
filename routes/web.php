<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PbfController;
// use Illuminate\Routing\Route;

// use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('auth.login');
})->name('home');

// ROuting view
Route::view('/blank', 'blank');

Route::middleware(['auth', 'checkRole:system,admin,apoteker,rawatinap,ruangoperasi'])->group(function () {
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('dashboard/dataobat', 'DashboardController@dataObat')->name('dashboard.obat');
    // Route::get('dashboard/dataobat', 'DashboardController@dataObat')->name('dashboard.obat');
    Route::get('antrianobat', 'RequestController@antrianObat')->name('requestObat.admin');
    Route::post('updateantrianobat', 'RequestController@updateAdmin')->name('requestObat.updateadmin');
    // Route::get('pbfList', 'PbfController@pbfList')->name('pbf.listPbf');
});

Route::middleware(['auth', 'checkRole:system,admin'])->group(function () {
    Route::post('kategori/hapusdata/{id}', 'KategoriController@destroy');
    Route::resource('kategori', 'KategoriController');
    Route::post('dataobat/hapusdata/{id}', 'ObatController@destroy');
    Route::post('dataobat/fixdata', 'ObatController@fixdata');
    Route::resource('dataobat', 'ObatController');
    Route::post('dataobat/import', 'ObatController@importDataObat')->name('dataobat.importData');
    Route::post('dataobat/getData', 'ObatController@getData');
    Route::post('dataobat/pemasukan', 'ObatController@pemasukan')->name("dataobat.pemasukan");
    Route::post('dataobat/distribusi', 'ObatController@pengeluaran')->name("dataobat.distribusi");
    Route::post('obat/hapusdata/{id}', 'dataObatController@destroy');
    Route::resource('obat', 'dataObatController');
    Route::post('satuanobat/hapusdata/{id}', 'satuanObatController@destroy');
    Route::resource('satuanobat', 'satuanObatController');
    Route::post('pbf/hapusdata/{id}', 'PbfController@destroy');
    Route::resource('pbf', 'PbfController');
    Route::post('distribusi/hapusdata/{id}', 'DistribusiController@destroy');
    Route::post('distribusi/hapusdatadistribusi', 'DistribusiController@hapusdatadistribusi');
    Route::resource('distribusi', 'DistribusiController');
    Route::post('users/hapusdata/{id}', 'UsersController@destroy');
    Route::resource('users', 'UsersController');
    Route::get('cetak/antrianObatIndex', 'RequestController@cetakDistribusiIndex');
    Route::get('cetak/antrianObat', 'CetakController@cetakDistribusi');
    Route::post('request/batalkanProses', 'RequestController@batalkanProses');
});

Route::middleware(['auth', 'checkRole:system,apoteker,rawatinap,ruangoperasi'])->group(function () {
    Route::post('requestObat/hapusdata/{id}', 'RequestController@destroy');
    Route::resource('requestObat', 'RequestController');
    Route::get('apoteker/listDistribusi', 'ApotekersController@listDistribusi')->name('apoteker.listDistribusi');
    Route::post('apoteker/list', 'ApotekersController@listData')->name('apoteker.list');
    Route::post('apoteker/dataobat/{id}', 'ApotekersController@destroy');
    Route::resource('apoteker', 'ApotekersController');
    Route::post('getStokObat', 'RequestController@getStokObat');
    Route::group(['prefix' => 'stokopname'], function () {
        Route::get('', 'StokOpnameController@index');
        Route::get('toExcel', 'StokOpnameController@exportToExcel');
        Route::post('insertData', 'StokOpnameController@insertStokOpname');
        Route::post('getListHarga', 'StokOpnameController@getListHarga');
    });
    Route::group(['prefix' => 'groupData', 'middleware' => 'checkRole:ruangoperasi,system'], function () {
        Route::get('/', 'GroupController@index');
        Route::get('/add', 'GroupController@add');
        Route::get('/edit/{id}', 'GroupController@edit');
        Route::post('/getObat', 'GroupController@getObat');
        Route::post('/getData', 'GroupController@getData');
        Route::post('/addData', 'GroupController@addData');
        Route::post('/editData', 'GroupController@editData');
        Route::post('/deleteData', 'GroupController@deleteData');
    });
    Route::group(['prefix' => 'laporanpemakaianobat', 'middleware' => 'checkRole:ruangoperasi,system'], function () {
        Route::get('/', 'LaporanPemakaianController@index');
        Route::get('/add', 'LaporanPemakaianController@add');
        Route::get('/edit/{id}', 'LaporanPemakaianController@edit');
        Route::get('/detail/{id}', 'LaporanPemakaianController@detail');
        Route::get('/getObat', 'LaporanPemakaianController@getObat');
        Route::post('/getData', 'LaporanPemakaianController@getData');
        Route::post('/addData', 'LaporanPemakaianController@addData');
        Route::post('/editData', 'LaporanPemakaianController@editData');
        Route::post('/deleteData', 'LaporanPemakaianController@deleteData');
        Route::get('/cetakData', 'CetakController@cetakLaporanPemakaian');
        Route::post('/validasiData', 'LaporanPemakaianController@validasiData');
        Route::post('/batalkanValidasi', 'LaporanPemakaianController@batalkanValidasi');
    });
});

Auth::routes();
