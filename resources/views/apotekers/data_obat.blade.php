@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
@endsection


@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>List <strong>Obat tersedia</strong></h2>
                </div>
                <div class="card-body">
                    <div class="row d-flex align-items-end">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal_awal">Tanggal awal</label>
                                <input type="date" name="tanggal_awal" id="tanggal_awal" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal_akhir">Tanggal akhir</label>
                                <input type="date" name="tanggal_akhir" id="tanggal_akhir" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button id="filter-btn" class="btn btn-primary">Filter data</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-3">
                        <table id="pbfTable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama obat</td>
                                    <td>Ketersediaan</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="pbfModal" tabindex="-1" role="dialog" aria-labelledby="pbfModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="pbfModalLabel"><b>Tambah data</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="formPbf">
                    @csrf
                    <input type="hidden" id="type-action" name="type_action">
                    <input type="hidden" id="hidden_id" name="hidden_id">
                    <input type="hidden" id="_method" name="_method">
                    <div class="form-group">
                        <label for="obat_id">Pilih obat</label>
                        <select name="obat_id" id="obat_id" class="custom-select" aria-placeholder="Pilih obat" required>
                            @foreach($obat as $data)
                            <option value="{{ $data->dataPemasukan->id ?? 0}}">{{ $data->dataPemasukan->nama ?? 'error' }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="total">Total</label>
                        <input type="number" name="total" id="total" class="form-control" placeholder="Total obat" required>
                    </div>
                    <div class="form-group">
                        <label for="distribusi">Distribusi</label>
                        <select name="distribusi" id="distribusi" class="custom-select" aria-placeholder="Distribusi" required>
                            <option value="Rawat inap">Rawat inap</option>
                            <option value="Rawat jalan">Rawat jalan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan" required></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn" id="button-action">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script>
    function refresh_data() {
        var table = $('#pbfTable').DataTable({
            paginate: true,
            info: true,
            sort: true,
            destroy: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('apoteker/list') }}",
                method: 'POST',
                data: {
                    tanggal_awal: $('#tanggal_awal').val(),
                    tanggal_akhir: $('#tanggal_akhir').val(),
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'nama_obat',
                },
                {
                    data: 'stok',
                },
            ]
        });
    }
    $(document).ready(function() {
        refresh_data();

        $('.btn-tambah').on('click', function(e) {
            e.preventDefault();
            $('.modal-header').removeClass('bg-warning').addClass('bg-success');
            $('.modal-title').text('Tambah data');
            $('#type-action').val('add');
            $('#_method').val('POST');
            $('#button-action').text('Tambah data').removeClass('btn-warning').addClass('btn-success');
            $('#formPbf')[0].reset();
            $('#pbfModal').modal('show');
        });

        $(document).on('click', '.btn-edit', function() {
            var id = $(this).attr('id');
            var nama = $(this).data('nama');
            $('.modal-header').addClass('bg-warning');
            $('.modal-title').text('Sunting PBF');
            $('#type-action').val('edit');
            $('#hidden_id').val(id);
            $('#nama').val(nama);
            $('#_method').val('PUT');
            $('#button-action').text('Update PBF').addClass('btn-warning').removeClass('btn-success');
            $('#pbfModal').modal('show');
        })

        $('#formPbf').on('submit', function(e) {
            e.preventDefault();
            var method = '';
            var action = '';
            var id = $('#hidden_id').val();

            if ($('#type-action').val() == 'add') {
                action = '{{ route("apoteker.store") }}';
                method = 'POST';
                $.ajax({
                    url: action,
                    method: method,
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(response) {
                        if (response.status == true) {
                            swal.fire({
                                title: response.message,
                                icon: 'success',
                                // timer: 3000
                            });
                        } else {
                            swal.fire({
                                title: response.message,
                                icon: 'error',
                                // timer: 3000
                            });
                        }
                        table.ajax.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf')[0].reset();
                    },
                    fail: function(response) {
                        swal.fire({
                            title: response.message,
                            icon: 'error',
                            // timer: 3000
                        });
                        // location.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf')[0].reset();

                    },
                });
            }
            if ($('#type-action').val() == 'edit') {
                action = 'apoteker/' + id;
                method = 'POST';
                data = $(this).serialize();
                console.log(data);

                $.ajax({
                    url: action,
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(response) {
                        if (response.status == true) {
                            swal.fire({
                                title: response.message,
                                icon: 'success',
                                // timer: 3000
                            });
                        } else {
                            swal.fire({
                                title: response.message,
                                icon: 'error',
                                // timer: 3000
                            });
                        }
                        table.ajax.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf')[0].reset();
                    },
                    fail: function() {
                        swal.fire({
                            title: 'Gagal menyunting data',
                            icon: 'error',
                            timer: 3000
                        });
                        table.ajax.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf')[0].reset();

                    },
                });
            }
        });

        $(document).on('click', '.btn-delete', function() {
            var id = $(this).data('id');
            var token = $(this).data('token');

            swal.fire({
                title: 'Apakah anda yakin menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#e3342f',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                                'content')
                        },
                        url: '{{ url("apoteker/hapusdata") }}' + '/' + id,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id': id,
                            '_method': 'POST',
                            '_token': token,
                        },
                        success: function() {
                            swal.fire({
                                title: 'Data berhasil dihapus',
                                icon: 'success',
                                timer: 3000,
                            });
                            table.ajax.reload();
                        },
                        fail: function() {
                            swal.fire({
                                title: 'Data gagal dihapus',
                                icon: 'error',
                                timer: 3000,
                            });
                            table.ajax.reload();
                        },
                    });
                }
            });
        });

        $('#filter-btn').on('click', function() {
            refresh_data();
        });
    })
</script>
@endpush