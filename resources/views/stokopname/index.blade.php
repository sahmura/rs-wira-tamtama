@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Stok <strong>Opname</strong></h2>
                </div>
                <div class="card-body">
                    <div class="row d-flex align-items-end">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bulan">Filter Bulan</label>
                                <input type="month" name="bulan" id="bulan" value="{{ \Carbon\Carbon::now()->format('Y-m') }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button class="btn btn-primary" id="btn-filter">Filter Data</button>
                                <button class="btn btn-warning" id="btn-cetak">Cetak Data</button>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="form-group">
                                <button class="btn btn-success" id="btn-addopname">Tambah Stok Opname</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="pbfTable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama Matkes</td>
                                    <td>Bentuk Sediaan</td>
                                    <td>Harga (Rp)</td>
                                    <td>Stok Awal</td>
                                    <td>Penerimaan</td>
                                    <td>Pemakaian</td>
                                    <td>Persediaan Akhir</td>
                                    <td>Nominal Persediaan Akhir</td>
                                    <td>Nominal Pemakaian</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="stokOpnameModal" tabindex="-1" role="dialog" aria-labelledby="stokOpnameModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="stokOpnameModalTitle">Tambah Stok Opname</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" id="formopname">
                    @csrf
                    <div class="form-group">
                        <label for="obat">Nama Obat</label>
                        <select name="obat_id" id="obat_id" class="custom-select select2" style="width: 100%;" placeholder="Pilih Obat" required>
                            <option value="">Pilih Obat</option>
                            @foreach($dataObat as $stokAp)
                            <option value="{{ $stokAp->obat_id }}">{{ $stokAp->dataObat->nama ?? 'Data dihapus' }}</option>
                            @endforeach
                        </select>
                        <small>Obat yang tersedia hanya obat yang ada di stok apoteker, rawat inap, atau ruang operasi
                        </small>
                    </div>
                    <div class="form-group" id="harga_obat_akhir" style="display: none;">
                        <label for="harga_obat">Harga Obat</label>
                        <select name="harga_obat" id="harga_obat" class="custom-select" placeholder="Pilih Harga"></select>
                    </div>
                    <div class="form-group">
                        <label for="stok_awal">Stok Awal</label>
                        <input type="number" name="stok_awal" id="stok_awal" class="form-control" placeholder="Stok Awal" min="0" required>
                    </div>
                    <div class="form-group">
                        <label for="stokopname">Stok Opname</label>
                        <input type="number" name="stokopname" id="stokopname" class="form-control" placeholder="Stok Opname" min="0" required>
                    </div>
                    <div class="form-group">
                        <label for="periode">Periode</label>
                        <input type="month" name="periodeopname" id="periode" class="form-control" placeholder="Periode" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-saveopname">Simpan data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    function refreshdata() {
        $('#pbfTable').DataTable({
            paginate: true,
            destroy: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('stokopname') }}",
                data: {
                    bulan: $('#bulan').val()
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'namamatkes'
                },
                {
                    data: 'bentuk'
                },
                {
                    data: 'harga',
                    class: 'text-right'
                },
                {
                    data: 'stok_awal',
                    class: 'text-right'
                },
                {
                    data: 'penerimaan',
                    class: 'text-right'
                },
                {
                    data: 'pemakaian',
                    class: 'text-right'
                },
                {
                    data: 'persediaan_akhir',
                    class: 'text-right'
                },
                {
                    data: 'nominal_akhir',
                    class: 'text-right'
                },
                {
                    data: 'nominal_pemakaian',
                    class: 'text-right'
                },
            ]
        });
    }

    $(document).ready(function() {
        refreshdata();
        $('.select2').select2();
    });

    $('#btn-cetak').on('click', function() {
        var date = $('#bulan').val();
        window.open("{{ url('stokopname/toExcel') }}?bulan=" + date);
    });

    $('#btn-filter').on('click', function() {
        refreshdata();
    });

    $('#btn-addopname').on('click', function() {
        $('#formopname')[0].reset();
        $('#harga_obat_akhir').hide();
        $('#stokOpnameModal').modal('show');
    });

    $('#btn-saveopname').on('click', function() {
        var data = $('#formopname').serialize();
        $.ajax({
            url: "{{ url('stokopname/insertData') }}",
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            success: function(data) {
                swal.fire({
                    title: data.message,
                    icon: data.type
                }).then((Confirm) => {
                    window.location.reload();
                });
            }
        });
    });

    $('#obat_id').change(function() {
        $.ajax({
            url: "{{ url('stokopname/getListHarga') }}",
            data: {
                obat_id: $(this).val()
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            success: function(data) {
                $('#harga_obat').empty();
                $('#harga_obat_akhir').show();
                $.each(data.data, function(index, value) {
                    $('#harga_obat').append(new Option('Rp' + value.harga_akhir, value.id + '-' + value.harga_akhir));
                })
            }
        });
    })
</script>
@endpush