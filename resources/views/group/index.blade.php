@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Grouping <strong>Obat dan Alat</strong>
                        <a class='btn btn-success float-right' href="{{ url('groupData/add') }}">Tambah Group</a>
                    </h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataGroup" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td style="width: 20px;">No</td>
                                    <td>Nama Group</td>
                                    <td style="width: 30px;">Aksi</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    function refreshdata() {
        $('#dataGroup').DataTable({
            paginate: true,
            destroy: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('groupData') }}",
                method: 'GET'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'name'
                },
                {
                    data: 'action',
                    class: 'text-center'
                },
            ]
        });
    }

    $(document).ready(function() {
        refreshdata();
        $('.select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin menghapus data?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#e3342f',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{ url("groupData/deleteData") }}',
                    method: 'POST',
                    data: {
                        'id': id,
                    },
                    success: function(res) {
                        swal.fire({
                            title: res.title,
                            icon: res.icon,
                            text: res.text,
                            timer: 3000,
                        });
                        refreshdata();

                    },
                });
            }
        });
    });

    $(document).on('click', '.btn-edit', function() {
        var id = $(this).data('id');
        window.location.href = "{{ url('groupData/edit/') }}" + '/' + id;
    })
</script>
@endpush