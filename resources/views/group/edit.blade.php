@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Grouping <strong>Obat dan Alat</strong></h2>
                </div>
                <div class="card-body">
                    <form action="" id="dataform" method="POST">
                        <input type="hidden" value="{{ $data->id }}" name="id">
                        <div class="form-group">
                            <label for="name">Nama Group</label>
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Nama group" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label for="">Data Group</label>
                        </div>
                        @if($data->dataObat != null)
                        @foreach($data->dataObat as $list)
                        <div class="input-group mb-3">
                            <select name="obat[]" id="obat" class="select2" style="width: 94%;">
                                @foreach($obat as $data)
                                <option value="{{ $data->id }}" @if($data->id == $list->id) selected='selected' @endif>{{ $data->nama }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-danger btn-hapus-obat btn-sm" type="button" id="button-addon2">Hapus</button>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="obatlain"></div>
                        <div class="form-group">
                            <button id="btn-simpan-data" class="btn btn-success float-right" type="button">Simpan data</button>
                            <button id="btn-tambah-data" class="btn btn-primary float-right" type="button">Tambah data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    function refreshdata() {
        $('#dataGroup').DataTable({
            paginate: true,
            destroy: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('groupData') }}",
                method: 'GET'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'name'
                },
                {
                    data: 'action',
                    class: 'text-center'
                },
            ]
        });
    }

    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    $(document).ready(function() {
        refreshdata();
        $('.select2').select2();
        $('.select2').trigger('change');
    });

    $('#btn-tambah-data').on('click', function() {
        let id = makeid(5);
        let input = '<div class="input-group mb-3"><select name="obat[]" id="' +
            id +
            '" class="select-obat" style="width: 95% ;">' +
            @foreach($obat as $data)
        '<option value="{{ $data->id }}">{{ $data->nama }}</option>' +
        @endforeach
            '</select><div class="input-group-append"><button class="btn btn-danger btn-hapus-obat btn-sm" type="button" id="button-addon2">Hapus</button></div></div>';
        $('#obatlain').append(input);
        $('#' + id).select2();
    });

    $(document).on('click', '.btn-hapus-obat', function() {
        $(this).parent('div').parent('div').remove();
    });

    $('#btn-simpan-data').on('click', function() {
        swal.fire({
            title: 'Simpan data?',
            icon: 'warning',
            showCancelButton: true,
        }).then((Confirm) => {
            if (Confirm.value) {
                var data = $('#dataform').serialize();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{ url("groupData/editData") }}',
                    method: 'POST',
                    data: data,
                    success: function(response) {
                        swal.fire({
                            title: response.title,
                            text: response.text,
                            icon: response.icon,
                        }).then((Confirm) => {
                            if (response.status) {
                                window.location.href = "{{ url('groupData') }}";
                            } else {
                                window.location.reload();
                            }
                        });
                    },
                });
            }
        });
    })
</script>
@endpush