<style>
    table td {
        padding: 2px 4px;
        vertical-align: middle;
    }
</style>
<page backbottom="7mm" backtop="7mm">
    <page_footer>
        <table>
            <tr>
                <td>
                    <p>Dicetak tanggal: {{ $printDate }}, Oleh: {{ $printedBy }}</p>
                </td>
                <td style="width: 630px;"></td>
                <td>
                    <p>Halaman [[page_cu]] Dari [[page_nb]]</p>
                </td>
            </tr>
        </table>
    </page_footer>
    <table style="border: none; width: 100%">
        <tr>
            <td style="text-align: center; width: 100%"><b>JASA DAN BIAYA PEMAKAIAN OBAT</b></td>
        </tr>
        <tr>
            <td style="text-align: center; width: 100%"><b>DALAM TINDAKAN {{$tindakan}}</b></td>
        </tr>
    </table>
    <table style="border: none; margin-top: 2em">
        <tr>
            <td style="width: 10px;font-weight: 600">1.</td>
            <td style="width: 150px;font-weight: 600">Nama Pasien</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->nama ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">2.</td>
            <td style="width: 150px;font-weight: 600">Tanggal Lahir</td>
            <td style="width: 10px;">:</td>
            <td>{{ \Carbon\Carbon::parse($detail->tanggal_lahir)->format('d/m/Y') ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">3.</td>
            <td style="width: 150px;font-weight: 600">Jenis Kelamin</td>
            <td style="width: 10px;">:</td>
            <td>{{ ($detail->jenis_kelamin == 'P') ? 'Perempuan' : 'Laki-laki' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">4.</td>
            <td style="width: 150px;font-weight: 600">Usia</td>
            <td style="width: 10px;">:</td>
            <td>{{ \Carbon\Carbon::createFromDate($detail->tanggal_lahir)->age ?? '-' }} Tahun</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">5.</td>
            <td style="width: 150px;font-weight: 600">Status</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->status ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">6.</td>
            <td style="width: 150px;font-weight: 600">Pangkat / Pekerjaan</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->pangkat_pekerjaan ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">7.</td>
            <td style="width: 150px;font-weight: 600">Alamat</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->alamat ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">8.</td>
            <td style="width: 150px;font-weight: 600">Macam Operasi</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->macam_operasi ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">9.</td>
            <td style="width: 150px;font-weight: 600">Golongan Operasi</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->golongan_operasi ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">10.</td>
            <td style="width: 150px;font-weight: 600">Tanggal Operasi</td>
            <td style="width: 10px;">:</td>
            <td>{{ (isset($detail->tanggal_operasi)) ? \Carbon\Carbon::parse($detail->tanggal_operasi)->locale('id')->isoFormat('Do MMMM YYYY') : '-' }}</td>
        </tr>
    </table>
    <table style="margin-top: 2em">
        <tr>
            <td style="width: 10px;font-weight: 600"></td>
            <td style="width: 150px;font-weight: 600; text-decoration: underline">JASA OPERASI</td>
            <td style="width: 10px;"></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">1.</td>
            <td style="width: 150px;font-weight: 600">Dokter Bedah</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->dokter_bedah->nama ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">2.</td>
            <td style="width: 150px;font-weight: 600">Dokter Anestesi</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->dokter_anestesi->nama ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">3.</td>
            <td style="width: 150px;font-weight: 600">Dokter Spesialis Penunjang</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->dokter->nama ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">4.</td>
            <td style="width: 150px;font-weight: 600">Perawat Bedah</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->perawat_bedah ?? '-' }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">5.</td>
            <td style="width: 150px;font-weight: 600">Perawat Anestesi</td>
            <td style="width: 10px;">:</td>
            <td>{{ $detail->perawat_anestesi ?? '-' }}</td>
        </tr>
        <!-- <tr>
            <td style="width: 10px;font-weight: 600">6.</td>
            <td style="width: 150px;font-weight: 600">Sewa Alat</td>
            <td style="width: 10px;">:</td>
            <td>{{ ($detail->sewa_alat) ? number_format($detail->sewa_alat) : 0 }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">7.</td>
            <td style="width: 150px;font-weight: 600">Sewa OK</td>
            <td style="width: 10px;">:</td>
            <td>{{ ($detail->sewa_ok) ? number_format($detail->sewa_ok) : 0 }}</td>
        </tr>
        <tr>
            <td style="width: 10px;font-weight: 600">8.</td>
            <td style="width: 150px;font-weight: 600">Biaya Lain-lain</td>
            <td style="width: 10px;">:</td>
            <td>{{ number_format($detail->biaya_lain) ?? 0 }}</td>
        </tr> -->
    </table>
    <table cellpadding="0" cellspacing="0" style="width: 100%; margin-top: 20px">
        <tr>
            <td style="text-align: center; border: 1px solid black; width:4%;">NO</td>
            <td style="border: 1px solid black; width:27%; text-align: center;">NAMA</td>
            <td style="border: 1px solid black; width:14%; text-align: center;">JUMLAH</td>
            <td style="border: 1px solid black; width:14%; text-align: center;">HARGA OBAT<br>(Rp)</td>
            <td style="border: 1px solid black; width:14%; text-align: center;">SUB TOTAL</td>
            <td style="border: 1px solid black; width:14%; text-align: center;">JASA</td>
            <td style="border: 1px solid black; width:14%; text-align: center;">KET</td>
        </tr>

        @if($detail->tipe_pasien == 'BPJS')
        @php $persenan = 20/100; @endphp
        @elseif($detail->tipe_pasien == 'SWASTA')
        @php $persenan = 30/100; @endphp
        @else
        @php $persenan = 0; @endphp
        @endif

        @php
        $totalPemakaian = 0;
        $totalharga = 0;
        $num = 0;
        @endphp
        @foreach($data as $obat)
        <tr>
            @php
            $hargaObatDanPersenan = (float) str_replace('.', '', $obat['harga_satuan_terakhir']) + ((float) str_replace('.', '', $obat['harga_satuan_terakhir']) * $persenan);
            @endphp
            <td style="border: 1px solid black; width:3%; text-align: center">{{ $loop->iteration }}</td>
            <td style="border: 1px solid black; width:27%;">{{ $obat['nama'] }}</td>
            <td style="border: 1px solid black; width:14%;text-align: right">{{ $obat['pemakaian'] }}</td>
            <td style="border: 1px solid black; width:14%;text-align: right">{{ number_format($hargaObatDanPersenan) }}</td>
            <td style="border: 1px solid black; width:14%;text-align: right">{{ number_format((float) $obat['pemakaian'] * $hargaObatDanPersenan) }}</td>
            <td style="border: 1px solid black; width:14%;"></td>
            <td style="border: 1px solid black; width:14%;"></td>
        </tr>
        @php
        $totalPemakaian += $obat['pemakaian'];
        $totalharga += (float) $obat['pemakaian'] * $hargaObatDanPersenan;
        $num = $loop->iteration;
        @endphp
        @endforeach
        <tr>
            <td style="border: 1px solid black; text-align:center; font-weight: 600" colspan='2'><b>Jumlah</b></td>
            <td style="border: 1px solid black; text-align: right"><b>{{ $totalPemakaian }}</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"><b>{{ number_format($totalharga) }}</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 1 }}</td>
            <td style="border: 1px solid black;" colspan='4'>Dokter Bedah</td>
            <td style="border: 1px solid black; text-align: right">{{ ($detail->jasa_dokter_bedah) ? number_format($detail->jasa_dokter_bedah) : 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $jasa_dokter_bedah = ($detail->jasa_dokter_bedah) ? $detail->jasa_dokter_bedah : 0;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 2 }}</td>
            <td style="border: 1px solid black;" colspan='4'>Dokter Anestesi</td>
            <td style="border: 1px solid black; text-align: right">{{ ($detail->jasa_dokter_anestesi) ? number_format($detail->jasa_dokter_anestesi) : 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $jasa_dokter_anestesi = ($detail->jasa_dokter_anestesi) ? $detail->jasa_dokter_anestesi : 0;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 3 }}</td>
            <td style="border: 1px solid black;" colspan='4'>Dokter Spesialis</td>
            <td style="border: 1px solid black; text-align: right">{{ ($detail->jasa_dokter_spesialis) ? number_format($detail->jasa_dokter_spesialis) : 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $jasa_dokter_spesialis = ($detail->jasa_dokter_spesialis) ? $detail->jasa_dokter_spesialis : 0;
            $jasa_dokter = $jasa_dokter_anestesi + $jasa_dokter_bedah + $jasa_dokter_spesialis;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 4 }}</td>
            <td style="border: 1px solid black;" colspan='4'>Perawat Bedah</td>
            <td style="border: 1px solid black; text-align: right">{{ ($detail->jasa_perawat_bedah) ? number_format($detail->jasa_perawat_bedah) : 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $jasa_perawat_bedah = ($detail->jasa_perawat_bedah) ? $detail->jasa_perawat_bedah : 0;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 5 }}</td>
            <td style="border: 1px solid black;" colspan='4'>Perawat Anestesi</td>
            <td style="border: 1px solid black; text-align: right">{{ ($detail->jasa_perawat_anestesi) ? number_format($detail->jasa_perawat_anestesi) : 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $jasa_perawat_anestesi = ($detail->jasa_perawat_anestesi) ? $detail->jasa_perawat_anestesi : 0;
            $jasa_perawat = $jasa_perawat_anestesi + $jasa_perawat_bedah;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align:center; font-weight: 600" colspan='2'><b>Jumlah</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"><b>{{ number_format($jasa_dokter + $jasa_perawat) }}</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 3 }}</td>
            <td style="border: 1px solid black;" colspan='3'>Sewa Alat</td>
            <td style="border: 1px solid black; text-align: right">{{ ($detail->sewa_alat) ? number_format($detail->sewa_alat) : 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $sewa_alat = ($detail->sewa_alat) ? $detail->sewa_alat : 0;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 4 }}</td>
            <td style="border: 1px solid black;" colspan='3'>Sewa OK</td>
            <td style="border: 1px solid black; text-align: right">{{ ($detail->sewa_ok) ? number_format($detail->sewa_ok) : 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $sewa_ok = ($detail->sewa_ok) ? $detail->sewa_ok : 0;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $num + 5 }}</td>
            <td style="border: 1px solid black;" colspan='3'>Biaya Lain</td>
            <td style="border: 1px solid black; text-align: right">{{ number_format($detail->biaya_lain) ?? 0 }}</td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $biaya_lain = ($detail->biaya_lain) ? $detail->biaya_lain : 0;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align:center; font-weight: 600" colspan='2'><b>Jumlah</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"><b>{{ number_format($sewa_alat + $sewa_ok + $biaya_lain) }}</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"></td>
            @php
            $total_biaya_lain = $sewa_alat + $sewa_ok + $biaya_lain;
            @endphp
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align:center; font-weight: 600" colspan='2'><b>Jumlah Total</b></td>
            <td style="border: 1px solid black; text-align: right"><b>{{ $totalPemakaian }}</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
            <td style="border: 1px solid black; text-align: right"><b>{{ number_format($totalharga + $total_biaya_lain) }}</b></td>
            <td style="border: 1px solid black; text-align: right"><b>{{ number_format($jasa_perawat + $jasa_dokter) }}</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
        </tr>
        <tr>
            <td style="border: 1px solid black; text-align:center; font-weight: 600" colspan='4'><b>Total</b></td>
            <td style="border: 1px solid black; text-align: center" colspan="2"><b>{{ number_format($totalharga + $total_biaya_lain + $jasa_perawat + $jasa_dokter) }}</b></td>
            <td style="border: 1px solid black; text-align: right"></td>
        </tr>
    </table>
    <table style="margin-top: 20px; width: 100%">
        <tr>
            <td style="width: 100%;text-align:right">Semarang, {{\Carbon\Carbon::parse($printDate)->format('d/m/Y')}}</td>
        </tr>
        <tr>
            <td style="width: 100%;text-align:right">PETUGAS OPERASI</td>
        </tr>
        <tr>
            <td style="width: 100%;text-align:right; padding-top: 50px">________________</td>
        </tr>
    </table>
</page>