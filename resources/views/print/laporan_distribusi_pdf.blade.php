<style>
    table td {
        padding: 2px 4px;
        vertical-align: middle;
    }
</style>
<page backbottom="7mm" backtop="7mm">
    <page_footer>
        <table>
            <tr>
                <td>
                    <p>Dicetak tanggal: {{ $data->printDate }}, Oleh: {{ $data->printedBy }}</p>
                </td>
                <td style="width: 630px;"></td>
                <td>
                    <p>Halaman [[page_cu]] Dari [[page_nb]]</p>
                </td>
            </tr>
        </table>
    </page_footer>
    <table cellpadding="0" cellspacing="0" style=" margin-top: 5px;" width="100%">
        <tr>
            <td style="text-align: center; width: 734px; font-size: 18px; font-weight: 500;">
                Data Persetujuan Gudang
            </td>
        </tr>
        <tr>
            <td style="padding: 25px 0px 0px 0px; font-size: 14px; font-weight: 500;">
                Tanggal Konfirmasi Gudang : {{ $data->tanggalkonfirmasi }}
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" style=" margin-top: 3em;">
        <tr>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black; width:20px; background: #ddd;">
                No </td>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black; width:250px; background: #ddd;">
                Nama Obat </td>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black; width:114px; background: #ddd;">
                Satuan</td>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black; width:200px; background: #ddd;">
                Kadaluwarsa </td>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black; width:70px; background: #ddd;">
                Jumlah </td>
        </tr>
        @php
        $total = 0;
        @endphp
        @foreach($data as $detail)
        <tr>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black;">
                {{$loop->iteration}} </td>
            <td style="text-align: left; vertical-align: middle; border: 1px solid black;">
                {{ $detail->nama_obat }}
            </td>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black;">
                {{ $detail->nama_satuan ?? 'Belum ditentukan' }} </td>
            <td style="text-align: center; vertical-align: middle; border: 1px solid black;">
                {{ $detail->dataObat->kadaluwarsa_obat }}
            </td>
            <td style="text-align: right; vertical-align: middle; border: 1px solid black;">
                {{ number_format($detail->total) }} </td>
        </tr>
        @php
        $total += $detail->total;
        @endphp
        @endforeach
        <tr>
            <td colspan="4" style="font-weight: bold; font-size: 16px; text-align: center; border: 1px solid black;">
                Total</td>
            <td style="font-weight: bold; font-size: 16px; text-align: right; border: 1px solid black;">
                {{ number_format($total) }}</td>
        </tr>

    </table>
</page>