@extends('layouts.master')

@section('css')
    <!-- code css here -->
    {{-- HAPUS YANG TIDAK DIPERLUKAN!!! --}}
    {{-- Data Tables CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">

    {{-- jQuery Vector Map --}}
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">
@endsection

@section('body')
	<!-- code body here -->
	Blank Page.....
@endsection

@push('javascript')
    <!-- javascript body here -->
    {{-- HAPUS YANG TIDAK DIPERLUKAN!!! --}}
    {{-- Data Tables --}}
    <script src="{{ asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/assets/js/init-scripts/data-table/datatables-init.js') }}"></script>

    {{-- Chart js --}}
    <script src="{{ asset('assets/vendors/chart.js/dist/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/assets/js/init-scripts/chart-js/chartjs-init.js') }}"></script>

    <!--  flot-chart js -->
    <script src="{{ asset('assets/vendors/flot/excanvas.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot/jquery.flot.crosshair.js') }}"></script>
    <script src="{{ asset('assets/assets/js/init-scripts/flot-chart/curvedLines.js') }}"></script>
    <script src="{{ asset('assets/assets/js/init-scripts/flot-chart/flot-tooltip/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/assets/js/init-scripts/flot-chart/flot-chart-init.js') }}"></script>

    <!-- chart-piety -->
    <script src="{{ asset('assets/vendors/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('assets/assets/js/init-scripts/peitychart/peitychart.init.js') }}"></script>

    {{-- Chosen Form Advance jQuery --}}
    <script src="{{ asset('assets/vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        jQuery(document).ready(function() {
            jQuery(".standardSelect").chosen({
                disable_search_threshold: 10,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
        });
    </script>

    {{-- Form Validation jQuery --}}
    <script src="{{ asset('assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>


    {{-- Dahsboard --}}
    <script src="{{ asset('assets/assets/js/dashboard.js') }}"></script>

    {{-- Widget --}}
    <script src="{{ asset('assets/vendors/chart.js/dist/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/assets/js/widgets.js') }}"></script>

    {{-- Map --}}
    <script src="{{ asset('assets/vendors/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ asset('assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>
@endpush
