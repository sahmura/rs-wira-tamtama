@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Laporan <strong>Pemakaian</strong></h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-borderless">
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Nama Pasien</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->nama ?? '-' }} ({{ $data->tipe_pasien ?? 'Tipe Pasien tidak tersetting' }})</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Tanggal Lahir</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ \Carbon\Carbon::parse($data->tanggal_lahir)->format('d/m/Y') ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Jenis Kelamin</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ ($data->jenis_kelamin == 'P') ? 'Perempuan' : 'Laki-laki' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Usia</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ \Carbon\Carbon::createFromDate($data->tanggal_lahir)->age ?? '-' }} Tahun</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Status</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->status ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Pangkat / Pekerjaan</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->pangkat_pekerjaan ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Alamat</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->alamat ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Macam Operasi</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->macam_operasi ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Golongan Operasi</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->golongan_operasi ?? '-' }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-borderless">
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Tanggal Operasi</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ (isset($data->tanggal_operasi)) ? \Carbon\Carbon::parse($data->tanggal_operasi)->locale('id')->isoFormat('Do MMMM YYYY') : '-' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Dokter Bedah</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->dokter_bedah->nama ?? 'Belum disetting '}}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Dokter Anestesi</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->dokter_anestesi->nama ?? 'Belum disetting '}}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Dokter Spesialis</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->dokter->nama ?? 'Belum disetting '}}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Perawat Bedah</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->perawat_bedah ?? 'Belum disetting '}}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Perawat Anestes</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $data->perawat_anestesi ?? 'Belum disetting '}}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Sewa Alat</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ ($data->sewa_alat) ? number_format($data->sewa_alat) : 'Belum disetting '}}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Sewa OK</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ ($data->sewa_ok) ? number_format($data->sewa_ok) : 'Belum disetting '}}</td>
                                </tr>
                                <tr>
                                    <td style="width: 170px;font-weight: 600">Biaya Lain-lain</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ ($data->biaya_lain) ? number_format($data->biaya_lain) : 'Belum disetting '}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-12 mt-5">
                            <table class="table table-bordered">
                                <thead>
                                    <th style="width: 20px; text-align: center">No</th>
                                    <th>Nama Obat</th>
                                    <th>Jumlah</th>
                                    <th>Harga Obat</th>
                                    <th>Sub Total</th>
                                    <th>Jasa</th>
                                    <th>Ket</th>
                                </thead>
                                <tbody>
                                    @if($data->tipe_pasien == 'BPJS')
                                    @php $persenan = 20/100; @endphp
                                    @elseif($data->tipe_pasien == 'SWASTA')
                                    @php $persenan = 30/100; @endphp
                                    @else
                                    @php $persenan = 0; @endphp
                                    @endif

                                    @php
                                    $totalPemakaian = 0;
                                    $totalHarga = 0;
                                    $num = 0;
                                    $jasa_dokter = 0;
                                    $jasa_perawat = 0;
                                    @endphp
                                    @foreach($content as $obat)
                                    @php
                                    $hargaObatDanPersenan = (float) str_replace('.', '', $obat['harga_satuan_terakhir']) + ((float) str_replace('.', '', $obat['harga_satuan_terakhir']) * $persenan);
                                    @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $obat['nama'] }}</td>
                                        <td style="text-align: right">{{ $obat['pemakaian'] }}</td>
                                        <td style="text-align: right">{{ number_format($hargaObatDanPersenan) }}</td>
                                        <td style="text-align: right">{{ number_format((float) $obat['pemakaian'] * $hargaObatDanPersenan) }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @php
                                    $totalPemakaian += (int) $obat['pemakaian'];
                                    $totalHarga += ((float) $obat['pemakaian'] * $hargaObatDanPersenan);
                                    $num = $loop->iteration;
                                    @endphp
                                    @endforeach
                                    <tr>
                                        <td>{{ $num + 1 }}</td>
                                        <td>Jasa Dokter Bedah</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->jasa_dokter_bedah) ? number_format($data->jasa_dokter_bedah) : 0 }}</td>
                                        <td></td>
                                        @php
                                        $jasa_dokter_bedah = ($data->jasa_dokter_bedah) ? $data->jasa_dokter_bedah : 0;
                                        @endphp
                                    </tr>
                                    <tr>
                                        <td>{{ $num + 2 }}</td>
                                        <td>Jasa Dokter Anestesi</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->jasa_dokter_anestesi) ? number_format($data->jasa_dokter_anestesi) : 0 }}</td>
                                        <td></td>
                                        @php
                                        $jasa_dokter_anestesi = ($data->jasa_dokter_anestesi) ? $data->jasa_dokter_anestesi : 0;
                                        @endphp
                                    </tr>
                                    <tr>
                                        <td>{{ $num + 3 }}</td>
                                        <td>Jasa Dokter Spesialis</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->jasa_dokter_spesialis) ? number_format($data->jasa_dokter_spesialis) : 0 }}</td>
                                        <td></td>
                                        @php
                                        $jasa_dokter_spesialis = ($data->jasa_dokter_spesialis) ? $data->jasa_dokter_spesialis : 0;
                                        $jasa_dokter = $jasa_dokter_anestesi + $jasa_dokter_bedah + $jasa_dokter_spesialis;
                                        @endphp
                                    </tr>
                                    <tr>
                                        <td>{{ $num + 4 }}</td>
                                        <td>Jasa Perawat Bedah</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->jasa_perawat_bedah) ? number_format($data->jasa_perawat_bedah) : 0 }}</td>
                                        <td></td>
                                        @php
                                        $jasa_perawat_bedah = ($data->jasa_perawat_bedah) ? $data->jasa_perawat_bedah : 0;
                                        @endphp
                                    </tr>
                                    <tr>
                                        <td>{{ $num + 4 }}</td>
                                        <td>Jasa Perawat Anestesi</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->jasa_perawat_anestesi) ? number_format($data->jasa_perawat_anestesi) : 0 }}</td>
                                        <td></td>
                                        @php
                                        $jasa_perawat_anestesi = ($data->jasa_perawat_anestesi) ? $data->jasa_perawat_anestesi : 0;
                                        $jasa_perawat = $jasa_perawat_anestesi + $jasa_perawat_bedah;
                                        @endphp
                                    </tr>
                                    <tr>
                                        <td>{{ $num + 5 }}</td>
                                        <td>Sewa Alat</td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->sewa_alat) ? number_format($data->sewa_alat) : 0 }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>{{ $num + 6 }}</td>
                                        <td>Sewa OK</td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->sewa_ok) ? number_format($data->sewa_ok) : 0 }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>{{ $num + 7 }}</td>
                                        <td>Biaya Lain</td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">{{ ($data->biaya_lain) ? number_format($data->biaya_lain) : 0 }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                                <tfoot style="font-weight: 600;">
                                    @php
                                    $total_biaya_lain = $data->sewa_alat + $data->sewa_ok + $data->biaya_lain;
                                    @endphp
                                    <tr>
                                        <td colspan="2" class="text-center">Jumlah</td>
                                        <td class="text-right">{{ $totalPemakaian }}</td>
                                        <td></td>
                                        <td class="text-right">{{ number_format($totalHarga + $total_biaya_lain) }}</td>
                                        <td class="text-right">{{ number_format($jasa_dokter + $jasa_perawat)}}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-center">Total</td>
                                        <td class="text-center" colspan="2">{{ number_format($totalHarga + $total_biaya_lain + $jasa_dokter + $jasa_perawat) }}</td>
                                        <td class="text-right"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-12 my-3">
                            @if($data->is_validate == 0)
                            <button class="btn btn-success float-right" id="btn-validasi" data-id="{{$data->id}}">Validasi</button>
                            @else
                            <button class="btn btn-success float-right ml-1" id="btn-cetak" data-id="{{$data->id}}">Cetak Data</button>
                            <button class="btn btn-danger float-right ml-1" id="btn-batal" data-id="{{$data->id}}">Batalkan</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('javascript')
<script>
    $('#btn-cetak').on('click', function() {
        var id = $(this).data('id');
        window.open("{{ url('laporanpemakaianobat/cetakData') }}" + '?id=' + id + '&target=_blank');
    });

    $('#btn-batal').on('click', function() {
        var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin membatalkan laporan?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#e3342f',
            confirmButtonText: 'Batalkan',
            cancelButtonText: 'Tutup'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{ url("laporanpemakaianobat/batalkanValidasi") }}',
                    method: 'POST',
                    data: {
                        'id': id,
                    },
                    success: function(res) {
                        swal.fire({
                            title: res.title,
                            icon: res.icon,
                            text: res.text,
                            timer: 3000,
                        });
                        window.location.reload();

                    },
                });
            }
        });
    });

    $('#btn-validasi').on('click', function() {
        var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin memvalidasi laporan?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#4DA361',
            confirmButtonText: 'Validasi',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{ url("laporanpemakaianobat/validasiData") }}',
                    method: 'POST',
                    data: {
                        'id': id,
                    },
                    success: function(res) {
                        swal.fire({
                            title: res.title,
                            icon: res.icon,
                            text: res.text,
                            timer: 3000,
                        });
                        window.location.reload();
                    },
                });
            }
        });
    });
</script>
@endpush