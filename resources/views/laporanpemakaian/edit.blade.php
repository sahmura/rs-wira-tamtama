@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Tambah <strong>Laporan Pemakaian</strong></h2>
                </div>
                <div class="card-body">
                    <form action="" id="dataform" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" id="id" name="id" value="{{ $data->id }}">
                                <div class="form-group">
                                    <label for="nama">Nama Pasien</label>
                                    <input type="text" class="form-control" name="nama" id="name" placeholder="Nama Pasien" value="{{ $data->nama }}">
                                </div>
                                <div class="form-group">
                                    <label for="tipe_pasien">Tipe Pasien</label>
                                    <select name="tipe_pasien" id="tipe_pasien" class="form-control" required>
                                        <option value="BPJS" @if($data->tipe_pasien == 'BPJS') selected='selected' @endif>BPJS</option>
                                        <option value="SWASTA" @if($data->tipe_pasien == 'SWASTA') selected='selected' @endif>SWASTA</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" value="{{ \Carbon\Carbon::parse($data->tanggal_lahir)->format('Y-m-d') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    <select name="jenis_kelamin" id="jenis_kelamin" class="custom-select" required>
                                        <option value="L" @if($data->jenis_kelamin == 'L') selected='selected' @endif>Laki-laki</option>
                                        <option value="P" @if($data->jenis_kelamin == 'P') selected='selected' @endif>Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="{{ $data->status }}">
                                </div>
                                <div class="form-group">
                                    <label for="pangkat_pekerjaan">Pangkat / Pekerjaan</label>
                                    <input type="text" class="form-control" name="pangkat_pekerjaan" id="pangkat_pekerjaan" placeholder="Pangkat / Pekerjaan" value="{{ $data->pangkat_pekerjaan }}">
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat">{{ $data->alamat }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="macam_operasi">Macam Operasi</label>
                                    <input type="text" class="form-control" name="macam_operasi" id="macam_operasi" placeholder="Macam Operasi" value="{{ $data->macam_operasi }}">
                                </div>
                                <div class="form-group">
                                    <label for="golongan_operasi">Golongan Operasi</label>
                                    <input type="text" class="form-control" name="golongan_operasi" id="golongan_operasi" placeholder="Golongan Operasi" value="{{ $data->golongan_operasi }}">
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_operasi">Tanggal Operasi</label>
                                    <input type="date" class="form-control" name="tanggal_operasi" id="tanggal_operasi" placeholder="Tanggal Operasi" value="{{ \Carbon\Carbon::parse($data->tanggal_operasi)->format('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dokter_id_bedah">Dokter Bedah</label>
                                    <select name="dokter_id_bedah" id="dokter_id_bedah" class="select2" style="width: 100%;" required>
                                        <option value="0">Tidak Ada</option>
                                        @foreach($dokter as $dokbed)
                                        <option value="{{ $dokbed->id }}" @if($data->dokter_id_bedah == $dokbed->id) selected='selected' @endif>{{ $dokbed->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jasa_dokter_bedah">Jasa Dokter Bedah</label>
                                    <input type="number" class="form-control" name="jasa_dokter_bedah" id="jasa_dokter_bedah" placeholder="Jasa Dokter Bedah" value="{{ $data->jasa_dokter_bedah ?? 0 }}">
                                </div>
                                <div class="form-group">
                                    <label for="dokter_id_anestesi">Dokter Anestesi</label>
                                    <select name="dokter_id_anestesi" id="dokter_id_anestesi" class="select2" style="width: 100%;" required>
                                        <option value="0">Tidak Ada</option>
                                        @foreach($dokter as $dokanes)
                                        <option value="{{ $dokanes->id }}" @if($data->dokter_id_anestesi == $dokanes->id) selected='selected' @endif>{{ $dokanes->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jasa_dokter_anestesi">Jasa Dokter Anestesi</label>
                                    <input type="number" class="form-control" name="jasa_dokter_anestesi" id="jasa_dokter_anestesi" placeholder="Jasa Dokter Anestesi" value="{{ $data->jasa_dokter_anestesi ?? 0 }}">
                                </div>
                                <div class="form-group">
                                    <label for="dokter_id">Dokter Spesialis</label>
                                    <select name="dokter_id" id="dokter_id" class="select2" style="width: 100%;" required>
                                        <option value="0">Tidak Ada</option>
                                        @foreach($dokter as $dok)
                                        <option value="{{ $dok->id }}" @if($data->dokter_id == $dok->id) selected='selected' @endif>{{ $dok->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jasa_dokter_spesialis">Jasa Dokter Spesialis</label>
                                    <input type="number" class="form-control" name="jasa_dokter_spesialis" id="jasa_dokter_spesialis" placeholder="Jasa Dokter Spesialis" value="{{ $data->jasa_dokter_spesialis ?? 0 }}">
                                </div>
                                <div class="form-group">
                                    <label for="perawat_bedah">Perawat Bedah</label>
                                    <input type="text" class="form-control" name="perawat_bedah" id="perawat_bedah" placeholder="Perawat Bedah" value="{{ $data->perawat_bedah }}">
                                </div>
                                <div class="form-group">
                                    <label for="jasa_perawat_bedah">Jasa Perawat Bedah</label>
                                    <input type="number" class="form-control" name="jasa_perawat_bedah" id="jasa_perawat_bedah" placeholder="Jasa Perawat Bedah" value="{{ $data->jasa_perawat_bedah ?? 0 }}">
                                </div>
                                <div class="form-group">
                                    <label for="perawat_anestesi">Perawat Anestesi</label>
                                    <input type="text" class="form-control" name="perawat_anestesi" id="perawat_anestesi" placeholder="Perawat Anestesi" value="{{ $data->perawat_anestesi }}">
                                </div>
                                <div class="form-group">
                                    <label for="jasa_perawat_anestesi">Jasa Perawat Anestesi</label>
                                    <input type="number" class="form-control" name="jasa_perawat_anestesi" id="jasa_perawat_anestesi" placeholder="Jasa Perawat Anestesi" value="{{ $data->jasa_perawat_anestesi ?? 0 }}">
                                </div>
                                <div class="form-group">
                                    <label for="sewa_alat">Sewa Alat</label>
                                    <input type="number" class="form-control" name="sewa_alat" id="sewa_alat" placeholder="Sewa Alat" value="{{ $data->sewa_alat }}">
                                </div>
                                <div class="form-group">
                                    <label for="sewa_ok">Sewa OK</label>
                                    <input type="number" class="form-control" name="sewa_ok" id="sewa_ok" placeholder="Sewa OK" value="{{ $data->sewa_ok }}">
                                </div>
                                <div class="form-group">
                                    <label for="biaya_lain">Biaya Lain</label>
                                    <input type="number" class="form-control" name="biaya_lain" id="biaya_lain" placeholder="Sewa OK" value="{{ $data->biaya_lain }}">
                                </div>
                            </div>
                            <div class="col-md-12 mt-5">
                                <div class="form-group mb-3">
                                    <label for="">Data Obat</label>
                                    <button class="btn btn-xs btn-success float-right" id="btn-add-group" type="button">Masukan Group</button>
                                </div>
                                @foreach($data->dataObats as $list)
                                <div class="input-group mb-3">
                                    <select name="obat[]" id="obat-{{$loop->iteration - 1}}" class="select2 firstdata" style="width: 60%;">
                                        @foreach($obats as $dataobat)
                                        <option value="{{ $dataobat->id }}" @if($dataobat->id == $list->obat_id) selected='selected' @endif>{{ $dataobat->nama }}</option>
                                        @endforeach
                                    </select>
                                    <input type="number" name="pemakaian[]" id="pemakaian" class="form-control" placeholder="Pemakaian" value="{{ $list->pemakaian }}">
                                    <div class="input-group-append">
                                        <button class="btn btn-danger btn-hapus-obat btn-sm" type="button" id="button-addon2">Hapus</button>
                                    </div>
                                </div>
                                @endforeach
                                <div id="obatlain"></div>
                                <div class="form-group">
                                    <button id="btn-simpan-data" class="btn btn-success float-right" type="button">Simpan data</button>
                                    <button id="btn-tambah-data" class="btn btn-primary float-right" type="button">Tambah data</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="groupModal" tabindex="-1" role="dialog" aria-labelledby="groupModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="groupModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="pilihgroup">Pilih Group</label>
                    <select name="pilihgroup" id="pilihgroup" class="form-control">
                        @foreach($groups as $group)
                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-pilih-group">Pilih</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('javascript')
<!-- javascript body here -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    function refreshdata() {
        $('#dataGroup').DataTable({
            paginate: true,
            destroy: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('laporanpemakaianobat') }}",
                method: 'GET'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'nama'
                },
                {
                    data: 'action',
                    class: 'text-center'
                },
            ]
        });
    }

    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    $(document).ready(function() {
        refreshdata();
        // $('.select2').each(function(index, value) {
        //     $('#obat-' + index).select2('open');
        //     $('#obat-' + index).trigger('change');
        // })
        $('.select2').select2().select2('open');
        $('.select2').select2().select2('close');
        $('#dokter_id').val('{{ $data->dokter_id }}');
        $('#dokter_id').trigger('change');
        $('#dokter_id_bedah').val('{{ $data->dokter_id_bedah }}');
        $('#dokter_id_bedah').trigger('change');
        $('#dokter_id_anestesi').val('{{ $data->dokter_id_anestesi }}');
        $('#dokter_id_anestesi').trigger('change');
    });

    $('#btn-tambah-data').on('click', function() {
        let id = makeid(5);
        let input = '<div class="input-group mb-3"><select name="obat[]" id="' +
            id +
            '" class="select-obat" style="width: 60% ;">' +
            @foreach($obats as $dataobats)
        '<option value="{{ $dataobats->id }}">{{ $dataobats->nama }}</option>' +
        @endforeach
            '</select>' +
            '<input type="number" name="pemakaian[]" id="pemakaian" class="form-control" placeholder="Pemakaian"><div class="input-group-append"><button class="btn btn-danger btn-hapus-obat btn-sm" type="button" id="button-addon2">Hapus</button></div></div>';
        $('#obatlain').append(input);
        $('#' + id).select2();
    });

    $(document).on('click', '.btn-hapus-obat', function() {
        $(this).parent('div').parent('div').remove();
    });

    $('#btn-simpan-data').on('click', function() {
        swal.fire({
            title: 'Simpan data?',
            icon: 'warning',
            showCancelButton: true,
        }).then((Confirm) => {
            if (Confirm.value) {
                var data = $('#dataform').serialize();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{ url("laporanpemakaianobat/editData") }}',
                    method: 'POST',
                    data: data,
                    success: function(response) {
                        swal.fire({
                            title: response.title,
                            text: response.text,
                            icon: response.icon,
                        }).then((Confirm) => {
                            if (response.status) {
                                window.location.href = "{{ url('laporanpemakaianobat') }}";
                            } else {
                                window.location.reload();
                            }
                        });
                    },
                });
            }
        });
    });

    $('#btn-pilih-group').on('click', function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                    'content')
            },
            url: '{{ url("laporanpemakaianobat/getObat") }}',
            method: 'GET',
            data: {
                id: $('#pilihgroup').val()
            },
            success: function(data) {
                var list = '';
                $.each(data, function(index, value) {
                    if (index == 0) {
                        $('.firstdata').val(value.id);
                        $('.firstdata').trigger('change');
                    } else {
                        let id = makeid(5);
                        let input = '<div class="input-group mb-3"><select name="obat[]" id="' +
                            id +
                            '" class="select-obat" style="width: 89%">' +
                            @foreach($obats as $dataobats2)
                        '<option value="{{ $dataobats2->id }}">{{ $dataobats2->nama }}</option>' +
                        @endforeach
                            '</select>' +
                            '<input type="number" name="pemakaian[]" id="pemakaian" class="form-control" placeholder="Pemakaian"><div class="input-group-append"><button class="btn btn-danger btn-hapus-obat btn-sm" type="button" id="button-addon2">Hapus</button></div></div>';
                        $('#obatlain').append(input);
                        $('#' + id).select2();
                        $('#' + id).val(value.id);
                        $('#' + id).trigger('change');
                    }
                });
                $('#groupModal').modal('hide');

            }
        });
    });

    $('#btn-add-group').on('click', function() {
        $('#groupModal').modal('show');
    })
</script>
@endpush