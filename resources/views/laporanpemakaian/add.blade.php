@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Tambah <strong>Laporan Pemakaian</strong></h2>
                </div>
                <div class="card-body">
                    <form action="" id="dataform" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Nama Pasien</label>
                                    <input type="text" class="form-control" name="nama" id="name" placeholder="Nama Pasien" required>
                                </div>
                                <div class="form-group">
                                    <label for="tipe_pasien">Tipe Pasien</label>
                                    <select name="tipe_pasien" id="tipe_pasien" class="form-control" required>
                                        <option value="BPJS">BPJS</option>
                                        <option value="SWASTA">SWASTA</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" required>
                                </div>
                                <div class="form-group">
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    <select name="jenis_kelamin" id="jenis_kelamin" class="custom-select" required>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <input type="text" class="form-control" name="status" id="status" placeholder="Status">
                                </div>
                                <div class="form-group">
                                    <label for="pangkat_pekerjaan">Pangkat / Pekerjaan</label>
                                    <input type="text" class="form-control" name="pangkat_pekerjaan" id="pangkat_pekerjaan" placeholder="Pangkat / Pekerjaan">
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="macam_operasi">Macam Operasi</label>
                                    <input type="text" class="form-control" name="macam_operasi" id="macam_operasi" placeholder="Macam Operasi">
                                </div>
                                <div class="form-group">
                                    <label for="golongan_operasi">Golongan Operasi</label>
                                    <input type="text" class="form-control" name="golongan_operasi" id="golongan_operasi" placeholder="Golongan Operasi">
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_operasi">Tanggal Operasi</label>
                                    <input type="date" class="form-control" name="tanggal_operasi" id="tanggal_operasi" placeholder="Tanggal Operasi">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dokter_id_bedah">Dokter Bedah</label>
                                    <select name="dokter_id_bedah" id="dokter_id_bedah" class="select2" style="width: 100%;" required>
                                        <option value="0">Tidak Ada</option>
                                        @foreach($dokter as $dok)
                                        <option value="{{ $dok->id }}">{{ $dok->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jasa_dokter_bedah">Jasa Dokter Bedah</label>
                                    <input type="number" class="form-control" name="jasa_dokter_bedah" id="jasa_dokter_bedah" placeholder="Jasa Dokter Bedah">
                                </div>
                                <div class="form-group">
                                    <label for="dokter_id_anestesi">Dokter Anestesi</label>
                                    <select name="dokter_id_anestesi" id="dokter_id_anestesi" class="select2" style="width: 100%;" required>
                                        <option value="0">Tidak Ada</option>
                                        @foreach($dokter as $dok)
                                        <option value="{{ $dok->id }}">{{ $dok->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jasa_dokter_anestesi">Jasa Dokter Anestesi</label>
                                    <input type="number" class="form-control" name="jasa_dokter_anestesi" id="jasa_dokter_anestesi" placeholder="Jasa Dokter Anestesi">
                                </div>
                                <div class="form-group">
                                    <label for="dokter_id">Dokter Spesialis</label>
                                    <select name="dokter_id" id="dokter_id" class="select2" style="width: 100%;" required>
                                        <option value="0">Tidak Ada</option>
                                        @foreach($dokter as $dok)
                                        <option value="{{ $dok->id }}">{{ $dok->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jasa_dokter_spesialis">Jasa Dokter Spesialis</label>
                                    <input type="number" class="form-control" name="jasa_dokter_spesialis" id="jasa_dokter_spesialis" placeholder="Jasa Dokter Spesialis">
                                </div>
                                <div class="form-group">
                                    <label for="perawat_bedah">Perawat Bedah</label>
                                    <input type="text" class="form-control" name="perawat_bedah" id="perawat_bedah" placeholder="Perawat Bedah">
                                </div>
                                <div class="form-group">
                                    <label for="jasa_perawat_bedah">Jasa Perawat Bedah</label>
                                    <input type="number" class="form-control" name="jasa_perawat_bedah" id="jasa_perawat_bedah" placeholder="Jasa Perawat Bedah">
                                </div>
                                <div class="form-group">
                                    <label for="perawat_anestesi">Perawat Anestesi</label>
                                    <input type="text" class="form-control" name="perawat_anestesi" id="perawat_anestesi" placeholder="Perawat Anestesi">
                                </div>
                                <div class="form-group">
                                    <label for="jasa_perawat_anestesi">Jasa Perawat Anestesi</label>
                                    <input type="number" class="form-control" name="jasa_perawat_anestesi" id="jasa_perawat_anestesi" placeholder="Jasa Perawat Anestesi">
                                </div>
                                <div class="form-group">
                                    <label for="sewa_alat">Sewa Alat</label>
                                    <input type="number" class="form-control" name="sewa_alat" id="sewa_alat" placeholder="Sewa Alat">
                                </div>
                                <div class="form-group">
                                    <label for="sewa_ok">Sewa OK</label>
                                    <input type="number" class="form-control" name="sewa_ok" id="sewa_ok" placeholder="Sewa OK">
                                </div>
                                <div class="form-group">
                                    <label for="biaya_lain">Biaya Lain</label>
                                    <input type="number" class="form-control" name="biaya_lain" id="biaya_lain" placeholder="Sewa OK">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group mt-5 mb-3">
                                    <label for="">Data Obat</label>
                                    <button class="btn btn-xs btn-success float-right" id="btn-add-group" type="button">Masukan Group</button>
                                </div>
                                <div class="input-group mb-3">
                                    <select name="obat[]" id="obat" class="select2 firstdata" style="width: 70%;">
                                        @foreach($obats as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                        @endforeach
                                    </select>
                                    <input type="number" name="pemakaian[]" id="pemakaian" class="form-control" placeholder="Pemakaian">
                                </div>
                                <div id="obatlain"></div>
                                <div class="form-group">
                                    <button id="btn-simpan-data" class="btn btn-success float-right" type="button">Simpan data</button>
                                    <button id="btn-tambah-data" class="btn btn-primary float-right" type="button">Tambah data</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="groupModal" tabindex="-1" role="dialog" aria-labelledby="groupModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="groupModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="pilihgroup">Pilih Group</label>
                    <select name="pilihgroup" id="pilihgroup" class="form-control">
                        @foreach($groups as $group)
                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-pilih-group">Pilih</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('javascript')
<!-- javascript body here -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    function refreshdata() {
        $('#dataGroup').DataTable({
            paginate: true,
            destroy: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('laporanpemakaianobat') }}",
                method: 'GET'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'nama'
                },
                {
                    data: 'action',
                    class: 'text-center'
                },
            ]
        });
    }

    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    function input() {
        return
    }

    $(document).ready(function() {
        refreshdata();
        $('.select2').select2();
    });

    $('#btn-tambah-data').on('click', function() {
        let id = makeid(5);
        let input = '<div class="input-group mb-3"><select name="obat[]" id="' +
            id +
            '" class="select-obat" style="width: 70% ;">' +
            @foreach($obats as $data)
        '<option value="{{ $data->id }}">{{ $data->nama }}</option>' +
        @endforeach
            '</select>' +
            '<input type="number" name="pemakaian[]" id="pemakaian" class="form-control" placeholder="Pemakaian"><div class="input-group-append"><button class="btn btn-danger btn-hapus-obat btn-sm" type="button" id="button-addon2">Hapus</button></div></div>';
        $('#obatlain').append(input);
        $('#' + id).select2();
    });

    $(document).on('click', '.btn-hapus-obat', function() {
        $(this).parent('div').parent('div').remove();
    });

    $('#btn-simpan-data').on('click', function() {
        swal.fire({
            title: 'Simpan data?',
            icon: 'warning',
            showCancelButton: true,
        }).then((Confirm) => {
            if (Confirm.value) {
                var data = $('#dataform').serialize();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{ url("laporanpemakaianobat/addData") }}',
                    method: 'POST',
                    data: data,
                    success: function(response) {
                        swal.fire({
                            title: response.title,
                            text: response.text,
                            icon: response.icon,
                        }).then((Confirm) => {
                            if (response.status) {
                                window.location.href = "{{ url('laporanpemakaianobat') }}";
                            } else {
                                window.location.reload();
                            }
                        });
                    },
                });
            }
        });
    });

    $('#btn-pilih-group').on('click', function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                    'content')
            },
            url: '{{ url("laporanpemakaianobat/getObat") }}',
            method: 'GET',
            data: {
                id: $('#pilihgroup').val()
            },
            success: function(data) {
                var list = '';
                $.each(data, function(index, value) {
                    if (index == 0) {
                        $('.firstdata').val(value.id);
                        $('.firstdata').trigger('change');
                    } else {
                        let id = makeid(5);
                        let input = '<div class="input-group mb-3"><select name="obat[]" id="' +
                            id +
                            '" class="select-obat" style="width: 70% ;">' +
                            @foreach($obats as $data)
                        '<option value="{{ $data->id }}">{{ $data->nama }}</option>' +
                        @endforeach
                            '</select>' +
                            '<input type="number" name="pemakaian[]" id="' + id + '_pemakaian" class="form-control" placeholder="Pemakaian"><div class="input-group-append"><button class="btn btn-danger btn-hapus-obat btn-sm" type="button" id="button-addon2">Hapus</button></div></div>';
                        $('#obatlain').append(input);
                        $('#' + id).select2();
                        $('#' + id).val(value.id);
                        $('#' + id).trigger('change');
                    }
                });
                $('#groupModal').modal('hide');

            }
        });
    });

    $('#btn-add-group').on('click', function() {
        $('#groupModal').modal('show');
    })
</script>
@endpush