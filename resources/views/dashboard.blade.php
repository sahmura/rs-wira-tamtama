@extends('layouts.master')

@section('css')
<!-- code css here -->
@endsection

@section('body')

{{-- <div class="col-sm-12">
    <div class="alert  alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span> You successfully read this important alert message.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div> --}}

<div class="row">
    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="fa fa-medkit text-success border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-heading"><b>Total Data Obat</b></div>
                        <div class="stat-digit">{{ number_format($dataObat) }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-bookmark-alt text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-heading"><b>Pemasukan Obat</b></div>
                        <div class="stat-digit">{{ number_format($pemasukanObat) }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="fa fa-hospital-o text-warning border-warning"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-heading"><b>Total PBF</b></div>
                        <div class="stat-digit">{{ $totalPbf }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="fa fa-archive text-success border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-heading"><b>Total Stok di Gudang</b></div>
                        <div class="stat-digit">{{ number_format($stokgudang) }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="fa fa-inbox text-danger border-danger"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-heading"><b>Total Stok di Apotek</b></div>
                        <div class="stat-digit">{{ number_format($stokapotek) }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h2>
                    <srong>Data obat</strong>
                </h2>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="dataObatTable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nama</td>
                                <td>Stok Gudang</td>
                                <td>Satuan</td>
                                <td>Tgl. Kadaluwarsa</td>
                                <td>Kadaluwarsa</td>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('javascript')
<!-- javascript body here -->
<script src="{{ asset('assets/assets/js/dashboard.js') }}"></script>
<script src="{{ asset('assets/assets/js/widgets.js') }}"></script>
<script>
    $(document).ready(function () {
        var table = $('#dataObatTable').DataTable({
            processing: true,
            // serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('dashboard/dataobat') }}",
                // method: 'POST',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    'orderable': false,
                    'searchable': false,
                    width: '10px',
                    class: "text-center"

                },
                {
                    data: 'nama_obat',
                },
                {
                    data: 'stok',
                },
                {
                    data: 'satuan',
                },
                {
                    data: 'tanggal_kadaluwarsa',
                },
                {
                    orderable: false,
                    data: 'kadaluwarsa',
                    width: "120px",
                }
            ]
        });
    });

</script>
@endpush
