@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <a href="{{route('users.index')}}" class="btn btn-danger">Kembali</a>
                        </div>
                        <h2><strong>Register </strong> User</h2>
                    </div>
                    <div class="card-body card-block">
                        <form method="POST" action="{{ route('users.update', $user->id) }}" class="form-horizontal">
                            @csrf
                            @method('put')
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="name" class=" form-control-label">Nama Lengkap</label></div>
                                <div class="col-12 col-md-9">
                                    <input value="{{ $user->name }}" type="text" id="name" name="name" placeholder="Nama Lengkap" class="form-control @error('name') is-invalid @enderror" required autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="email" class=" form-control-label">Email</label></div>
                                <div class="col-12 col-md-9">
                                    <input value="{{ $user->email }}" type="email" id="email" name="email" placeholder="Tulis Email" class="form-control @error('email') is-invalid @enderror" required>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="role" class=" form-control-label">Role</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="role" id="role" class="form-control">
                                        <option value="admin">Admin</option>
                                        <option value="apoteker">Apoteker</option>
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="row form-group">
                                <div class="col col-md-3"><label for="password" class=" form-control-label">Password</label></div>
                                <div class="col-12 col-md-9"><input type="password" id="password" name="password" placeholder="Tulis Password" class="form-control">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="password_confirmation" class=" form-control-label">Password</label></div>
                                <div class="col-12 col-md-9"><input type="password" id="password-confirmation" name="password_confirmation" placeholder="Ulangi Password" class="form-control"></div>
                            </div> --}}
                            <div class="row form-group">
                                <div class="col col-md-3"></div>
                                <div class="col col-md-9">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-dot-circle-o"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->

@endpush
