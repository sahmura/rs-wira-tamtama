@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <a href="{{route('users.create')}}" class="btn btn-success">Tambah Users</a>
                    </div>
                    <h2>List <strong>Users</strong></h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="userTable" class="table table-striped table-bordered">
                            <thead>
                                <td>No</td>
                                <td>Nama</td>
                                <td>Email</td>
                                <td>Role</td>
                                <td>Aksi</td>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
{{-- Data Tables --}}
<script>
    $(document).ready(function () {
        var table = $('#userTable').DataTable({
            paginate: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('users.index') }}",
                // method: 'POST',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                    width: '10px'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'role',
                    name: 'role'
                },
                {
                    data: 'action',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                    width: '20px'
                }
            ]
        });

        $(document).on('click', '.btn-delete', function () {
            var id = $(this).data('id');
            var token = $(this).data('token');

            swal.fire({
                title: 'Apakah anda yakin menghapus data?',
                text: 'Data yang dihapus tidak bisa dikembalikan',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#e3342f',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                                'content')
                        },
                        url: '{{url("users/hapusdata")}}' + '/' + id,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id': id,
                            '_method': 'POST',
                            '_token': token,
                        },
                        success: function () {
                            swal.fire({
                                title: 'Data berhasil dihapus',
                                icon: 'success',
                                timer: 3000,
                            });
                            table.ajax.reload();
                        },
                        fail: function () {
                            swal.fire({
                                title: 'Data gagal dihapus',
                                icon: 'error',
                                timer: 3000,
                            });
                            table.ajax.reload();
                        },
                    });
                }
            });
        });
    })

</script>

@endpush
