@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <div class="btn-group">
                            <a href="{{route('dataobat.create')}}" class="btn btn-success">Tambah obat</a>
                            <button class="btn btn-info text-white" id="importData" data-toggle="modal"
                                data-target="#importDataModal">Import data</button>
                        </div>
                    </div>
                    <h2>List <strong>obat terdaftar</strong></h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="obatTable" class="table table-striped table-bordered">
                            <thead>
                                <td>No</td>
                                <td>Tgl Masuk</td>
                                <td>Nomor Faktur</td>
                                <td>PBF</td>
                                <td>Nama Obat</td>
                                <td>Jml Obat Masuk</td>
                                <td>Kadaluwarsa</td>
                                <td>Harga satuan (Rp)</td>
                                <td>Aksi</td>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="importDataModal" tabindex="-1" role="dialog" aria-labelledby="importDataModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importDataModalLabel">Import data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <form action="{{ route('dataobat.importData') }}" method="post" enctype="multipart/form-data"
                    id="importDataForm">
                    @csrf
                    @method('POST')
                    <label for="file">Upload file dalam format excel</label>
                    <input type="file" name="file" id="file" class="form-control" required placeholder="File">
                </form> -->
                Dalam Perbaikan
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info text-white" id="btn-import" form="importDataForm">Import
                    data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
{{-- Data Tables --}}
<script>
    $(document).ready(function () {
        $('#obatTable').DataTable({
            paginate: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('dataobat.index') }}",
                // method: 'POST',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'tanggal_pemasukan',
                    render: function (data, type, row) {
                        var date = new Date(row.tanggal_pemasukan);
                        var monthNames = [
                            "Januari", "Februari", "Maret",
                            "April", "Mei", "Juni", "Juli",
                            "Agustus", "September", "Oktober",
                            "November", "Desember"
                        ];
                        var day = date.getDate();
                        var monthIndex = date.getMonth();
                        var year = date.getFullYear();
                        return day + ' ' + monthNames[monthIndex] + ' ' + year;
                    }
                },
                {
                    data: 'nomor_faktur',
                    name: 'nomor_faktur'
                },
                {
                    data: 'pbf',
                    name: 'pbf'
                },
                {
                    data: 'nama_obat',
                },
                {
                    data: 'jumlah_obat',

                },
                {
                    data: 'tanggal_kadaluwarsa',
                    name: 'tanggal_kadaluwarsa',
                    render: function (data, type, row) {
                        var date = new Date(row.tanggal_kadaluwarsa);
                        var monthNames = [
                            "Januari", "Februari", "Maret",
                            "April", "Mei", "Juni", "Juli",
                            "Agustus", "September", "Oktober",
                            "November", "Desember"
                        ];
                        var day = date.getDate();
                        var monthIndex = date.getMonth();
                        var year = date.getFullYear();
                        return day + ' ' + monthNames[monthIndex] + ' ' + year;
                    }
                },
                {
                    data: 'harga_akhir_satuan',
                },
                {
                    data: 'action',
                    orderable: false
                }
            ]
        });
    })

</script>
<script>
    $(document).on('click', '.btn-delete', function () {
        var id = $(this).data('id');
        var token = $(this).data('token');


        swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            text: 'Data yang dihapus tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#e3342f',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{url("dataobat/hapusdata")}}' + '/' + id,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'id': id,
                        '_method': 'POST',
                        '_token': token,
                    },
                    success: function (response) {
                        swal.fire({
                            title: response.message,
                            icon: 'success',
                            timer: 3000,
                        });
                        location.reload();

                    },

                    fail: function () {
                        swal.fire({
                            title: 'Data gagal dihapus',
                            icon: 'error',
                            timer: 3000,
                        });
                        location.reload();
                    },
                });
            }
        })
    })

</script>

@endpush
