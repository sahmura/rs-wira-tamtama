@extends('layouts.master')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <a href="{{route('dataobat.index')}}" class="btn btn-danger">Kembali</a>
                    </div>
                    <h2 class="card-title">Tambah <strong>data pemasukan</strong></h2>
                </div>
                <div class="card-body">
                    <form action="{{ route('dataobat.store') }}" method="post" id="formTambahData">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nomor_faktur">Nomor faktur</label>
                                    <input autocomplete="off" name="nomor_faktur" id="nomor_faktur" class="form-control"
                                        placeholder="Nomor faktur" list="nomor_fakturs">
                                    <datalist id="nomor_fakturs">
                                        @foreach($listNomorFaktur as $nofaktur)
                                        <option value="{{ $nofaktur->nomor_faktur }}">
                                            @endforeach
                                    </datalist>
                                    @error('nomor_faktur')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_pemasukan">Tanggal Pemasukan</label>
                                    <input type="date" name="tanggal_pemasukan" id="tanggal_pemasukan"
                                        class="form-control" placeholder="Tanggal Pemasukan">
                                    @error('tanggal_pemasukan')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama obat</label>
                                    <select name="obat_id" id="obat_id" class="custom-select js-example-basic-single"
                                        required>
                                        @foreach($namaObat as $nama)
                                        <option value="{{$nama->id}}">{{$nama->nama}}</option>
                                        @endforeach
                                    </select>
                                    <!-- <input type="text" name="nama" id="nama" class="form-control"
                                        placeholder="Nama obat" list="dataObat" autocomplete="off">
                                    <datalist id="dataObat">
                                        @foreach($namaObat as $nama)
                                        <option value="{{ $nama->nama }}"></option>
                                    @endforeach
                                    </datalist> -->
                                    @error('nama')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="pbf">PBF</label>
                                    <select name="pbf_id" id="pbf_id" class="custom-select">
                                        @foreach($pbf as $pbf_data)
                                        <option value="{{ $pbf_data->id }}">{{ $pbf_data->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('pbf')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="satuanjenis">Satuan</label>
                                    <select name="satuanjenis" id="satuanjenis" class="custom-select">
                                        @foreach($satuan as $satuan_data)
                                        <option value="{{ $satuan_data->id }}">{{ $satuan_data->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('pbf')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_kadaluwarsa">Tanggal kadaluwarsa</label>
                                    <input type="date" name="tanggal_kadaluwarsa" id="tanggal_kadaluwarsa"
                                        class="form-control datepicker" placeholder="Kadaluwarsa">
                                    @error('tanggal_kadaluwarsa')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <!-- <div class="form-group">
                                    <label for="kategori">Kategori</label>
                                    <select name="kategori" id="kategori" class="custom-select">
                                        @foreach($kategori as $kat)
                                        <option value="{{ $kat->nama }}">{{ $kat->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('kategori')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div> -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jumlah_kemasan">Jumlah kemasan/box</label>
                                    <input type="number" name="jumlah_kemasan" id="jumlah_kemasan" class="form-control"
                                        placeholder="Jumlah kemasan/box" min="1">
                                    @error('jumlah_kemasan')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_satuan">Jumlah satuan</label>
                                    <input type="number" name="jumlah_satuan" id="jumlah_satuan" class="form-control"
                                        placeholder="Jumlah satuan dalam satu kemasan/box" min="1">
                                    @error('jumlah_satuan')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="hargaperbox">Harga per kemasan/box</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp.</div>
                                        <input type="number" name="hargaperbox" id="hargaperbox" step=".01"
                                            class="form-control" placeholder="Harga per kemasan/box">
                                    </div>
                                    @error('hargaperbox')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="bruto">Netto</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp.</div>
                                        <input type="number" name="netto" id="bruto" step=".01" class="form-control"
                                            placeholder="Netto" min="0">
                                    </div>
                                    @error('bruto')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="ppnbruto">PPN Netto</label>
                                    <div class="input-group">
                                        <input type="number" name="ppn_netto" step=".01" id="ppnbruto"
                                            class="form-control" placeholder="PPN Netto %">
                                        <div class="input-group-addon" min="0">%</div>
                                    </div>
                                    @error('ppnbruto')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="diskon">Diskon</label>
                                    <div class="input-group">
                                        <input type="number" name="diskon" step=".00001" id="diskon"
                                            class="form-control" placeholder="Diskon %" min="0">
                                        <div class="input-group-addon">%</div>
                                    </div>
                                    @error('diskon')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="ppnharga">Harga akhir manual (Tulis 0 Jika ingin dihitung
                                        otomatis)</label>
                                    <div class="input-group">
                                        <input type="number" name="harga_akhir_manual" id="ppnharga"
                                            class="form-control" placeholder="Harga akhir manual" min="0" value="0">
                                    </div>
                                    @error('ppnharga')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <h5 class="mb-5">Cek detail obat</h5>
                    <div class="row">
                        <div class="col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib"><i class="ti-money text-success border-success"></i>
                                        </div>
                                        <div class="stat-content dib">
                                            <div class="stat-text">Jumlah obat</div>
                                            <div class="stat-digit" id="ket_jumlah_obat">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib"><i class="ti-money text-success border-success"></i>
                                        </div>
                                        <div class="stat-content dib">
                                            <div class="stat-text">Harga Netto + PPN</div>
                                            <div class="stat-digit" id="ket_netto_plus_ppn">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib"><i class="ti-money text-success border-success"></i>
                                        </div>
                                        <div class="stat-content dib">
                                            <div class="stat-text">Diskon</div>
                                            <div class="stat-digit" id="ket_diskon">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib"><i class="ti-money text-success border-success"></i>
                                        </div>
                                        <div class="stat-content dib">
                                            <div class="stat-text">Diskon Netto</div>
                                            <div class="stat-digit" id="ket_diskon_netto">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib"><i class="ti-money text-success border-success"></i>
                                        </div>
                                        <div class="stat-content dib">
                                            <div class="stat-text">Harga Akhir</div>
                                            <div class="stat-digit" id="ket_harga_akhir">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib"><i class="ti-money text-success border-success"></i>
                                        </div>
                                        <div class="stat-content dib">
                                            <div class="stat-text">Harga Akhir Satuan</div>
                                            <div class="stat-digit" id="ket_harga_akhir_satuan">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button class="btn btn-success" id="btn-tambah" type="submit"
                                    form="formTambahData">Tambah data obat</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {

        $('.js-example-basic-single').select2();

        $('input[type=number').attr('min', '0');

        let ket_jumlah_obat = 0;
        let ket_netto_plus_ppn = 0;
        let ket_diskon = 0;
        let ket_diskon_netto = 0;
        let ket_harga_akhir = 0;
        let ket_harga_akhir_satuan = 0;

        let jumlah_kemasan = 0;
        let jumlah_satuan = 0;
        let bruto = 0;
        let ppnbruto = 0;
        let ppnharga = 0;
        let diskon = 0;

        $('#jumlah_kemasan').keyup(function (index) {
            jumlah_kemasan = $(this).val();
            calc();
        });

        $('#jumlah_satuan').keyup(function (index) {
            jumlah_satuan = $(this).val();
            calc();
        });

        $('#bruto').keyup(function () {
            bruto = $(this).val();
            calc();
        })

        $('#ppnbruto').keyup(function () {
            ppnbruto = $(this).val();
            calc();
        })

        $('#ppnharga').keyup(function () {
            ppnharga = $(this).val();
            calc();
        })

        $('#diskon').keyup(function () {
            diskon = $(this).val();
            calc();
        });

        $('#nama').on('change', function () {
            var name = $('#nama').val();
            var data = getData(name);
        });

        $('#hargaperbox').keyup(function () {
            var netto = ($(this).val() * $('#jumlah_kemasan').val());
            $('#bruto').val(netto);
            bruto = netto;
            calc();

        });

        $('#jumlah_kemasan').keyup(function () {
            jumlah_kemasan = $(this).val();
            var netto = ($(this).val() * $('#hargaperbox').val());
            $('#bruto').val(netto);
            calc();

        });

        $('#bruto').keyup(function () {
            bruto = $(this).val();
            var hargaperitem = ($(this).val() / $('#jumlah_kemasan').val());
            $('#hargaperbox').val(hargaperitem.toFixed(2));
            calc();

        });

        function calc() {
            ket_jumlah_obat = jumlah_kemasan * jumlah_satuan;
            ket_netto_plus_ppn = parseInt(bruto) + parseInt((bruto * (ppnbruto / 100)));
            ket_diskon = parseInt(ket_netto_plus_ppn) * (diskon / 100);
            ket_diskon_netto = parseInt(bruto) * (diskon / 100);
            ket_harga_akhir = ket_netto_plus_ppn - ket_diskon;
            ket_harga_akhir_satuan = ket_harga_akhir / (jumlah_satuan * jumlah_kemasan);

            $('#ket_jumlah_obat').html(ket_jumlah_obat);
            $('#ket_netto_plus_ppn').html('Rp' + ket_netto_plus_ppn);
            $('#ket_diskon').html('Rp' + ket_diskon);
            $('#ket_diskon_netto').html('Rp' + ket_diskon_netto);
            $('#ket_harga_akhir').html('Rp' + ket_harga_akhir);
            $('#ket_harga_akhir_satuan').html('Rp' + ket_harga_akhir_satuan);
        }

        function getData(dataName) {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("dataobat/getData") }}',
                data: {
                    nama: dataName,
                },
                success: function (response) {
                    return response;
                }
            })
        }

        // var hargaperitemready = ($(bruto).val() / $('#jumlah_kemasan').val());
        // $('#hargaperbox').val(hargaperitemready);



    })

</script>
@endpush
