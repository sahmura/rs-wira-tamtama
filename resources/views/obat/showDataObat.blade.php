@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('body')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Keterangan obat <strong>{{ $obat->nama }}</strong></h2>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td style="width: 120px;"><i class="fa fa-medkit"></i> Nama</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $obat->nama }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;"><i class="fa fa-plus-square"></i> Kategori</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ $obat->kategori }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;"><i class="fa fa-ambulance"></i> Stok</td>
                                    <td style="width: 10px;">:</td>
                                    <td>{{ (isset($obat->stok->stok)) ? $obat->stok->stok : 'Data belum tersedia' }}
                                        <button class="btn btn-info btn-sm ml-3" data-id="{{ $obat->id }}" id='fix-btn'>Perbaiki
                                            data</button></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="table-responsive mt-5">
                        <h4 class="mb-3">Data pemasukan</h4>
                        <hr>
                        <table id="obatTable" class="table table-striped table-bordered">
                            <thead>
                                <td>No</td>
                                <td>Tgl Masuk</td>
                                <td>Nomor Faktur</td>
                                <td>PBF</td>
                                <td>Jml Obat Masuk</td>
                                <td>Harga satuan (Rp)</td>
                            </thead>
                        </table>
                    </div>

                    <div class="table-responsive mt-5">
                        <h4 class="mb-3">Data pengeluaran</h4>
                        <hr>
                        <table id="obatTableDistribusi" class="table table-striped table-bordered">
                            <thead>
                                <td>No</td>
                                <td>Tgl. Verifikasi Gudang</td>
                                <td>Distribusi</td>
                                <td>Banyaknya</td>
                                <td>Keterangan</td>
                                <td style="width: 10px;">Aksi</td>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="importDataModal" tabindex="-1" role="dialog" aria-labelledby="importDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importDataModalLabel">Import data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dataobat.importData') }}" method="post" enctype="multipart/form-data" id="importDataForm">
                    <label for="file">Upload file dalam format excel</label>
                    <input type="file" name="file" id="file" class="form-control" required placeholder="File">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info text-white" id="btn-import" form="importDataForm">Import
                    data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
{{-- Data Tables --}}
<script>
    $(document).ready(function() {
        $('#obatTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('dataobat.pemasukan') }}",
                data: {
                    id: "{{ $obat->id }}"
                },
                method: 'POST',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    class: 'text-center',
                    orderable: false,
                    'searchable': false
                },
                {
                    data: 'tanggal_pemasukan',
                    render: function(data, type, row) {
                        var date = new Date(row.tanggal_pemasukan);
                        var monthNames = [
                            "Januari", "Februari", "Maret",
                            "April", "Mei", "Juni", "Juli",
                            "Agustus", "September", "Oktober",
                            "November", "Desember"
                        ];
                        var day = date.getDate();
                        var monthIndex = date.getMonth();
                        var year = date.getFullYear();
                        return day + ' ' + monthNames[monthIndex] + ' ' + year;
                    }
                },
                {
                    data: 'nomor_faktur',
                    name: 'nomor_faktur'
                },
                {
                    data: 'nama_pbf',
                },
                {
                    data: 'jumlah_obat',

                },
                {
                    data: 'harga_akhir_satuan',
                    name: 'harga'
                },
            ]
        });

        $('#obatTableDistribusi').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('dataobat.distribusi') }}",
                method: 'POST',
                data: {
                    id: "{{ $obat->id }}"
                },
            },
            columns: [{
                    data: 'DT_RowIndex',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                    width: '10px'

                },
                {
                    data: 'tanggal_validasi',
                    defaultContent: 'Belum ada data'
                },
                {
                    data: 'distribusi',
                    name: 'distribusi'
                },
                {
                    data: 'total',
                    name: 'total'
                },
                {
                    data: 'keterangan',
                },
                {
                    data: 'action',
                }
            ]
        });
    })
</script>
<script>
    $(document).on('click', '.btn-delete', function() {
        var id = $(this).data('id');
        var token = $(this).data('token');


        swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            text: 'Data yang dihapus tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#e3342f',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{url("dataobat/hapusdata")}}' + '/' + id,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'id': id,
                        '_method': 'POST',
                        '_token': token,
                    },
                    success: function(response) {
                        swal.fire({
                            title: response.message,
                            icon: 'success',
                            timer: 3000,
                        });
                        location.reload();

                    },

                    fail: function() {
                        swal.fire({
                            title: 'Data gagal dihapus',
                            icon: 'error',
                            timer: 3000,
                        });
                        location.reload();
                    },
                });
            }
        })
    });

    $('#fix-btn').on('click', function() {
        var id = $(this).data('id');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                    'content')
            },
            url: '{{url("dataobat/fixdata")}}',
            data: {
                id: id
            },
            method: 'POST',
            success: function(data) {
                swal.fire({
                    title: data.title,
                    text: data.text,
                    icon: data.icon,
                }).then((Confirm) => {
                    window.location.reload();
                })
            }
        });
    });

    $(document).on('click', '.btn-delete-dis', function() {
        var id = $(this).data('id');
        swal.fire({
            title: 'Apakah Anda yakin menghapus data ini?',
            text: 'Data yang dihapus tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#e3342f',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                            'content')
                    },
                    url: '{{url("distribusi/hapusdatadistribusi")}}',
                    data: {
                        id: id
                    },
                    method: 'POST',
                    success: function(data) {
                        swal.fire({
                            title: data.title,
                            text: data.text,
                            icon: data.icon,
                        }).then((Confirm) => {
                            window.location.reload();
                        })
                    }
                });
            }
        })
    })
</script>

@endpush