@extends('layouts.master')

@section('css')
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <a href="{{route('dataobat.index')}}" class="btn btn-danger">Kembali</a>
                    </div>
                    <h2 class="card-title">Sunting <strong>data obat</strong></h2>
                </div>
                <div class="card-body">
                    <form action="{{ route('dataobat.update', $obat->id) }}" method="POST" id="formEditData">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nomor_faktur">Nomor faktur</label>
                                    <input type="text" name="nomor_faktur" id="nomor_faktur" class="form-control"
                                        placeholder="Nomor faktur" value="{{ $obat->nomor_faktur }}">
                                    @error('nomor_faktur')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_pemasukan">Tanggal Pemasukan</label>
                                    <input type="date" name="tanggal_pemasukan" id="tanggal_pemasukan"
                                        class="form-control" placeholder="Tanggal Pemasukan"
                                        value="{{ $obat->tanggal_pemasukan }}">
                                    @error('tanggal_pemasukan')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama obat</label>
                                    <select name="obat_id" id="obat_id" class="custom-select js-example-basic-single"
                                        required>
                                        @foreach($listdataobat as $nama)
                                        <option value="{{$nama->id}}" @if($obat->obat_id == $nama->id)
                                            selected='selected'
                                            @endif>{{$nama->nama}}</option>
                                        @endforeach
                                    </select>
                                    @error('nama')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="pbf">PBF</label>
                                    <select name="pbf_id" id="pbf_id" class="custom-select">
                                        @foreach($pbf as $pbf_data)
                                        <option value="{{ $pbf_data->id }}" @if($obat->pbf_id == $pbf_data->id)
                                            selected='selected' @endif>{{ $pbf_data->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('pbf')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="satuanjenis">Satuan</label>
                                    <select name="satuanjenis" id="satuanjenis" class="custom-select">
                                        @foreach($satuan as $satuan_data)
                                        <option value="{{ $satuan_data->id }}" @if($obat->satuan_id == $satuan_data->id)
                                            selected='selected' @endif>{{ $satuan_data->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('pbf')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_kadaluwarsa">Tanggal kadaluwarsa</label>
                                    <input type="date" name="tanggal_kadaluwarsa" id="tanggal_kadaluwarsa"
                                        class="form-control datepicker" placeholder="Kadaluwarsa"
                                        value="{{ $obat->tanggal_kadaluwarsa }}">
                                    @error('tanggal_kadaluwarsa')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <!-- <div class="form-group">
                                    <label for="kategori">Kategori</label>
                                    <select name="kategori" id="kategori" class="custom-select">
                                        @foreach($kategori as $kat)
                                        <option value="{{ $kat->nama }}">{{ $kat->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('kategori')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div> -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jumlah_kemasan">Jumlah kemasan/box</label>
                                    <input type="number" name="jumlah_kemasan" id="jumlah_kemasan" class="form-control"
                                        placeholder="Jumlah kemasan/box" value="{{ $obat->jumlah_kemasan }}">
                                    @error('jumlah_kemasan')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_satuan">Jumlah satuan</label>
                                    <input type="number" name="jumlah_satuan" id="jumlah_satuan" class="form-control"
                                        placeholder="Jumlah satuan dalam satu kemasan/box" min="0"
                                        value="{{ $obat->jumlah_satuan }}">
                                    @error('jumlah_satuan')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="hargaperbox">Harga per kemasan/box</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp.</div>
                                        <input type="number" name="hargaperbox" id="hargaperbox" step=".01"
                                            class="form-control" placeholder="Harga per kemasan/box">
                                    </div>
                                    @error('hargaperbox')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="bruto">Netto</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp.</div>
                                        <input type="number" name="netto" id="bruto" class="form-control"
                                            placeholder="Netto" min="0" value="{{ $obat->netto }}" step=".01">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                    @error('bruto')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="ppnbruto">PPN Netto</label>
                                    <div class="input-group">
                                        <input type="number" name="ppn_netto" id="ppnbruto" class="form-control"
                                            placeholder="PPN Netto %" value="{{ $obat->ppn_netto }}" step=".01">
                                        <div class="input-group-addon" min="0"">%</div>
                                    </div>
                                    @error('ppnbruto')
                                    <span class=" text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="diskon">Diskon</label>
                                            <div class="input-group">
                                                <input type="number" name="diskon" id="diskon" class="form-control"
                                                    placeholder="Diskon %" min="0" value="{{ $obat->diskon }}"
                                                    step=".00001">
                                                <div class="input-group-addon">%</div>
                                            </div>
                                            @error('diskon')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="ppnharga">Harga akhir manual</label>
                                            <div class="input-group">
                                                <input type="number" name="harga_akhir_manual" id="ppnharga"
                                                    class="form-control" placeholder="Harga akhir manual" min="0"
                                                    value="{{ $obat->harga_akhir_manual }}">
                                            </div>
                                            @error('ppnharga')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                    </form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button class="btn btn-success" id="btn-tambah" type="submit" form="formEditData">Simpan
                                    data obat</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('javascript')
<script>
    $(document).ready(function () {
        $('input[type=number').attr('min', '0');
        $('#hargaperbox').keyup(function () {
            var netto = ($(this).val() * $('#jumlah_kemasan').val());
            $('#bruto').val(netto);

        });

        $('#jumlah_kemasan').keyup(function () {
            var netto = ($(this).val() * $('#hargaperbox').val());
            $('#bruto').val(netto);

        });

        $('#bruto').keyup(function () {
            var hargaperitem = ($(this).val() / $('#jumlah_kemasan').val());
            $('#hargaperbox').val(hargaperitem.toFixed(2));

        });

        $('#hargaperbox').val(($('#bruto').val() / $('#jumlah_kemasan').val()).toFixed(2));
    });

</script>
@endpush
