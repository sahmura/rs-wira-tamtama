@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button type="button" class="btn btn-success btn-tambah">Tambah Permintaan</button>
                    </div>
                    <h2>List <strong>Permintaan</strong></h2>
                </div>
                <div class="row my-3 mx-2">
                    <div class="col-md-8">
                        <button class="btn btn-warning" id="filterBelumSetuju" data-id="0">Belum Disetujui</button>
                        <button class="btn btn-success" id="filterSetuju" data-id="1">Disetujui</button>
                        <button class="btn btn-warning" id="filterBatalkan" data-id="3">Dibatalkan</button>
                        <button class="btn btn-danger" id="filterTolak" data-id="2">Ditolak</button>
                        <button class="btn btn-info" id="filterSemua" data-id="2">Semua Data</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="pbfTable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama obat</td>
                                    <td>Apoteker</td>
                                    <td>Total</td>
                                    <td>Keterangan</td>
                                    <td>Status</td>
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal" id="pbfModal" tabindex="-1" role="dialog" aria-labelledby="pbfModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="pbfModalLabel"><b>Tambah Permintaan</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="formPbf">
                    @csrf
                    <input type="hidden" id="type-action" name="type_action">
                    <input type="hidden" id="hidden_id" name="hidden_id">
                    <input type="hidden" id="_method" name="_method">
                    <input type="hidden" id="isdone" name="isdone">
                    <div class="form-group">
                        <label for="nama">Nama Obat</label><br>
                        <select name="nama" id="nama" class="custom-select js-example-basic-single" style="width: 100%" required>
                            @foreach($obat as $data)
                            <option value="{{ $data->id }}">{{$data->nama}}</option>
                            @endforeach
                        </select>
                        <h6 class="mt-3">Stok di gudang : <b><span id="hasilStok">Pilih obat</span></b></h6>
                    </div>
                    <div class="form-group">
                        <label for="total">Total</label>
                        <input type="number" name="total" id="total" class="form-control" placeholder="Banyaknya obat yang diminta" required>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan" required></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="button-action">Tambah Data</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" id="permintaan-header">
                <h5 class="modal-title text-white" id="feedbackModalLabel"><b>Hasil permintaan</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="lead" id="feedback"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" id="filter-header">
                <h5 class="modal-title text-white" id="filterModalLabel"><b>Filter <span id="jenisFilter"></span></b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="filterForm">
                    @csrf
                    <input type="hidden" name="jenisFilterId" id="jenisFilterId">
                    <div class="form-group">
                        <label for="tanggal_awal">Tanggal Awal</label>
                        <input type="date" name="tanggal_awal" id="tanggal_awal" class="form-control" value="{{ $tanggal_awal }}" required>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_akhir">Tanggal Akhir</label>
                        <input type="date" name="tanggal_akhir" id="tanggal_akhir" class="form-control" value="{{ $tanggal_akhir }}" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-filter-data">Filter data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    function refreshdata() {
        $('#pbfTable').DataTable({
            paginate: true,
            destroy: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('requestObat.index') }}",
                data: {
                    jenis: $('#jenisFilterId').val(),
                    tanggal_awal: $('#tanggal_awal').val(),
                    tanggal_akhir: $('#tanggal_akhir').val(),
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'namaobat',
                },
                {
                    data: 'nama_user',
                },
                {
                    data: 'total',
                },
                {
                    data: 'keterangan',
                },
                {
                    render: function(data, type, row) {
                        if (row.is_done == 0) {
                            return "<span class='badge badge-warning'>Belum disetujui</span>";
                        } else if (row.is_done == 1) {
                            return "<span class='badge badge-success'>Disetujui</span>";
                        } else if (row.is_done == 2) {
                            return "<span class='badge badge-danger'>Ditolak</span>";
                        } else if (row.is_done == 3) {
                            return "<span class='badge badge-warning'>Dibatalkan</span>";
                        }
                    }
                },
                {
                    data: 'action',
                    render: function(data, type, row) {
                        if (row.is_done > 0) {
                            return "<button data-feedback='" + row.feedback +
                                "' class='btn btn-info btn-sm btn-ditolak'><i class='fa fa-search'></i></button>"
                        } else {
                            return row.action;
                        }
                    }
                },
            ]
        });
    }
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
        refreshdata();
        $('#tanggal_awal').val("{{ $tanggal_awal }}");
        $('#tanggal_akhir').val("{{ $tanggal_akhir }}");

        $('.btn-tambah').on('click', function(e) {
            e.preventDefault();
            $('.modal-header').removeClass('bg-warning').addClass('bg-success');
            $('.modal-title').text('Tambah Permintaan Obat');
            $('#type-action').val('add');
            $('#_method').val('POST');
            $('#button-action').text('Tambah Permintaan').removeClass('btn-warning').addClass(
                'btn-success');
            $('#formPbf')[0].reset();
            $('#pbfModal').modal('show');
            $('#nama').select().focus();
        });

        $(document).on('click', '.btn-edit', function() {
            var id = $(this).attr('id');
            var nama = $(this).data('nama');
            var total = $(this).data('total');
            var keterangan = $(this).data('keterangan');
            var isdone = $(this).data('isdone');
            $('.modal-header').addClass('bg-warning');
            $('.modal-title').text('Sunting Permintaan');
            $('#type-action').val('edit');
            $('#hidden_id').val(id);
            $('#nama').val(nama);
            $('#total').val(total);
            $('#keterangan').val(keterangan);
            $('#isdone').val(isdone);
            $('#_method').val('PUT');
            $('#button-action').text('Update Data').addClass('btn-warning').removeClass(
                'btn-success');
            $('#pbfModal').modal('show');
            $('#nama').select().focus();
        })

        $('#formPbf').on('submit', function(e) {
            e.preventDefault();
            var method = '';
            var action = '';
            var id = $('#hidden_id').val();

            if ($('#type-action').val() == 'add') {
                action = '{{ route("requestObat.store") }}';
                method = 'POST';
                $.ajax({
                    url: action,
                    method: method,
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(response) {
                        if (response.status == true) {
                            swal.fire({
                                title: response.message,
                                icon: 'success',
                                // timer: 3000
                            });
                        } else {
                            swal.fire({
                                title: response.message,
                                icon: 'error',
                                // timer: 3000
                            });
                        }
                        refreshdata();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();
                    },
                    fail: function(response) {
                        swal.fire({
                            title: response.message,
                            icon: 'error',
                            // timer: 3000
                        });
                        // location.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();

                    },
                });
            }
            if ($('#type-action').val() == 'edit') {
                action = 'requestObat/' + id;
                method = 'POST';
                data = $(this).serialize();

                $.ajax({
                    url: action,
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(response) {
                        if (response.status == true) {
                            swal.fire({
                                title: response.message,
                                icon: 'success',
                                // timer: 3000
                            });
                        } else {
                            swal.fire({
                                title: response.message,
                                icon: 'error',
                                // timer: 3000
                            });
                        }
                        refreshdata();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();
                    },
                    fail: function() {
                        swal.fire({
                            title: 'Gagal menyunting data',
                            icon: 'error',
                            timer: 3000
                        });
                        refreshdata();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();

                    },
                });
            }
        });

        $(document).on('click', '.btn-delete', function() {
            var id = $(this).data('id');
            var token = $(this).data('token');

            swal.fire({
                title: 'Apakah anda yakin menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#e3342f',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                                'content')
                        },
                        url: '{{url("requestObat/hapusdata")}}' + '/' + id,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id': id,
                            '_method': 'POST',
                            '_token': token,
                        },
                        success: function() {
                            swal.fire({
                                title: 'Data berhasil dihapus',
                                icon: 'success',
                                timer: 3000,
                            });
                            refreshdata();
                        },
                        fail: function() {
                            swal.fire({
                                title: 'Data gagal dihapus',
                                icon: 'error',
                                timer: 3000,
                            });
                            refreshdata();
                        },
                    });
                }
            });
        });
    });

    $(document).on('click', '.btn-ditolak', function() {
        var feedback = $(this).data('feedback');

        $('#feedback').html(feedback);
        $('.modal-title').text('Feedback request');
        $('.modal-header').removeClass('bg-warning');
        $('.modal-header').addClass('bg-info');
        $('#feedbackModal').modal('show');
    });

    $('#filterBelumSetuju').on('click', function() {
        $('#filter-header').removeClass('bg-success');
        $('#filter-header').removeClass('bg-danger');
        $('#filter-header').addClass('bg-warning');
        $('#jenisFilter').html('Yang Belum Disetujui');
        $('#filterForm')[0].reset();
        $('#jenisFilterId').val($(this).data('id'));
        $('#filterModal').modal('show');
    });

    $('#filterSetuju').on('click', function() {
        $('#filter-header').removeClass('bg-warning');
        $('#filter-header').removeClass('bg-danger');
        $('#filter-header').addClass('bg-success');
        $('#jenisFilter').html('Yang Disetujui');
        $('#filterForm')[0].reset();
        $('#jenisFilterId').val($(this).data('id'));
        $('#filterModal').modal('show');
    });

    $('#filterTolak').on('click', function() {
        $('#filter-header').removeClass('bg-success');
        $('#filter-header').removeClass('bg-warning');
        $('#filter-header').addClass('bg-danger');
        $('#jenisFilter').html('Yang Ditolak');
        $('#filterForm')[0].reset();
        $('#jenisFilterId').val($(this).data('id'));
        $('#filterModal').modal('show');
    });

    $('#filterBatalkan').on('click', function() {
        $('#filter-header').removeClass('bg-success');
        $('#filter-header').removeClass('bg-danger');
        $('#filter-header').addClass('bg-warning');
        $('#jenisFilter').html('Yang Dibatalkan');
        $('#filterForm')[0].reset();
        $('#jenisFilterId').val($(this).data('id'));
        $('#filterModal').modal('show');
    });

    $('#filterSemua').on('click', function() {
        $('#jenisFilterId').val('');
        refreshdata();
    });

    $('#btn-filter-data').on('click', function() {
        refreshdata();
        $('#filterModal').modal('hide');
    });

    $('#nama').on('change', function() {
        var id = $(this).val();
        $('#hasilStok').html('Loading...');
        $.ajax({
            url: "{{ url('getStokObat') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                    'content')
            },
            method: 'POST',
            data: {
                obat_id: $(this).val()
            },
            success: function(response) {
                $('#hasilStok').html(response.stok);
            }
        });
    });
</script>
@endpush