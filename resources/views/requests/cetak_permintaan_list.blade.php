@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Cetak List <strong>Permintaan</strong></h2>
                </div>
                <div class="row my-3 mx-2">
                    <div class="col-md-12">
                        <button class="btn btn-warning" id="filterBelumSetuju" data-id="0">Belum Disetujui</button>
                        <button class="btn btn-success" id="filterSetuju" data-id="1">Disetujui</button>
                        <button class="btn btn-danger" id="filterTolak" data-id="2">Ditolak</button>
                        <button class="btn btn-info" id="filterSemua" data-id="2">Semua Data</button>
                        <button class="btn btn-primary float-right" id="cetakData">Cetak Data</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="pbfTable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama obat</td>
                                    <td>Apoteker</td>
                                    <td>Total</td>
                                    <td>Keterangan</td>
                                    <td>Tgl. Request</td>
                                    <td>Tgl. Konfirmasi Gudang</td>
                                    <td>Status</td>
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="detailRequest" tabindex="-1" role="dialog" aria-labelledby="detailRequestLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailRequestLabel"><b>Detail permintaan</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3 id="namaObat"></h3>
                        <p id="apoteker" class="lead"></p>
                        <table class="table table-striped">
                            <thead>
                                <th>Total</th>
                                <th>Keterangan</th>
                                <th>Tersedia</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="totalRequest"></td>
                                    <td id="keteranganRequest"></td>
                                    <td id="stokRequest"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post" id="requestForm">
                            @csrf
                            @method('post')
                            <input type="hidden" name="idRequest" id="idRequest">
                            <input type="hidden" name="id_pemesan" id="id_pemesan">
                            <div class="form-group">
                                <label for="totalObatRequest"><b>Total disetujui</b></label>
                                <input type="number" name="totalObatRequest" id="totalObatRequest" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="feedbackRequest"><b>Feedback</b></label>
                                <textarea type="text" name="feedbackRequest" id="feedbackRequest" class="form-control" placeholder="Tulis pesan"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_validasi">Tanggal Disetujui</label>
                                <input type="date" name="tanggal_validasi" id="tanggal_validasi" class="form-control">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <span class="text-danger">Data yang disetujui <b>tidak bisa diubah kembali</b></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-tolak">Tolak</button>
                <button type="button" class="btn btn-primary btn-setuju">Setujui</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" id="filter-header">
                <h5 class="modal-title text-white" id="filterModalLabel"><b>Filter <span id="jenisFilter"></span></b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="filterForm">
                    @csrf
                    <input type="hidden" name="jenisFilterId" id="jenisFilterId">
                    <div class="form-group">
                        <label for="tanggal_awal">Tanggal Awal</label>
                        <input type="date" name="tanggal_awal" id="tanggal_awal" class="form-control" value="{{ $tanggal_awal }}" required>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_akhir">Tanggal Akhir</label>
                        <input type="date" name="tanggal_akhir" id="tanggal_akhir" class="form-control" value="{{ $tanggal_akhir }}" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-filter-data">Filter data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script>
    function refreshdata() {
        return $('#pbfTable').DataTable({
            paginate: true,
            info: true,
            destroy: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [0, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('cetak/antrianObatIndex') }}",
                data: {
                    jenis: $('#jenisFilterId').val(),
                    tanggal_awal: $('#tanggal_awal').val(),
                    tanggal_akhir: $('#tanggal_akhir').val(),
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                    width: '10px'

                },
                {
                    data: 'nama_obat',
                },
                {
                    data: 'nama_user',
                },
                {
                    data: 'total',
                },
                {
                    data: 'keterangan',
                },
                {
                    data: 'tanggal_request',
                    defaultContent: 'Belum ada data'
                },
                {
                    data: 'tanggal_validasi',
                    defaultContent: 'Belum ada data'
                },
                {
                    render: function(data, type, row) {
                        if (row.is_done == 0) {
                            return "<span class='badge badge-warning'>Belum disetujui</span>";
                        } else if (row.is_done == 1) {
                            return "<span class='badge badge-success'>Disetujui</span>";
                        } else if (row.is_done == 2) {
                            return "<span class='badge badge-danger'>Ditolak</span>";
                        }
                    }
                },
                {
                    data: 'action',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                },
            ]
        });
    }
    $(document).ready(function() {
        var table = refreshdata();
        var notIncluded = Array();

        $('#pbfTable').on('click', '.btn-remove', function() {
            $(this).removeClass('btn-remove');
            $(this).removeClass('btn-danger');
            $(this).html('<i class="fa fa-check"></i>');
            $(this).addClass('btn-include');
            $(this).addClass('btn-success');
            notIncluded.pop($(this).data('id'));
            console.log(notIncluded);
        });

        $('#pbfTable').on('click', '.btn-include', function() {
            $(this).removeClass('btn-include');
            $(this).removeClass('btn-success');
            $(this).html('<i class="fa fa-times"></i>');
            $(this).addClass('btn-remove');
            $(this).addClass('btn-danger');
            notIncluded.push($(this).data('id'));
            console.log(notIncluded);
        });

        $('#cetakData').on('click', function() {
            var tanggal_awal = $('#tanggal_awal').val();
            var tanggal_akhir = $('#tanggal_akhir').val();
            var jenisFilterId = $('#jenisFilterId').val();

            if (notIncluded.length == 0) {
                window.open("{{ url('cetak/antrianObat') }}" + '?awal=' + tanggal_awal + '&akhir=' +
                    tanggal_akhir +
                    '&jenis=' + jenisFilterId);
            } else {
                var notPrinted = notIncluded.join();
                window.open("{{ url('cetak/antrianObat') }}" + '?awal=' + tanggal_awal + '&akhir=' +
                    tanggal_akhir +
                    '&jenis=' + jenisFilterId + '&notin=' + notPrinted);
            }
        });
    });

    $('#filterBelumSetuju').on('click', function() {
        $('#filter-header').removeClass('bg-success');
        $('#filter-header').removeClass('bg-danger');
        $('#filter-header').addClass('bg-warning');
        $('#jenisFilter').html('Yang Belum Disetujui');
        $('#filterForm')[0].reset();
        $('#jenisFilterId').val($(this).data('id'));
        $('#filterModal').modal('show');
    });

    $('#filterSetuju').on('click', function() {
        $('#filter-header').removeClass('bg-warning');
        $('#filter-header').removeClass('bg-danger');
        $('#filter-header').addClass('bg-success');
        $('#jenisFilter').html('Yang Disetujui');
        $('#filterForm')[0].reset();
        $('#jenisFilterId').val($(this).data('id'));
        $('#filterModal').modal('show');
    });

    $('#filterTolak').on('click', function() {
        $('#filter-header').removeClass('bg-success');
        $('#filter-header').removeClass('bg-warning');
        $('#filter-header').addClass('bg-danger');
        $('#jenisFilter').html('Yang Ditolak');
        $('#filterForm')[0].reset();
        $('#jenisFilterId').val($(this).data('id'));
        $('#filterModal').modal('show');
    });

    $('#filterSemua').on('click', function() {
        $('#jenisFilterId').val('');
        refreshdata();
    });

    $('#btn-filter-data').on('click', function() {
        refreshdata();
        $('#filterModal').modal('hide');
        // console.log($('#tanggal_awal').val());
    });
</script>
@endpush