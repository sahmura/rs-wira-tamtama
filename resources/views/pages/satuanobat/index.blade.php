@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button type="button" class="btn btn-success btn-tambah">Tambah Data</button>
                    </div>
                    <h2>List <strong>Satuan Obat</strong></h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="pbfTable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Satuan</td>
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal" id="pbfModal" tabindex="-1" role="dialog" aria-labelledby="pbfModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="pbfModalLabel"><b>Tambah Data</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="formPbf">
                    @csrf
                    <input type="hidden" id="type-action" name="type_action">
                    <input type="hidden" id="hidden_id" name="hidden_id">
                    <input type="hidden" id="_method" name="_method">
                    <div class="form-group">
                        <label for="nama">Nama Satuan</label>
                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Satuan"
                            required>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="button-action">Tambah Data</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script>
    $(document).ready(function () {
        var table = $('#pbfTable').DataTable({
            paginate: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('satuanobat.index') }}",
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'action',
                    orderable: false,
                    width: '20px'

                }
            ]
        });

        $('.btn-tambah').on('click', function (e) {
            e.preventDefault();
            $('.modal-header').removeClass('bg-warning').addClass('bg-success');
            $('.modal-title').text('Tambah Satuan Obat baru');
            $('#type-action').val('add');
            $('#_method').val('POST');
            $('#button-action').text('Tambah Satuan').removeClass('btn-warning').addClass(
                'btn-success');
            $('#formPbf')[0].reset();
            $('#pbfModal').modal('show');
            $('#nama').select();
            $('#nama').focus();
        });

        $(document).on('click', '.btn-edit', function () {
            var id = $(this).attr('id');
            var nama = $(this).data('nama');
            $('.modal-header').addClass('bg-warning');
            $('.modal-title').text('Sunting Satuan Obat');
            $('#type-action').val('edit');
            $('#hidden_id').val(id);
            $('#nama').val(nama);
            $('#_method').val('PUT');
            $('#button-action').text('Update Satuan').addClass('btn-warning').removeClass(
                'btn-success');
            $('#pbfModal').modal('show');
            $('#nama').select();
            $('#nama').focus();
        })

        $('#formPbf').on('submit', function (e) {
            e.preventDefault();
            var method = '';
            var action = '';
            var id = $('#hidden_id').val();

            if ($('#type-action').val() == 'add') {
                action = '{{ route("satuanobat.store") }}';
                method = 'POST';
                $.ajax({
                    url: action,
                    method: method,
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.status == true) {
                            swal.fire({
                                title: response.message,
                                icon: 'success',
                                // timer: 3000
                            });
                        } else {
                            swal.fire({
                                title: response.message,
                                icon: 'error',
                                // timer: 3000
                            });
                        }
                        table.ajax.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();
                    },
                    fail: function (response) {
                        swal.fire({
                            title: response.message,
                            icon: 'error',
                            // timer: 3000
                        });
                        // location.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();

                    },
                });
            }
            if ($('#type-action').val() == 'edit') {
                action = 'satuanobat/' + id;
                method = 'POST';
                data = $(this).serialize();
                console.log(data);

                $.ajax({
                    url: action,
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.status == true) {
                            swal.fire({
                                title: response.message,
                                icon: 'success',
                                // timer: 3000
                            });
                        } else {
                            swal.fire({
                                title: response.message,
                                icon: 'error',
                                // timer: 3000
                            });
                        }
                        table.ajax.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();
                    },
                    fail: function () {
                        swal.fire({
                            title: 'Gagal menyunting data',
                            icon: 'error',
                            timer: 3000
                        });
                        table.ajax.reload();
                        $('#pbfModal').modal('hide');
                        $('#formPbf').reset();

                    },
                });
            }
        });

        $(document).on('click', '.btn-delete', function () {
            var id = $(this).data('id');
            var token = $(this).data('token');

            swal.fire({
                title: 'Apakah anda yakin menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#e3342f',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr(
                                'content')
                        },
                        url: '{{url("satuanobat/hapusdata")}}' + '/' + id,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id': id,
                            '_method': 'POST',
                            '_token': token,
                        },
                        success: function () {
                            swal.fire({
                                title: 'Data berhasil dihapus',
                                icon: 'success',
                                timer: 3000,
                            });
                            table.ajax.reload();
                        },
                        fail: function () {
                            swal.fire({
                                title: 'Data gagal dihapus',
                                icon: 'error',
                                timer: 3000,
                            });
                            table.ajax.reload();
                        },
                    });
                }
            });
        });
    })

</script>
@endpush
