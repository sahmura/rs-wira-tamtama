@extends('layouts.master')

@section('css')
<!-- code css here -->
{{-- Data Tables CSS --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <!-- <div class="float-right">
                        <button type="button" class="btn btn-success btn-tambah">Tambah distribusi</button>
                    </div> -->
                    <h2>Riwayat data <strong>distribusi obat</strong></h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama</td>
                                    <td>Distribusi</td>
                                    <td>Banyaknya</td>
                                    <td>Keterangan</td>
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<!-- javascript body here -->
<script>
    $(document).ready(function() {
        var table = $('#dataTable').DataTable({
            paginate: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('distribusi.index') }}",
                // method: 'POST',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                    width: '10px'

                },
                {
                    data: 'nama_obat',
                },
                {
                    data: 'distribusi',
                    name: 'distribusi'
                },
                {
                    data: 'total',
                    name: 'total'
                },
                {
                    data: 'keterangan',
                },
                {
                    data: 'action',
                    class: 'text-center',
                    orderable: false,
                    searchable: false,
                    width: '20px'
                }
            ]
        });
    })
</script>
@endpush