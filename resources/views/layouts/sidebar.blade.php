    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('assets/images/logo-rs-wira-tamtama.png') }}" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="{{ asset('assets/images/logo2-wira-tamtama.png') }}" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ route('home') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard
                        </a></li>
                    @if(auth()->user()->role === 'admin' OR auth()->user()->role === 'system')
                    <li class="menu-item-has-children dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Admin</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-hospital-o"></i><a href="{{ROUTE('pbf.index')}}">PBF</a></li>
                            <li><i class="menu-icon fa fa-inbox"></i><a href="{{ROUTE('satuanobat.index')}}">Satuan
                                    Obat</a></li>
                            <li><i class="menu-icon fa fa-medkit"></i><a href="{{ROUTE('obat.index')}}">Data Obat</a>
                            </li>
                            <li><i class="menu-icon fa fa-bars"></i><a href="{{ route('kategori.index') }}">Kategori</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-medkit"></i>Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-square"></i><a href="{{route('dataobat.index')}}">Data
                                    Pemasukan</a></li>
                            <li><i class="menu-icon fa fa-ambulance"></i><a href="{{ROUTE('distribusi.index')}}">Riwayat
                                    Distribusi</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('requestObat.admin') }}"> <i class="menu-icon fa fa-sort-amount-asc"></i>Antrian Obat</a></li>
                    <li class="menu-item-has-children dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-print"></i>Cetak Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sort-amount-asc"></i><a href="{{url('cetak/antrianObatIndex')}}">Antrian Obat</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('users.index') }}"> <i class="menu-icon fa fa-user"></i>Users</a></li>
                    @endif

                    @if(auth()->user()->role != 'admin' OR auth()->user()->role === 'system')
                    <li><a href="{{ route('apoteker.index') }}"> <i class="menu-icon fa fa-suitcase"></i>Stok obat</a>
                    </li>
                    <!-- <li><a href="{{ route('apoteker.listDistribusi') }}"> <i class="menu-icon fa fa-ambulance"></i>Distribusi</a></li> -->
                    <li><a href="{{ route('requestObat.index') }}"> <i class="menu-icon fa fa-medkit"></i>Permintaan
                            Obat</a></li>
                    <li><a href="{{ url('stokopname') }}"><i class="menu-icon fa fa-archive"></i> Stok Opname</a></li>
                    @endif

                    @if(auth()->user()->role === 'ruangoperasi' OR auth()->user()->role === 'system')
                    <li><a href="{{ url('laporanpemakaianobat') }}"><i class="menu-icon fa fa-print"></i> Lap. Pemakaian Obat</a></li>
                    <li><a href="{{ url('groupData') }}"><i class="menu-icon fa fa-archive"></i> Group</a></li>
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->