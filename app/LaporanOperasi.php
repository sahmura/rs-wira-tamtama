<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaporanOperasi extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'nama',
        'tipe_pasien',
        'status',
        'pangkat_pekerjaan',
        'alamat',
        'macam_operasi',
        'golongan_operasi',
        'tanggal_operasi',
        'dokter_id',
        'dokter_id_bedah',
        'dokter_id_anestesi',
        'perawat_bedah',
        'perawat_anestesi',
        'sewa_alat',
        'sewa_ok',
        'biaya_lain',
        'jenis_kelamin',
        'tanggal_lahir',
        'jasa_dokter_bedah',
        'jasa_dokter_anestesi',
        'jasa_dokter_spesialis',
        'jasa_perawat_bedah',
        'jasa_perawat_anestesi'
    ];

    protected $appends = ['harga_satuan_terakhir'];

    public function dataObats()
    {
        return $this->belongsToMany('App\dataObat', 'laporanoperasi_obat', 'laporan_id', 'obat_id')
            ->whereNull('laporanoperasi_obat.deleted_at');
    }

    public function obatOne()
    {
        return $this->hasOne('App\DataPemasukan', 'obat_id', 'obat_id')->latest();
    }

    public function getHargaSatuanTerakhirAttribute()
    {
        if (isset($this->obatOne)) {
            return $this->obatOne->harga_akhir_satuan;
        } else {
            return 0;
        }
    }

    public function dokter()
    {
        return $this->hasOne('App\DataDokter', 'id', 'dokter_id');
    }
    public function dokter_bedah()
    {
        return $this->hasOne('App\DataDokter', 'id', 'dokter_id_bedah');
    }
    public function dokter_anestesi()
    {
        return $this->hasOne('App\DataDokter', 'id', 'dokter_id_anestesi');
    }
}
