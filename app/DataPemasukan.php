<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataPemasukan extends Model
{
    use SoftDeletes;
    protected $table = 'data_pemasukan';
    protected $connection = 'mysql';
    protected $fillable = [
        'id',
        'obat_id',
        'satuan_id',
        'nomor_faktur',
        'tanggal_pemasukan',
        'pbf_id',
        'tanggal_kadaluwarsa',
        'jumlah_kemasan',
        'jumlah_satuan',
        'netto',
        'diskon',
        'harga_akhir_manual',
        'ppn_netto'
    ];

    protected $appends = ['harga_akhir', 'harga_akhir_satuan', 'jumlah_obat', 'nama_obat', 'nama_pbf', 'auto_stok'];

    public function obat()
    {
        return $this->BelongsTo('App\dataObat', 'obat_id', 'id');
    }

    public function pbf()
    {
        return $this->BelongsTo('App\Pbf', 'pbf_id', 'id');
    }

    public function getHargaAkhirAttribute()
    {
        $ket_jumlah_obat = $this->jumlah_kemasan * $this->jumlah_satuan;
        $ket_netto_plus_ppn = ($this->netto) + ($this->netto * ($this->ppn_netto / 100));
        $ket_diskon = ($ket_netto_plus_ppn) * ($this->diskon / 100);
        $ket_diskon_netto = ($this->netto) * ($this->diskon / 100);
        $ket_harga_akhir = $ket_netto_plus_ppn - $ket_diskon;
        $ket_harga_akhir_satuan = $ket_harga_akhir / ($this->jumlah_satuan * $this->jumlah_kemasan);
        return number_format($ket_harga_akhir, 2, ',', '.');
    }

    public function getHargaAkhirSatuanAttribute()
    {
        $ket_jumlah_obat = $this->jumlah_kemasan * $this->jumlah_satuan;
        $ket_netto_plus_ppn = ($this->netto) + ($this->netto * ($this->ppn_netto / 100));
        $ket_diskon = ($ket_netto_plus_ppn) * ($this->diskon / 100);
        $ket_diskon_netto = ($this->netto) * ($this->diskon / 100);
        $ket_harga_akhir = $ket_netto_plus_ppn - $ket_diskon;
        $ket_harga_akhir_satuan = $ket_harga_akhir / ($this->jumlah_satuan * $this->jumlah_kemasan);
        return number_format($ket_harga_akhir_satuan, 2, ',', '.');
    }

    public function getJumlahObatAttribute()
    {
        return $this->jumlah_kemasan * $this->jumlah_satuan;
    }

    public function getNamaObatAttribute()
    {
        return (isset($this->obat->nama)) ? $this->obat->nama : 'Belum tersedia / dihapus';
    }

    public function getNamaPbfAttribute()
    {
        return (isset($this->pbf->nama)) ? $this->pbf->nama : 'Belum tersedia';
    }

    public function satuan()
    {
        return $this->belongsTo('App\SatuanJenis', 'satuan_id', 'id');
    }

    public function stok()
    {
        return $this->hasOne('App\Stok', 'obat_id', 'obat_id');
    }

    public function stokApoteker()
    {
        return $this->hasOne('App\StokApotekersModel', 'obat_id', 'obat_id');
    }

    public function getAutoStokAttribute()
    {
        return $this->stok ? $this->stok->stok : 'Belum tersedia';
    }
}
