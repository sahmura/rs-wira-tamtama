<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name'
    ];

    public function dataObat()
    {
        return $this->belongsToMany('App\dataObat', 'group_obat', 'group_id', 'obat_id')
            ->whereNull('group_obat.deleted_at');
    }
}
