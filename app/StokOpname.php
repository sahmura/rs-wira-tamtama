<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StokOpname extends Model
{
    protected $fillable = [
        'user_id',
        'obat_id',
        'stok_opname',
        'periode',
        'stok_awal',
        'harga_obat',
        'data_pemasukan_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function obat()
    {
        return $this->belongsTo('App\dataObat', 'obat_id', 'id');
    }

    public function stokObat()
    {
        return $this->belongsTo('App\StokApotekersModel', 'obat_id', 'obat_id')
            ->where('stok_apotekers.user_id', Auth()->user()->id);
    }

    public function datapemasukan()
    {
        return $this->belongsTo('App\DataPemasukan', 'data_pemasukan_id', 'id');
    }
}
