<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SatuanJenis extends Model
{
    protected $table = 'satuan_jenis';
    protected $fillable = [
        'id', 'nama'
    ];
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
