<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaporanOperasiObat extends Model
{
    use SoftDeletes;
    protected $table = 'laporanoperasi_obat';
    protected $fillable = [
        'laporan_id',
        'obat_id',
        'pemakaian'
    ];
}
