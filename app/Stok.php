<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\CssSelector\Node\FunctionNode;

class Stok extends Model
{
    protected $table = 'stok';
    protected $fillable = [
        'id', 'stok', 'obat_id'
    ];

    public function obat()
    {
        return $this->belongsTo('App\Obat');
    }

    public function dataObat()
    {
        return $this->belongsTo('App\dataObat', 'obat_id', 'id');
    }

    public function dataPemasukan()
    {
        return $this->belongsTo('App\DataPemasukan', 'obat_id', 'id');
    }
}
