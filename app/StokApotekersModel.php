<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StokApotekersModel extends Model
{
    use SoftDeletes;
    protected $table = 'stok_apotekers';
    protected $fillable = [
        'obat_id', 'user_id', 'stok'
    ];

    public function dataObat()
    {
        return $this->belongsTo('App\dataObat', 'obat_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function dataPemasukan()
    {
        return $this->belongsTo('App\DataPemasukan', 'obat_id', 'obat_id');
    }
}
