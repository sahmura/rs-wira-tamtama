<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class dataObat extends Model
{
    use SoftDeletes;

    protected $table = 'data_obat';
    protected $fillable = [
        'id', 'nama', 'pbf', 'kategori'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $appends = ['harga_satuan_terakhir'];

    public function obat()
    {
        return $this->hasMany('App\DataPemasukan', 'obat_id');
    }

    public function stok()
    {
        return $this->hasOne('App\Stok', 'obat_id', 'id');
    }

    public function distribusi()
    {
        return $this->hasMany('App\Distribusi', 'obat_id', 'id');
    }

    public function requests()
    {
        return $this->hasMany('App\Requests', 'obat_id', 'id');
    }

    public function stokApotekers()
    {
        return $this->belongsTo('App\StokApotekersModel', 'obat_id', 'id');
    }

    public function distribusiApotekers()
    {
        return $this->hasMany('App\DistribusiApotekersModel', 'obat_id', 'id');
    }

    public function obatOne()
    {
        return $this->hasOne('App\DataPemasukan', 'obat_id')->latest();
    }

    public function getSatuanObatAttribute()
    {
        if (isset($this->obatOne->satuan_id)) {
            return $this->obatOne->satuan_id;
        } else {
            return 0;
        }
    }

    public function getKadaluwarsaObatAttribute()
    {
        if (isset($this->obatOne->tanggal_kadaluwarsa)) {
            return Carbon::parse($this->obatOne->tanggal_kadaluwarsa)->locale('id')->isoFormat('Do MMMM YYYY');
        } else {
            return 'Belum ditentukan';
        }
    }

    public function getNamaSatuanAttribute()
    {
        if ($this->satuan_obat != null) {
            return $this->belongsTo('App\SatuanJenis', 'satuan_obat', 'id')->first()->nama;
        } else {
            return 'Belum ditentukan';
        }
    }

    public function getHargaSatuanTerakhirAttribute()
    {
        if (isset($this->obatOne->harga_akhir_satuan)) {
            return $this->obatOne->harga_akhir_satuan;
        } else {
            return 0;
        }
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'group_obat', 'obat_id', 'group_id');
    }

    public function laporanOperasi()
    {
        return $this->belongsToMany('App\LaporanOperasi', 'laporanoperasi_obat', 'obat_id', 'laporan_id');
    }
}
