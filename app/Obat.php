<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Obat extends Model
{
    use SoftDeletes;
    protected $table = 'obat';
    protected $fillable = [
        'id', 'user_id', 'id_obat', 'nomor_faktur', 'nama', 'tanggal_kadaluwarsa', 'kategori', 'jumlah_kemasan', 'jumlah_satuan', 'jumlah_obat', 'satuan', 'bruto', 'ppn_bruto', 'total_bruto', 'bruto_fix', 'diskon', 'total_diskon', 'harga_diskon', 'harga_satuan', 'ppn_harga', 'harga', 'created_at', 'ppn_bruto_persen', 'ppn_harga_persen', 'pbf_id'
    ];
    protected $hidden = [
        'updateed_at',
        'deleted_at',
    ];
    protected $dates = ['deleted_at'];
    protected $appends = ['auto_stok'];

    public function stok()
    {
        return $this->hasOne('App\Stok', 'obat_id', 'id');
    }

    public function distribusi()
    {
        return $this->belongsTo('App\Distribusi', 'obat_id');
    }

    public function data_obat()
    {
        return $this->belongsTo('App\dataObat', 'id_obat');
    }

    public function pbf()
    {
        return $this->belongsTo('App\Pbf', 'pbf_id', 'id');
    }

    public function stokApoteker()
    {
        return $this->hasOne('App\StokApotekersModel', 'obat_id', 'id');
    }

    public function getAutoStokAttribute()
    {
        return $this->stok->first()->stok;
    }