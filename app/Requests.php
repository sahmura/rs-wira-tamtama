<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Requests extends Model
{
    use SoftDeletes;
    protected $table = "requests";
    protected $fillable = [
        'user_id', 'obat_id', 'total', 'keterangan', 'is_done', 'feedback', 'tanggal_request', 'tanggal_validasi'
    ];

    public function dataObat()
    {
        return $this->belongsTo('App\dataObat', 'obat_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function stokOpname()
    {
        return $this->belongsTo('App\StokOpname', 'obat_id', 'obat_id');
    }
}
