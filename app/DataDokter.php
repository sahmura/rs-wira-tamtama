<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataDokter extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'nama'
    ];

    public function laporan()
    {
        return $this->belongsTo('App\LaporanOperasi', 'dokter_id', 'id');
    }
}
