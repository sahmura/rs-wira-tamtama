<?php

namespace App\Imports;

use App\Obat;
use App\Pbf;
use App\Stok;
use App\dataObat;
use App\SatuanJenis;
use App\Kategori;
use Exception;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//TODO: Memperbaiki fitur import

class ObatImport implements ToModel, WithStartRow
{
    /**
     * Fungsi untuk mengimport data ke dalam database dari excel
     * 
     * @param array $row berisi data yang diambil dari database
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $nomor_faktur = $row[1];
        $nama = $row[2];
        $pbf = $row[3];
        $tanggal_kadaluwarsa = date('Y-d-m H:i:s', strtotime($row[4]));
        $kategori = $row[5];
        $jumlah_kemasan = $row[6];
        $jumlah_satuan = $row[7];
        $satuan = $row[8];
        $bruto = $row[9];
        $diskon = $row[10];
        $ppnbruto = $row[11];
        $ppnharga = $row[12];

        $checkObat          = dataObat::where('nama', '=', $nama)->where('deleted_at', null);

        $jumlah_obat        = $jumlah_kemasan * $jumlah_satuan;
        $jumlah_diskon      = $bruto * $diskon / 100;
        $harga_diskon       = $bruto - $jumlah_diskon;
        $ppn_bruto          = $harga_diskon * $ppnbruto / 100;
        $total_bruto        = $bruto + $ppn_bruto - $jumlah_diskon;
        $bruto_fix          = $total_bruto * $jumlah_kemasan;
        $harga_satuan       = $harga_diskon / $jumlah_satuan;
        $ppn_harga          = $harga_satuan * $ppnharga / 100;
        $harga              = $harga_satuan + $ppn_harga;

        if ($checkObat->count() == 0) {
            DB::beginTransaction();
            try {
                $dataObat = dataObat::create(
                    [
                        'nama' => $nama,
                        'pbf' => $pbf,
                        'kategori' => $kategori
                    ]
                );
                $obat = Obat::create(
                    [
                        "user_id" => Auth::id(),
                        "nomor_faktur" => $nomor_faktur,
                        "id_obat" => $dataObat->id,
                        "tanggal_kadaluwarsa" => $tanggal_kadaluwarsa,
                        "jumlah_kemasan" => $jumlah_kemasan,
                        "jumlah_satuan" => $jumlah_satuan,
                        "jumlah_obat" => $jumlah_obat,
                        "satuan" => $satuan,
                        "bruto" => $bruto,
                        "ppn_bruto" => $ppn_bruto,
                        "total_bruto" => $total_bruto,
                        "bruto_fix" => $bruto_fix,
                        "diskon" => $diskon,
                        "total_diskon" => $jumlah_diskon,
                        "harga_diskon" => $harga_diskon,
                        "harga_satuan" => $harga_satuan,
                        "ppn_harga" => $ppn_harga,
                        "harga" => $harga,
                        "ppn_bruto_persen" => $ppnbruto,
                        "ppn_harga_persen" => $ppnharga,
                    ]
                );
            } catch (Exception $e) {
                DB::rollback();
                throw $e;
            }

            DB::commit();
            return Stok::create(
                [
                    'obat_id' => $dataObat->id,
                    'stok' => $jumlah_obat
                ]
            );
        } else {

            DB::beginTransaction();
            try {
                $checkObat = $checkObat->first();
                $obat = Obat::create(
                    [
                        "user_id" => Auth::id(),
                        "nomor_faktur" => $nomor_faktur,
                        "id_obat" => $checkObat->id,
                        "tanggal_kadaluwarsa" => $tanggal_kadaluwarsa,
                        "jumlah_kemasan" => $jumlah_kemasan,
                        "jumlah_satuan" => $jumlah_satuan,
                        "jumlah_obat" => $jumlah_obat,
                        "satuan" => $satuan,
                        "bruto" => $bruto,
                        "ppn_bruto" => $ppn_bruto,
                        "total_bruto" => $total_bruto,
                        "bruto_fix" => $bruto_fix,
                        "diskon" => $diskon,
                        "total_diskon" => $jumlah_diskon,
                        "harga_diskon" => $harga_diskon,
                        "harga_satuan" => $harga_satuan,
                        "ppn_harga" => $ppn_harga,
                        "harga" => $harga,
                        "ppn_bruto_persen" => $ppnbruto,
                        "ppn_harga_persen" => $ppnharga,
                    ]
                );
            } catch (Exception $e) {
                DB::rollback();
                throw $e;
            }

            DB::commit();
            // return Stok::create(
            //     [
            //         'obat_id' => $checkObat->id,
            //         'stok' => $jumlah_obat
            //     ]
            // );
            $checkStok = Stok::where('obat_id', '=', $checkObat->id);
            $stokObat = $checkStok->first();
            if ($stokObat == null) {
                $stokObatLama = 0;
            } else {
                $stokObatLama = $stokObat->stok;
            }
            return Stok::updateOrCreate(
                [
                    'obat_id' => $checkObat->id,
                ],
                [
                    'stok' => (int) $stokObatLama + $jumlah_obat
                ]
            );
        }
    }

    /**
     * Untuk menskip data yang ada headingnya
     *  
     * @return integer
     */
    public function startRow(): int
    {
        return 2;
    }
}
