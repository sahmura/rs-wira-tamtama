<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupObat extends Model
{
    use SoftDeletes;
    protected $table = 'group_obat';
    protected $fillable = [
        'group_id',
        'obat_id'
    ];
}
