<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DistribusiApotekersModel extends Model
{
    use SoftDeletes;
    protected $table = 'distribusi_apotekers';
    protected $fillable = [
        'obat_id', 'user_id', 'total', 'distribusi', 'keterangan'
    ];

    public function dataObat()
    {
        return $this->belongsTo('App\dataObat', 'obat_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
