<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Distribusi;
use App\dataObat;
use App\Stok;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use DB;


class DistribusiController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param object $request menerima request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            // $distribusi = Distribusi::where('deleted_at', null)->with('dataObat.stok')->latest()->get();
            $distribusi = DB::select('select distribusi.obat_id, data_obat.nama as nama_obat, distribusi.distribusi, distribusi.total, distribusi.keterangan from distribusi join data_obat on data_obat.id = distribusi.obat_id join stok on stok.obat_id = data_obat.id order by distribusi.id desc');
            return DataTables::of($distribusi)
                ->addColumn(
                    'action',
                    function ($distribusi) {
                        return '<a href="' . url('dataobat/' . $distribusi->obat_id) . '" class="btn btn-sm btn-info"><i class="fa fa-search"></i></a>';
                    }
                )
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        $obat = dataObat::all();
        return view('distribusi.distribusi_list', compact('obat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param object $request meneirma request
     * 
     * @return mixed
     */
    public function store(Request $request)
    {

        $stok = dataObat::with('stok')->where('id', $request->obat_id)->get();

        if ($stok[0]->stok->stok <= $request->total) {
            $response = [
                'status' => false,
                'message' => 'Stok obat tidak mencukupi'
            ];
        } else {
            $sisa = Stok::where('obat_id', $request->obat_id)->update(
                [
                    'stok' => $stok[0]->stok->stok - $request->total
                ]
            );

            if ($sisa) {
                $distribusi = new Distribusi();

                $distribusi->user_id = Auth::id();
                $distribusi->obat_id = $request->obat_id;
                $distribusi->distribusi = $request->distribusi;
                $distribusi->total = $request->total;
                $distribusi->keterangan = $request->keterangan;
                $save = $distribusi->save();

                if ($save) {
                    $response = [
                        'status' => true,
                        'message' => 'Data berhasil disimpan'
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Data gagal disimpan'
                    ];
                }
            } else {
                $response = [
                    'status' => false,
                    'message' => 'Gagal menghubungkan data stok'
                ];
            }
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id menerima id
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param object $request menerima request
     * @param int    $id      menerima id
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $stok = dataObat::with('stok')->where('id', $request->obat_id)->first();
        $stok = Stok::where('obat_id', $request->obat_id)->first();
        $stokSebelumnya = Distribusi::find($id);

        if (($stok->stok + $request->totalSebelumnya)  <= $request->total) {
            $response = [
                'status' => false,
                'message' => 'Stok obat tidak mencukupi'
            ];
        } else {
            $stokTambah = Stok::where('obat_id', $request->obat_id);
            $stokTambah->update(
                [
                    'stok' => $stok->stok + $request->totalSebelumnya
                ]
            );
            $stokSetelahUpdate = $stokTambah->first();

            if ($stokTambah) {
                $updateData = $stokSebelumnya->update(
                    [
                        'user_id' => Auth::id(),
                        'obat_id' => $request->obat_id,
                        'total' => $request->total,
                        'keterangan' => $request->keterangan,
                    ]
                );

                if ($updateData) {
                    $stokKurang = Stok::where('obat_id', $request->obat_id)->update(
                        [
                            'stok' => $stokSetelahUpdate->stok - $request->total
                        ]
                    );

                    $response = [
                        'status' => true,
                        'message' => 'Data berhasil disunting'
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Data gagal disunting'
                    ];
                }
            }
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id menerima id
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obat_id = Distribusi::find($id);
        $stok = Stok::where('obat_id', $obat_id->obat_id);
        $stokAwal = $stok->get();
        $stokUpdate = $stok->update(
            [
                'stok' => $stokAwal[0]->stok + $obat_id->total
            ]
        );

        if ($stok) {
            $obat_id->delete();
            return response()->json(['success' => 'Data berhasil dihapus, stok dikembalikan']);
        }
    }

    public function hapusdatadistribusi(Request $request)
    {
        try {
            DB::beginTransaction();
            $obat_id = Distribusi::find($request->id);
            $stok = Stok::where('obat_id', $obat_id->obat_id);
            $stokAwal = $stok->get();
            $stokUpdate = $stok->update(
                [
                    'stok' => $stokAwal[0]->stok + $obat_id->total
                ]
            );
            $obat_id->delete();
            DB::commit();
            $res = [
                'title' => 'Berhasil menghapus data',
                'text' => 'Stok dikembalikan',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $res = [
                'title' => 'Berhasil menghapus data',
                'text' => $e->getMessage(),
                'icon' => 'success'
            ];
        }

        return response()->json($res);
    }
}
