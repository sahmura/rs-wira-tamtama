<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requests;
use App\dataObat;
use App\Distribusi;
use App\Stok;
use App\StokApotekersModel;
use App\User;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Carbon\Carbon;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $obat = dataObat::with('stok')->orderBy('nama', 'ASC')->get();
        if ($request->ajax()) {
            if ($request->jenis != '') {
                if ($request->jenis == 1) {
                    $column = "tanggal_validasi";
                } else {
                    $column = "created_at";
                }
                // $permintaan = Requests::where(DB::raw("LEFT(requests.$column, 10)"), '>=', $request->tanggal_awal)
                //     ->where(DB::raw("LEFT(requests.$column, 10)"), '<=', $request->tanggal_akhir)
                //     ->where('is_done', $request->jenis)
                //     ->where('user_id', '=', Auth()->user()->id)
                //     ->with('dataObat.stok')->with('user')
                //     ->latest()->get();
                $permintaan = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where LEFT(requests.$column,10) >= '$request->tanggal_awal' and LEFT(requests.$column,10) <= '$request->tanggal_akhir' and requests.is_done = $request->jenis and users.id = " . Auth()->user()->id . " and requests.deleted_at is null order by requests.id DESC");
            } else {
                $permintaan = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where users.id = " . Auth()->user()->id . " and requests.deleted_at is null order by requests.id DESC");
            }
            return DataTables::of($permintaan)
                ->addColumn(
                    'namaobat',
                    function ($permintaan) {
                        if ($permintaan->nama_obat <> null) {
                            return $permintaan->nama_obat;
                        } else {
                            return 'Data telah dihapus';
                        }
                    }
                )
                ->addColumn(
                    'action',
                    function ($permintaan) {
                        if ($permintaan <> null) {
                            $button = "<div class='btn-group'>";
                            $button .= '<button type="button" class="btn btn-warning btn-sm btn-edit" data-id="' . $permintaan->id . '" data-nama="' . $permintaan->id_obat . '" data-total="' . $permintaan->total . '" data-keterangan="' . $permintaan->keterangan . '" data-isdone="' . $permintaan->is_done . '" data-toggle="tooltip" data-placement="bottom" title="Sunting data"><i class="fa fa-pencil-square-o"></i></button>';
                            $button .= '<button data-token="' . csrf_token() . '" data-id="' . $permintaan->id . '"  class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus data"><i class="fa fa-trash"></i></button></div>';

                            return $button;
                        } else {
                            return "<span class='badge badge-danger'><i class='fa fa-times'></i></span>";
                        }
                    }
                )
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $tanggal_awal = Carbon::now()->firstOfMonth()->format('Y-m-d');
        $tanggal_akhir = Carbon::now()->lastOfMonth()->format('Y-m-d');
        return view('requests.permintaan_list', compact('obat', 'tanggal_awal', 'tanggal_akhir'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'total' => 'required',
                'keterangan' => 'required',
            ]
        );

        $input = Requests::create(
            [
                'user_id' => Auth::id(),
                'obat_id' => $request->nama,
                'total' => $request->total,
                'keterangan' => $request->keterangan,
                'is_done' => 0,
                'tanggal_request' => now()
            ]
        );

        if ($input) {
            $response = [
                'status' => true,
                'message' => 'Berhasil menambahkan data'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Gagal menambahkan data'
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
                'total' => 'required',
                'keterangan' => 'required',
            ]
        );

        if ($request->is_done != 0) {

            $response = [
                'status' => false,
                'message' => 'Data yang disetujui tidak dapat diubah'
            ];
            return response()->json($response);
        }

        $update = Requests::find($id)->update(
            [
                'user_id' => Auth::id(),
                'obat_id' => $request->nama,
                'total' => $request->total,
                'keterangan' => $request->keterangan,
                'is_done' => 0,
            ]
        );

        if ($update) {
            $response = [
                'status' => true,
                'message' => 'Berhasil menambahkan data'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Gagal menambahkan data'
            ];
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requests = Requests::find($id);
        $requests->delete();
        return response()->json(['success' => 'Data berhasil dihapus']);
    }

    public function antrianObat(Request $request)
    {
        // $obat = dataObat::with('stok')->get();
        if ($request->ajax()) {
            if ($request->jenis != '') {
                if ($request->jenis == 1) {
                    $column = "tanggal_validasi";
                } else {
                    $column = "created_at";
                }
                // $permintaan = Requests::where(DB::raw("LEFT(requests.$column, 10)"), '>=', $request->tanggal_awal)
                //     ->where(DB::raw("LEFT(requests.$column, 10)"), '<=', $request->tanggal_akhir)
                //     ->where('is_done', $request->jenis)
                //     ->with('dataObat.stok')
                //     ->with('user')
                //     ->latest()->get();
                $permintaan = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where LEFT(requests.$column,10) >= '$request->tanggal_awal' and LEFT(requests.$column,10) <= '$request->tanggal_akhir' and requests.is_done = $request->jenis and requests.deleted_at is null order by requests.id DESC");
            } else {
                // $permintaan = Requests::with('dataObat.stok')->with('user')->latest()->get();
                $permintaan = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where requests.deleted_at is null order by requests.id DESC");
            }
            return DataTables::of($permintaan)
                ->addColumn(
                    'namaobat',
                    function ($permintaan) {
                        if ($permintaan->nama_obat <> null) {
                            return $permintaan->nama_obat;
                        } else {
                            return 'Data telah dihapus';
                        }
                    }
                )
                ->addColumn(
                    'action',
                    function ($permintaan) {
                        if ($permintaan->is_done == 1) {
                            return "<button class='btn btn-batalkan btn-sm btn-danger' data-id='" . $permintaan->id . "'><i class='fa fa-times'></i></button>";
                        } else if ($permintaan->is_done == 0) {

                            if ($permintaan->stok_obat <> null) {
                                $button = "<div class='btn-group'>";
                                $button .= "<button class='btn btn-info btn-detail btn-sm'
                                data-nama='" . $permintaan->nama_obat . "' 
                                data-apoteker='" . $permintaan->nama_user . "' 
                                data-total='" . $permintaan->total . "'
                                data-keterangan='" . $permintaan->keterangan . "'
                                data-id='" . $permintaan->id . "'
                                data-stok='" . $permintaan->stok_obat . "'
                                data-idpemesan='" . $permintaan->id_user . "'
                                ><i class='fa fa-search'></i></button></div>";
                                return $button;
                            } else {
                                return "<span class='badge badge-danger'><i class='fa fa-times'></i></span>";
                            }
                        } else {
                            return "<span class='badge badge-danger'>Telah direspon</span>";
                        }
                    }
                )
                ->rawColumns(['action', 'namaobat'])
                ->addIndexColumn()
                ->make(true);
        }

        $tanggal_awal = Carbon::now()->firstOfMonth()->format('Y-m-d');
        $tanggal_akhir = Carbon::now()->lastOfMonth()->format('Y-m-d');
        return view('requests.permintaan_list_admin', compact('tanggal_awal', 'tanggal_akhir'));
    }

    public function updateAdmin(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = Requests::find($request->idRequest);
            $dataObat = $data->obat_id;

            $stok = dataObat::with('stok')->find($data->obat_id);
            $stokAsli = Stok::find($stok->stok->id);

            if ($request->setuju) {
                if ($stok->stok->stok >= (int) $request->totalObatRequest) {
                    $update = $data->update(
                        [
                            'feedback' => $request->feedbackRequest,
                            'is_done' => $request->isDone,
                            'tanggal_validasi' => $request->tanggal_validasi,
                            'total' => $request->totalObatRequest
                        ]
                    );
                    if ($update) {
                        $stokSebelumnya = $stok->stok->stok;
                        $kurangiStok = $stokAsli->update(
                            [
                                'stok' => $stokSebelumnya - (int) $request->totalObatRequest,
                            ]
                        );

                        if ($kurangiStok) {

                            $tambahStokApoteker = StokApotekersModel::where('user_id', $request->id_pemesan)->where('obat_id', $data->obat_id);
                            if ($tambahStokApoteker->count() == 0) {
                                StokApotekersModel::create(
                                    [
                                        'user_id' => $request->id_pemesan,
                                        'obat_id' => $data->obat_id,
                                        'stok' => $request->totalObatRequest
                                    ]
                                );
                                $response = [
                                    'status' => true,
                                    'message' => 'Berhasil merespon data'
                                ];
                            } else {
                                StokApotekersModel::where('user_id', '=', $request->id_pemesan)->where('obat_id', $data->obat_id)->update(
                                    [
                                        'stok' => (int) $tambahStokApoteker->first()->stok + (int) $request->totalObatRequest
                                    ]
                                );
                                $response = [
                                    'status' => true,
                                    'message' => 'Berhasil merespon data'
                                ];
                            }
                        } else {
                            $response = [
                                'status' => false,
                                'message' => 'Gagal merespon data'
                            ];
                        }

                        Distribusi::create(
                            [
                                'user_id' => Auth()->user()->id,
                                'obat_id' => $data->obat_id,
                                'distribusi' => User::where('id', $request->id_pemesan)->first()->role,
                                'total' => $request->totalObatRequest,
                                'keterangan' => $request->feedbackRequest,
                                'tanggal_validasi' => $request->tanggal_validasi,
                                'request_id' => $data->id
                            ]
                        );
                    }
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Obat tidak mencukupi'
                    ];
                }
            } else {
                $update = $data->update(
                    [
                        'feedback' => $request->feedbackRequest,
                        'is_done' => $request->isDone,
                    ]
                );

                if ($update) {
                    $response = [
                        'status' => true,
                        'message' => 'Berhasil merespon data'
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Gagal merespon data'
                    ];
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($response);
    }

    public function getStokObat(Request $request)
    {
        try {
            $stok = Stok::where('obat_id', $request->obat_id)->first();
            $response = [
                'status' => true,
                'stok' => number_format($stok->stok)
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'stok' => 0
            ];
        }

        return response()->json($response);
    }

    public function cetakDistribusiIndex(Request $request)
    {
        // $obat = dataObat::with('stok')->get();
        if ($request->ajax()) {
            if ($request->jenis != '') {
                if ($request->jenis == 1) {
                    $column = "tanggal_validasi";
                } else {
                    $column = "created_at";
                }
                // $permintaan = Requests::where(DB::raw("LEFT(requests.$column, 10)"), '>=', $request->tanggal_awal)
                //     ->where(DB::raw("LEFT(requests.$column, 10)"), '<=', $request->tanggal_akhir)
                //     ->where('is_done', $request->jenis)
                //     ->with('dataObat.stok')
                //     ->with('user')
                //     ->latest()->get();
                $permintaan = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where LEFT(requests.$column,10) >= '$request->tanggal_awal' and LEFT(requests.$column,10) <= '$request->tanggal_akhir' and requests.is_done = $request->jenis and requests.deleted_at is null order by requests.id DESC");
            } else {
                // $permintaan = Requests::with('dataObat.stok')->with('user')->latest()->get();
                $permintaan = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where requests.deleted_at is null order by requests.id DESC");
            }
            return DataTables::of($permintaan)
                ->addColumn(
                    'action',
                    function ($permintaan) {
                        if (isset($permintaan->stok_obat)) {
                            $tersedia = $permintaan->stok_obat;
                        } else {
                            $tersedia = 0;
                        }
                        $button = "<div class='btn-group'>";
                        $button .= "<button class='btn btn-success btn-include btn-sm'
                        data-id='" . $permintaan->id . "'
                        ><i class='fa fa-check'></i></button></div>";
                        return $button;
                    }
                )
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $tanggal_awal = Carbon::now()->firstOfMonth()->format('Y-m-d');
        $tanggal_akhir = Carbon::now()->lastOfMonth()->format('Y-m-d');
        return view('requests.cetak_permintaan_list', compact('tanggal_awal', 'tanggal_akhir'));
    }

    public function batalkanProses(Request $request)
    {
        $data = Requests::where('id', $request->id)->first();
        try {
            DB::beginTransaction();
            $checkStok = StokApotekersModel::where('obat_id', $data->obat_id)->where('user_id', $data->user_id)->first();
            if ($checkStok->stok < $data->total) {
                $response = [
                    'status' => false,
                    'message' => 'Stok di tidak  tersedia',
                    'notes' => 'Pembatalan mengakibatkan pengkosongan data stok di instalasi terkait',
                    'type' => 'error'
                ];
                return response()->json($response);
            }
            StokApotekersModel::where('obat_id', $data->obat_id)
                ->where('user_id', $data->user_id)
                ->decrement('stok', $data->total);
            Stok::where('obat_id', $data->obat_id)
                ->increment('stok', $data->total);
            $deleteDistribusi = Distribusi::where('request_id', $data->id)->delete();
            if ($deleteDistribusi) {
                $nb = 'serta distribusi dibatalkan';
            } else {
                $nb = 'Namun distribusi harus dihapus manual';
            }
            Requests::where('id', $request->id)->update(['is_done' => 3]);
            DB::commit();
            $response = [
                'status' => true,
                'message' => 'Berhasil Dibatalkan',
                'notes' => 'Stok instalasi berkurang, dan stok gudang bertambah ' . $nb,
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
                'notes' => '',
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }
}
