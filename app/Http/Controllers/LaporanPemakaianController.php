<?php

namespace App\Http\Controllers;

use App\DataDokter;
use App\dataObat;
use App\Laporan;
use Illuminate\Http\Request;
use App\LaporanOperasi;
use App\LaporanOperasiObat;
use Yajra\DataTables\Facades\DataTables;
use App\Group;
use Carbon\Carbon;
use DB;

class LaporanPemakaianController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = LaporanOperasi::where('deleted_at', null);

            if ($request->tanggal_awal) {
                $data->where('tanggal_operasi', '>=', $request->tanggal_awal);
            }
            if ($request->tanggal_akhir) {
                $data->where('tanggal_operasi', '<=', $request->tanggal_akhir);
            }

            $data->orderBy('id', 'DESC')->get();
            return DataTables::of($data)
                ->addColumn(
                    'action',
                    function ($data) {
                        return "<div class='btn-group'>
                            <button class='btn btn-xs btn-success btn-detail' data-id='$data->id'><i class='fa fa-search'></i></button>
                            <button class='btn btn-xs btn-warning btn-edit' data-id='$data->id'><i class='fa fa-pencil-square-o'></i></button>
                            <button class='btn btn-xs btn-danger btn-delete' data-id='$data->id'><i class='fa fa-trash-o'></i></button>
                        </div>";
                    }
                )
                ->addColumn(
                    'tgl',
                    function ($data) {
                        return Carbon::parse($data->tanggal_operasi)->locale('id')->isoFormat('Do MMMM YYYY');
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }

        return view('laporanpemakaian.index');
    }

    public function edit($id)
    {
        $data = LaporanOperasi::where('id', $id)->where('deleted_at', null)->with('dataObats:laporan_id,obat_id,pemakaian')->first();
        $groups = Group::where('deleted_at', null)->get();
        $obats = dataObat::where('deleted_at', null)->get();
        $dokter = DataDokter::where('deleted_at', null)->get();
        return view('laporanpemakaian.edit', compact('data', 'groups', 'obats', 'dokter'));
    }

    public function add()
    {
        $groups = Group::where('deleted_at', null)->get();
        $obats = dataObat::where('deleted_at', null)->get();
        $dokter = DataDokter::where('deleted_at', null)->orderBy('nama', 'asc')->get();
        return view('laporanpemakaian.add', compact('groups', 'obats', 'dokter'));
    }

    public function addData(Request $request)
    {
        try {
            DB::beginTransaction();
            $laporan = LaporanOperasi::create(
                [
                    'nama' => $request->nama,
                    'tipe_pasien' => $request->tipe_pasien,
                    'status' => $request->status,
                    'pangkat_pekerjaan' => $request->pangkat_pekerjaan,
                    'alamat' => $request->alamat,
                    'macam_operasi' => $request->macam_operasi,
                    'golongan_operasi' => $request->golongan_operasi,
                    'tanggal_operasi' => $request->tanggal_operasi,
                    'dokter_id' => $request->dokter_id,
                    'dokter_id_bedah' => $request->dokter_id_bedah,
                    'dokter_id_anestesi' => $request->dokter_id_anestesi,
                    'tanggal_lahir' => $request->tanggal_lahir,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'perawat_bedah' => $request->perawat_bedah,
                    'perawat_anestesi' => $request->perawat_anestesi,
                    'sewa_alat' => $request->sewa_alat,
                    'sewa_ok' => $request->sewa_ok,
                    'biaya_lain' => $request->biaya_lain,
                    'jasa_dokter_bedah' => $request->jasa_dokter_bedah,
                    'jasa_dokter_anestesi' => $request->jasa_dokter_anestesi,
                    'jasa_dokter_spesialis' => $request->jasa_dokter_spesialis,
                    'jasa_perawat_anestesi' => $request->jasa_perawat_anestesi,
                    'jasa_perawat_bedah' => $request->jasa_perawat_bedah,
                ]
            );
            for ($index = 0; $index < count($request->obat); $index++) {
                LaporanOperasiObat::create(
                    [
                        'laporan_id' => $laporan->id,
                        'obat_id' => $request->obat[$index],
                        'pemakaian' => $request->pemakaian[$index]
                    ]
                );
            }
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil menambahkan data',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal menambahkan data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function editData(Request $request)
    {
        try {
            DB::beginTransaction();
            LaporanOperasi::where('id', $request->id)->update(
                [
                    'nama' => $request->nama,
                    'tipe_pasien' => $request->tipe_pasien,
                    'status' => $request->status,
                    'pangkat_pekerjaan' => $request->pangkat_pekerjaan,
                    'alamat' => $request->alamat,
                    'macam_operasi' => $request->macam_operasi,
                    'golongan_operasi' => $request->golongan_operasi,
                    'tanggal_operasi' => $request->tanggal_operasi,
                    'dokter_id' => $request->dokter_id,
                    'dokter_id_bedah' => $request->dokter_id_bedah,
                    'dokter_id_anestesi' => $request->dokter_id_anestesi,
                    'tanggal_lahir' => $request->tanggal_lahir,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'perawat_bedah' => $request->perawat_bedah,
                    'perawat_anestesi' => $request->perawat_anestesi,
                    'sewa_alat' => $request->sewa_alat,
                    'sewa_ok' => $request->sewa_ok,
                    'biaya_lain' => $request->biaya_lain,
                    'jasa_dokter_bedah' => $request->jasa_dokter_bedah,
                    'jasa_dokter_anestesi' => $request->jasa_dokter_anestesi,
                    'jasa_dokter_spesialis' => $request->jasa_dokter_spesialis,
                    'jasa_perawat_anestesi' => $request->jasa_perawat_anestesi,
                    'jasa_perawat_bedah' => $request->jasa_perawat_bedah,
                ]
            );
            LaporanOperasiObat::where('laporan_id', $request->id)->delete();
            for ($index = 0; $index < count($request->obat); $index++) {
                LaporanOperasiObat::create(
                    [
                        'laporan_id' => $request->id,
                        'obat_id' => $request->obat[$index],
                        'pemakaian' => $request->pemakaian[$index]
                    ]
                );
            }
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil mengedit data',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal mengedit data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function deleteData(Request $request)
    {
        try {
            DB::beginTransaction();
            LaporanOperasi::where('id', $request->id)->delete();
            LaporanOperasiObat::where('laporan_id', $request->id)->delete();
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil menghapus data',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal menghapus data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function getObat(Request $request)
    {
        $data = Group::where('deleted_at', null)->where('id', $request->id)->with('dataObat')->first();
        $listObat = [];
        foreach ($data->dataObat as $list) {
            $listObat[] = [
                'id' => $list->id,
                'nama' => $list->nama,
            ];
        }
        return response()->json($listObat);
    }

    public function detail($id)
    {
        $data = LaporanOperasi::where('id', $id)->with('dataObats.obatOne')->with(['dokter', 'dokter_bedah', 'dokter_anestesi'])->first();
        $pemakaian = LaporanOperasi::where('id', $id)->with('dataObats:laporan_id,obat_id,pemakaian')->first();
        $content = [];
        $i = 0;

        foreach ($data->dataObats as $obat) {
            $content[$i] = ['id' => $obat->id, 'nama' => $obat->nama, 'harga_satuan_terakhir' => $obat->harga_satuan_terakhir, 'pemakaian' => null];
            foreach ($pemakaian->dataObats as $pakai) {
                if ($obat->id == $pakai->obat_id) {
                    $content[$i]['pemakaian'] = $pakai->pemakaian;
                }
            }
            $i++;
        }

        return view('laporanpemakaian.detail', compact('data', 'content'));
    }

    public function validasiData(Request $request)
    {
        LaporanOperasi::where('id', $request->id)->update(
            [
                'is_validate' => 1
            ]
        );

        $response = [
            'status' => true,
            'title' => 'Berhasil memvalidasi data',
            'text' => '',
            'icon' => 'success'
        ];

        return response()->json($response);
    }

    public function batalkanValidasi(Request $request)
    {
        LaporanOperasi::where('id', $request->id)->update(
            [
                'is_validate' => 0
            ]
        );

        $response = [
            'status' => true,
            'title' => 'Berhasil membatalkan validasi data',
            'text' => '',
            'icon' => 'success'
        ];

        return response()->json($response);
    }
}
