<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Obat;
use App\Pbf;
use App\Stok;
use App\dataObat;
use App\DataPemasukan;
use App\SatuanJenis;
use App\Kategori;
use App\Distribusi;
use DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ObatImport;

class ObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // $obat = DataPemasukan::with('obat')->with('pbf')->orderBy('id', 'DESC')->get();
            $obat = DB::select('select data_pemasukan.id, data_pemasukan.obat_id, pbf.nama as nama_pbf, data_pemasukan.tanggal_pemasukan, data_pemasukan.nomor_faktur, data_obat.nama as nama_obat, data_pemasukan.tanggal_kadaluwarsa, ((((data_pemasukan.netto) + (data_pemasukan.netto*(data_pemasukan.ppn_netto/100)))-(((data_pemasukan.netto) + (data_pemasukan.netto*(data_pemasukan.ppn_netto/100)))*(data_pemasukan.diskon/100)))/(data_pemasukan.jumlah_satuan*data_pemasukan.jumlah_kemasan)) as harga_akhir_satuan, (data_pemasukan.jumlah_kemasan * data_pemasukan.jumlah_satuan) as jumlah_obat from data_pemasukan join data_obat on data_obat.id = data_pemasukan.obat_id join pbf on pbf.id = data_pemasukan.pbf_id order by data_pemasukan.id desc');
            return DataTables::of($obat)
                ->addColumn('action', function ($obat) {
                    if ($obat->nama_obat <> null) {
                        $button = "<div class='btn-group'>";
                        $button .= '<a href="' . route('dataobat.show', $obat->obat_id) . '" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-placement="bottom" title="Detail data"><i class="fa fa-search"></i></a>';
                        $button .= '<a href="' . route('dataobat.edit', $obat->id) . '" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="bottom" title="Sunting data"><i class="fa fa-pencil-square-o"></i></i></a>';
                        $button .= '<button data-token="' . csrf_token() . '" data-id="' . $obat->id . '"  class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus data"><i class="fa fa-trash-o"></i></button></div>';

                        return $button;
                    } else {
                        return "<span class='badge badge-danger'><i class='fa fa-times'></i></span>";
                    }
                })
                ->addColumn('pbf', function ($obat) {
                    return $obat->nama_pbf;
                })
                ->addIndexColumn()
                ->rawColumns(['action', 'pbf'])
                ->make(true);
        }
        return view('obat.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pbf = Pbf::orderBy('nama', 'ASC')->get();
        $satuan = SatuanJenis::get();
        $namaObat = dataObat::where('deleted_at', null)->get();
        $kategori = Kategori::get();
        $listNomorFaktur = DataPemasukan::distinct()->get('nomor_faktur');
        return view('obat.create', compact('pbf', 'satuan', 'namaObat', 'kategori', 'listNomorFaktur'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'obat_id' => 'required',
                'nomor_faktur' => 'required',
                'tanggal_pemasukan' => 'required',
                'pbf_id' => 'required',
                'tanggal_kadaluwarsa' => 'required',
                'jumlah_kemasan' => 'required',
                'jumlah_satuan' => 'required',
                'netto' => 'required',
                'diskon' => 'required',
                'harga_akhir_manual' => 'required',
                'ppn_netto' => 'required',
            ]
        );

        try {
            DB::beginTransaction();
            DataPemasukan::create(
                [
                    'obat_id' => $request->obat_id,
                    'nomor_faktur' => $request->nomor_faktur,
                    'tanggal_pemasukan' => $request->tanggal_pemasukan,
                    'pbf_id' => $request->pbf_id,
                    'satuan_id' => $request->satuanjenis,
                    'tanggal_kadaluwarsa' => $request->tanggal_kadaluwarsa,
                    'jumlah_kemasan' => $request->jumlah_kemasan,
                    'jumlah_satuan' => $request->jumlah_satuan,
                    'netto' => $request->netto,
                    'diskon' => $request->diskon,
                    'harga_akhir_manual' => $request->harga_akhir_manual,
                    'ppn_netto' => $request->ppn_netto,
                ]
            );
            $stok = Stok::where('obat_id', '=', $request->obat_id)->first();
            if ($stok) {
                $save = Stok::where('obat_id', '=', $request->obat_id)->update(
                    [
                        'stok' => $stok->stok + ($request->jumlah_kemasan * $request->jumlah_satuan)
                    ]
                );
            } else {
                $save = Stok::create(
                    [
                        'obat_id' => $request->obat_id,
                        'stok' => $request->jumlah_kemasan * $request->jumlah_satuan
                    ]
                );
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $save = false;
        }

        return $save ? redirect()->route('dataobat.index')->with('success', 'Berhasil menambah data obat') : redirect()->route('dataobat.create')->with('failed', 'Gagal menambah data obat, cek apakah nomor faktur sudah ada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obat = dataObat::with('obat')->with('stok')->where('id', $id)->first();
        return view('obat.showDataObat', compact('obat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obat = DataPemasukan::find($id);
        $dataobat = dataObat::find($obat->obat_id);
        $listdataobat = dataObat::get();
        $satuan = SatuanJenis::get();
        $pbf = Pbf::orderBy('nama', 'ASC')->get();
        $kategori = Kategori::get();
        $stok = Stok::where('obat_id', $obat->obat_id)->first();
        return view('obat.edit', compact('obat', 'pbf', 'dataobat', 'kategori', 'listdataobat', 'satuan', 'stok'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if (DataPemasukan::where('nomor_faktur', '=', $request->nomor_faktur)->whereNotIn('id', [$id])->count() != 0) {
        //     return redirect()->route('dataobat.index')->with('failed', 'Nomor faktur sudah tersedia');
        // }
        $idObatLama = DataPemasukan::where('id', '=', $id)->first();
        $stokObatLama = Stok::where('obat_id', '=', $idObatLama->obat_id)->first();
        $totalObatLama = $idObatLama->jumlah_kemasan * $idObatLama->jumlah_satuan;
        $totalObat = $request->jumlah_kemasan * $request->jumlah_satuan;

        try {
            DB::beginTransaction();
            DataPemasukan::where('id', '=', $id)->update(
                [
                    'obat_id' => $request->obat_id,
                    'nomor_faktur' => $request->nomor_faktur,
                    'tanggal_pemasukan' => $request->tanggal_pemasukan,
                    'pbf_id' => $request->pbf_id,
                    'satuan_id' => $request->satuanjenis,
                    'tanggal_kadaluwarsa' => $request->tanggal_kadaluwarsa,
                    'jumlah_kemasan' => $request->jumlah_kemasan,
                    'jumlah_satuan' => $request->jumlah_satuan,
                    'netto' => $request->netto,
                    'diskon' => $request->diskon,
                    'harga_akhir_manual' => $request->harga_akhir_manual,
                    'ppn_netto' => $request->ppn_netto,
                ]
            );
            Stok::where('obat_id', '=', $idObatLama->obat_id)->update(
                [
                    'stok' => $stokObatLama->stok - $totalObatLama,
                ]
            );
            $stokAwal = $stokObatLama->stok - $totalObatLama;
            $save = Stok::where('obat_id', '=', $request->obat_id)->update(
                [
                    'stok' => $stokAwal + $totalObat,
                ]
            );
            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
            $save = false;
        }

        return $save ? redirect()->route('dataobat.index')->with('success', 'Berhasil menyunting data obat') : redirect()->route('dataobat.index')->with('failed', 'Gagal menyunting data obat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obat = DataPemasukan::where('id', '=', $id)->first();
        $dataObat = dataObat::where('id', $obat->obat_id)->with('stok')->first();

        $updateData = Stok::find($dataObat->stok->id)->update(
            [
                'stok' => $dataObat->stok->stok - $obat->jumlah_obat
            ]
        );

        if ($updateData) {
            $obat->delete();
            return response()->json(['message' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['message' => 'Data gagal dihapus']);
        }
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $dataObat = dataObat::where('nama', '=', $request->nama)->firstOrFail();
            $response = [
                'status' => true,
                'data' => json_encode($dataObat)
            ];
            return response()->json($response);
        }
    }

    public function importDataObat(Request $request)
    {

        $this->validate(
            $request,
            [
                'file' => 'required|mimes:csv,xls,xlsx'
            ]
        );

        // import data
        Excel::import(new ObatImport, $request->file('file'));

        // alihkan halaman kembali
        return redirect()->route('dataobat.index')->with('success', 'Berhasil menambahkan dan import data obat');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pemasukan(Request $request)
    {
        // $obat = DataPemasukan::with('obat')->with('pbf')->where('obat_id', $request->id)->where('deleted_at', null)->get();
        $obat = DB::select("select data_pemasukan.tanggal_pemasukan, data_pemasukan.nomor_faktur, pbf.nama as nama_pbf, (data_pemasukan.jumlah_kemasan * data_pemasukan.jumlah_satuan) as jumlah_obat, ((((data_pemasukan.netto) + (data_pemasukan.netto*(data_pemasukan.ppn_netto/100)))-(((data_pemasukan.netto) + (data_pemasukan.netto*(data_pemasukan.ppn_netto/100)))*(data_pemasukan.diskon/100)))/(data_pemasukan.jumlah_satuan*data_pemasukan.jumlah_kemasan)) as harga_akhir_satuan from data_pemasukan join data_obat on data_pemasukan.obat_id = data_obat.id join pbf on pbf.id = data_pemasukan.pbf_id where data_pemasukan.obat_id = $request->id and data_pemasukan.deleted_at is null");
        if ($request->ajax()) {
            return DataTables::of($obat)
                ->addIndexColumn()
                ->make(true);
        }
        return view('obat.show', $request->id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pengeluaran(Request $request)
    {
        // $distribusi = Distribusi::with('dataObat.stok')->where('obat_id', $request->id)->where('deleted_at', null)->get();
        $distribusi = DB::select("select distribusi.id, distribusi.tanggal_validasi, distribusi.distribusi, distribusi.total, distribusi.keterangan  from distribusi join data_obat on data_obat.id = distribusi.obat_id join stok on stok.obat_id = data_obat.id where distribusi.obat_id = $request->id and distribusi.deleted_at is null");
        if ($request->ajax()) {
            return DataTables::of($distribusi)
                ->addColumn(
                    'action',
                    function ($distribusi) {
                        return '<button class="btn btn-danger btn-sm btn-delete-dis" data-id="' . $distribusi->id . '"><i class="fa fa-trash-o"></i></button>';
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
        return view('obat.show', $request->id);
    }

    public function fixdata(Request $request)
    {
        try {
            DB::beginTransaction();
            $getPemasukan = DB::select("SELECT obat_id, SUM((jumlah_kemasan * jumlah_satuan)) as pemasukan FROM data_pemasukan WHERE obat_id = $request->id AND deleted_at IS NULL GROUP BY obat_id");
            $getPengeluaran = DB::select("SELECT obat_id, SUM(total) as pengeluaran FROM distribusi WHERE obat_id = $request->id AND deleted_at IS NULL GROUP BY obat_id");
            if ($getPemasukan == null) {
                $pemasukan = 0;
            } else {
                $pemasukan = $getPemasukan[0]->pemasukan;
            }

            if ($getPengeluaran == null) {
                $pengeluaran = 0;
            } else {
                $pengeluaran = $getPengeluaran[0]->pengeluaran;
            }

            Stok::updateOrCreate(
                ['obat_id' => $request->id],
                ['stok' => ($pemasukan - $pengeluaran)]
            );
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Data telah diperbaharui',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Data ggal diperbaharui',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }
}
