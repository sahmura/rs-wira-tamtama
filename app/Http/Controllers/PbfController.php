<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pbf;
use Yajra\DataTables\Facades\DataTables;

class PbfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($pbf);
        if ($request->ajax()) {
            $pbf = Pbf::get();
            return DataTables::of($pbf)
                ->addColumn('action', function ($pbf) {
                    $button = "<div class='btn-group'>";
                    $button .= '<button type="button" class="btn btn-warning btn-sm btn-edit" id="' . $pbf->id . '" data-nama="' . $pbf->nama . '" data-toggle="tooltip" data-placement="bottom" title="Sunting data"><i class="fa fa-pencil-square-o"></i></button>';
                    $button .= '<button data-token="' . csrf_token() . '" data-id="' . $pbf->id . '"  class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></button></div>';

                    return $button;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('pbf.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = Pbf::where('nama', '=', $request->nama)->count();
        if ($check == 0) {
            $pbf = new Pbf();
            $pbf->nama = $request->nama;
            $pbf->save();
            $response = [
                'status' => true,
                'message' => 'Berhasil menambahkan data'
            ];
        } elseif ($check == 1) {
            $response = [
                'status' => false,
                'message' => 'Data sudah tersedia'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Gagal menambahkan data'
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Pbf::where('nama', '=', $request->nama)->count();
        if ($check == 0) {
            $pbf = Pbf::find($id);

            if ($pbf->nama == $request->nama) {
                $response = [
                    'status' => false,
                    'message' => 'Data sudah tersedia'
                ];
            } elseif ($pbf->nama != $request->nama) {
                $pbf->nama = $request->nama;
                $pbf->save();
                $response = [
                    'status' => true,
                    'message' => 'Data berhasil diubah'
                ];
            } else {
                $response = [
                    'status' => false,
                    'message' => 'Data gagal diubah'
                ];
            }
        } else {
            $response = [
                'status' => false,
                'message' => 'Data sudah tersedia'
            ];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pbf = Pbf::find($id);
        $pbf->delete();
        return response()->json(['success' => 'Data berhasil dihapus']);
    }
}
