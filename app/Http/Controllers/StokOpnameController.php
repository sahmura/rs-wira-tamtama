<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requests;
use App\StokApotekersModel;
use App\StokOpname;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use DB;

class StokOpnameController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $date = explode('-', $request->bulan);
            $year = $date[0];
            $month = $date[1];
            // $stokApotekers = StokOpname::where('user_id', Auth()->user()->id)
            //     ->whereMonth('periode', $month)->whereYear('periode', $year)
            //     ->with('obat')->with('stokObat')
            //     ->groupBy(['obat_id', 'user_id', 'stok_awal', 'stok_opname'])->get(['user_id', 'obat_id', 'stok_awal', 'stok_opname']);
            $stokApotekers = DB::select("select dob.nama as nama_obat, dp.satuan_id as satuan_id, so.stok_awal as stok_awal, sa.stok as stok_obat, so.stok_opname as stok_opname, ((((dp.netto) + (dp.netto*(dp.ppn_netto/100)))-(((dp.netto) + (dp.netto*(dp.ppn_netto/100)))*(dp.diskon/100)))/(dp.jumlah_satuan*dp.jumlah_kemasan)) as harga_akhir, so.periode as periode, so.harga_obat as harga_obat,
            CASE
                when dp.satuan_id <> 0 then (SELECT satuan_jenis.nama from satuan_jenis WHERE satuan_jenis.id = dp.satuan_id)
                else 'Satuan belum disetting'
            end as nama_satuan_terakhir
            from stok_opnames as so join users u on u.id = so.user_id join data_obat dob on dob.id = so.obat_id join data_pemasukan dp on dp.id = so.data_pemasukan_id join stok_apotekers sa on sa.obat_id = so.obat_id where MONTH(so.periode) = $month and YEAR(so.periode) = $year and so.user_id =" . auth()->user()->id);
            return DataTables::of($stokApotekers)
                ->addColumn(
                    'namamatkes',
                    function ($stokApotekers) {
                        return $stokApotekers->nama_obat;
                    }
                )
                ->addColumn(
                    'bentuk',
                    function ($stokApotekers) {
                        return $stokApotekers->nama_satuan_terakhir;
                    }
                )
                ->addColumn(
                    'harga',
                    function ($stokApotekers) {
                        return ($stokApotekers->harga_obat != null) ? number_format($stokApotekers->harga_obat, 2, ',', '.') : number_format($stokApotekers->harga_akhir, 2, ',', '.');
                        // return 'tes';
                    }
                )
                ->addColumn(
                    'stok_awal',
                    function ($stokApotekers) {
                        return number_format($stokApotekers->stok_awal);
                    }
                )
                ->addColumn(
                    'penerimaan', // stok obat
                    function ($stokApotekers) {
                        return number_format($stokApotekers->stok_obat);
                    }
                )
                ->addColumn(
                    'persediaan_akhir',
                    function ($stokApotekers) { //stok opname
                        return number_format($stokApotekers->stok_opname);
                    }
                )
                ->addColumn(
                    'pemakaian',
                    function ($stokApotekers) {
                        return number_format(($stokApotekers->stok_awal + $stokApotekers->stok_obat) - $stokApotekers->stok_opname); //penerimaan - persediaan_akhir
                    }
                )
                ->addColumn(
                    'nominal_akhir',
                    function ($stokApotekers) {
                        return ($stokApotekers->harga_obat != null) ? number_format($stokApotekers->stok_opname * $stokApotekers->harga_obat, 2, ',', '.') : number_format($stokApotekers->stok_opname * (int) $stokApotekers->harga_akhir, 2, ',', '.');
                        // persediaan akhir x harga obat
                    }
                )
                ->addColumn(
                    'nominal_pemakaian',
                    function ($stokApotekers) {
                        return ($stokApotekers->harga_obat != null) ? number_format((($stokApotekers->stok_awal + $stokApotekers->stok_obat) - $stokApotekers->stok_opname) * $stokApotekers->harga_obat, 2, ',', '.') : number_format((($stokApotekers->stok_awal + $stokApotekers->stok_obat) - $stokApotekers->stok_opname) * $stokApotekers->harga_akhir, 2, ',', '.');
                        // pemakaian x harga obat
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }

        $dataObat = Requests::where('user_id', Auth()->user()->id)
            ->where('is_done', 1)
            ->with('dataObat')->groupBy('user_id', 'obat_id')->get(['user_id', 'obat_id']);
        return view('stokopname.index', compact('dataObat'));
    }

    public function exportToExcel(Request $request)
    {
        $date   = explode('-', $request->bulan);
        $year   = $date[0];
        $month  = $date[1];
        $data   = DB::select("select dob.nama as nama_obat, dp.satuan_id as satuan_id, so.stok_awal as stok_awal, sa.stok as stok_obat, so.stok_opname as stok_opname, ((((dp.netto) + (dp.netto*(dp.ppn_netto/100)))-(((dp.netto) + (dp.netto*(dp.ppn_netto/100)))*(dp.diskon/100)))/(dp.jumlah_satuan*dp.jumlah_kemasan)) as harga_akhir, so.periode as periode, so.harga_obat as harga_obat,
        CASE
            when dp.satuan_id <> 0 then (SELECT satuan_jenis.nama from satuan_jenis WHERE satuan_jenis.id = dp.satuan_id)
            else 'Satuan belum disetting'
        end as nama_satuan_terakhir
        from stok_opnames as so join users u on u.id = so.user_id join data_obat dob on dob.id = so.obat_id join data_pemasukan dp on dp.id = so.data_pemasukan_id join stok_apotekers sa on sa.obat_id = so.obat_id where MONTH(so.periode) = $month and YEAR(so.periode) = $year and so.user_id =" . auth()->user()->id);
        $filename = 'Stok Opname Bulan ' . Carbon::parse($request->bulan)->locale('id')->isoFormat('MMMM YYYY') . '.xlsx';
        $template = 'stok_opname_template.xlsx';

        $this->_exportExcel($data, $template, $filename, $request->bulan);
    }

    private function _exportExcel($data, $template, $filename, $date)
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(public_path('template/' . $template));
        $sheet = $spreadsheet->setActiveSheetIndex(0);

        if (Auth()->user()->role == 'ruangoperasi') {
            $instalasi = 'Ruang Operasi';
        } else {
            $instalasi = Auth()->user()->role;
        }

        $fulldate = Carbon::parse($date)->locale('id')->isoFormat('MMMM YYYY');


        $sheet->setCellValue('A2', 'INSTALASI ' . strtoupper($instalasi));
        $sheet->setCellValue('A5', 'BULAN ' . strtoupper($fulldate));

        $setStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN
                ],

            ],
            'font' => [
                'size' => '11px'
            ]
        ];


        $row = 8;
        $number = 1;
        foreach ($data as $list) {
            $sheet->setCellValue('A' . $row, $number++);
            $sheet->setCellValue('B' . $row, $list->nama_obat);
            $sheet->setCellValue('C' . $row, $list->nama_satuan_terakhir);
            $sheet->setCellValue('D' . $row, $list->harga_akhir);
            $sheet->setCellValue('E' . $row, number_format($list->stok_awal));
            $sheet->setCellValue('F' . $row, number_format($list->stok_obat));
            $sheet->setCellValue('G' . $row, number_format($list->stok_opname));
            $sheet->setCellValue('H' . $row, number_format($list->stok_obat - $list->stok_opname));
            $sheet->setCellValue('I' . $row, number_format($list->stok_opname * (int) $list->harga_akhir));
            $sheet->setCellValue('J' . $row, number_format(($list->stok_obat - $list->stok_opname) * (int) $list->harga_akhir));

            $sheet->getStyle('A' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('B' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('C' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('D' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('E' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('F' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('G' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('H' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('I' . $row)->applyFromArray($setStyle);
            $sheet->getStyle('J' . $row)->applyFromArray($setStyle);

            $row++;
        }

        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        exit;
        die;
    }

    public function insertStokOpname(Request $request)
    {
        $split = explode('-', $request->harga_obat);
        $insert = StokOpname::create(
            [
                'user_id' => Auth()->user()->id,
                'obat_id' => $request->obat_id,
                'stok_opname' => $request->stokopname,
                'periode' => Carbon::parse($request->periodeopname)->format('Y-m-d'),
                'stok_awal' => $request->stok_awal,
                'harga_obat' => $split[1],
                'data_pemasukan_id' => $split[0]
            ]
        );

        $response = ($insert) ? ['status' => true, 'message' => 'Data Berhasil Disimpan', 'type' => 'success'] : ['status' => false, 'message' => 'Data Gagal Disimpan', 'type' => 'error'];

        return response()->json($response);
    }

    public function getListHarga(Request $request)
    {
        $data = DB::select("select round(((((netto) + (netto*(ppn_netto/100)))-(((netto) + (netto*(ppn_netto/100)))*(diskon/100)))/(jumlah_satuan*jumlah_kemasan)), 2) as harga_akhir, id from data_pemasukan where obat_id = $request->obat_id and deleted_at is null");
        $response = [
            'status' => true,
            'data' => $data
        ];
        return response()->json($response);
    }
}
