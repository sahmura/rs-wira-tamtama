<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StokApotekersModel;
use App\DistribusiApotekersModel;
use App\Requests;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use DB;

class ApotekersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obat = StokApotekersModel::with('dataObat', 'dataPemasukan')->get();
        return view('apotekers.data_obat', compact('obat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stok = StokApotekersModel::where('obat_id', $request->obat_id);
        $stokSebelumnya = $stok->first();

        if ((int) $stokSebelumnya->stok < $request->total) {
            $response = [
                'status' => false,
                'message' => 'Stok di apotek tidak cukup'
            ];

            return response()->json($response);
        } else {

            $distribusi = DistribusiApotekersModel::create(
                [
                    'user_id' => Auth::id(),
                    'obat_id' => $request->obat_id,
                    'total' => $request->total,
                    'distribusi' => $request->distribusi,
                    'keterangan' => $request->keterangan,
                ]
            );

            if ($distribusi) {
                $updateStok = StokApotekersModel::where('obat_id', $request->obat_id)->update(
                    [
                        'stok' => (int) $stokSebelumnya->stok - (int) $request->total
                    ]
                );

                if ($updateStok) {
                    $response = [
                        'status' => true,
                        'message' => 'Data berhasil ditambahkan'
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Data gagal ditambahkan'
                    ];
                }
            } else {
                $response = [
                    'status' => false,
                    'message' => 'Data gagal ditambahkan ke distribusi'
                ];
            }
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataDistribusiSebelumnya = DistribusiApotekersModel::find($id);
        $stok = StokApotekersModel::where('obat_id', $dataDistribusiSebelumnya->obat_id);
        $stokSemula = (int) $stok->first()->stok + (int) $dataDistribusiSebelumnya->total;

        $reset = $stok->update(
            [
                'stok' => $stokSemula,
            ]
        );

        if ($reset) {
            if ($stokSemula >= $request->total) {
                $dataDistribusiSebelumnya->update(
                    [
                        'user_id' => Auth::id(),
                        'obat_id' => $request->obat_id,
                        'total' => $request->total,
                        'distribusi' => $request->distribusi,
                        'keterangan' => $request->keterangan,
                    ]
                );

                $stokSebelumnyaBaru = StokApotekersModel::where('obat_id', $request->obat_id);
                $updateStok = $stokSebelumnyaBaru->update(
                    [
                        'stok' => $stokSebelumnyaBaru->first()->stok - $request->total
                    ]
                );

                if ($updateStok) {
                    $response = [
                        'status' => true,
                        'message' => 'Data berhasil diedit'
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Data gagal diedit'
                    ];
                }
            } else {
                $response = [
                    'status' => false,
                    'message' => 'Maaf stok tidak cukup'
                ];
            }
        } else {
            $response = [
                'status' => false,
                'message' => 'Gagal mengedit data'
            ];
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listData(Request $request)
    {
        if ($request->ajax()) {
            if (isset($request->tanggal_awal)) {
                // $obat = StokApotekersModel::where(DB::raw('LEFT(updated_at, 10)'), '>=', $request->tanggal_awal)->where(DB::raw('LEFT(updated_at, 10)'), '<=', $request->tanggal_akhir)->where('user_id', Auth()->user()->id)->with('dataObat')->get();
                $obat = DB::select("select data_obat.nama as nama_obat, stok_apotekers.stok from stok_apotekers join data_obat on data_obat.id = stok_apotekers.obat_id where stok_apotekers.user_id = " . Auth()->user()->id . " and stok_apotekers.deleted_at is null and LEFT(stok_apotekers.updated_at, 10) >= " . $request->tanggal_awal . " and LEFT(stok_apotekers.updated_at, 10) <= " . $request->tanggal_akhir);
            } else {
                // $obat = StokApotekersModel::where('user_id', Auth()->user()->id)->with('dataObat')->get();
                $obat = DB::select("select data_obat.nama as nama_obat, stok_apotekers.stok from stok_apotekers join data_obat on data_obat.id = stok_apotekers.obat_id where stok_apotekers.user_id = " . Auth()->user()->id . " and stok_apotekers.deleted_at is null");
            }
            return DataTables::of($obat)
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function listDistribusi(Request $request)
    {
        if ($request->ajax()) {
            $obatDistribusi = DistribusiApotekersModel::with('dataObat')->get();
            return DataTables::of($obatDistribusi)
                ->addIndexColumn()
                ->make(true);
        }

        $obat = StokApotekersModel::with('dataObat')->get();
        return view('apotekers.distribusi', compact('obat'));
    }
}
