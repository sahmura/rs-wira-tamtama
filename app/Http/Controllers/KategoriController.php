<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use Yajra\DataTables\Facades\DataTables;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($pbf);
        if ($request->ajax()) {
            $kategori = Kategori::get();
            return DataTables::of($kategori)
                ->addColumn('action', function ($kategori) {
                    $button = "<div class='btn-group'>";
                    $button .= '<button type="button" class="btn btn-warning btn-sm btn-edit" id="' . $kategori->id . '" data-nama="' . $kategori->nama . '" data-toggle="tooltip" data-placement="bottom" title="Sunting data"><i class="fa fa-pencil-square-o"></i></button>';
                    $button .= '<button data-token="' . csrf_token() . '" data-id="' . $kategori->id . '"  class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus data"><i class="fa fa-trash"></i></button></div>';

                    return $button;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('pages.kategori.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = Kategori::where('nama', '=', $request->nama)->count();
        if ($check == 0) {
            $kategori = new Kategori();
            $kategori->nama = $request->nama;
            $kategori->save();
            $response = [
                'status' => true,
                'message' => 'Berhasil menambahkan data'
            ];
        } elseif ($check == 1) {
            $response = [
                'status' => false,
                'message' => 'Data sudah tersedia'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Gagal menambahkan data'
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Kategori::where('nama', '=', $request->nama)->count();
        if ($check == 0) {
            $kategori = Kategori::find($id);

            if ($kategori->nama == $request->nama) {
                $response = [
                    'status' => false,
                    'message' => 'Data sudah tersedia'
                ];
            } elseif ($kategori->nama != $request->nama) {
                $kategori->nama = $request->nama;
                $kategori->save();
                $response = [
                    'status' => true,
                    'message' => 'Data berhasil diubah'
                ];
            } else {
                $response = [
                    'status' => false,
                    'message' => 'Data gagal diubah'
                ];
            }
        } else {
            $response = [
                'status' => false,
                'message' => 'Data sudah tersedia'
            ];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::find($id);
        $kategori->delete();
        return response()->json(['success' => 'Data berhasil dihapus']);
    }
}
