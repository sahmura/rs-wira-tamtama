<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dataObat;
use App\Pbf;
use App\SatuanJenis;
use App\Kategori;
use Yajra\DataTables\Facades\DataTables;
use DB;

class dataObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($pbf);
        $pbf = Pbf::all();
        $satuanjenis = SatuanJenis::all();
        $kategori = Kategori::all();
        if ($request->ajax()) {
            // $dataObat = dataObat::orderBy('id', 'DESC')->with('stok')->get();
            $dataObat = DB::select("select data_obat.id, data_obat.pbf, data_obat.nama, data_obat.kategori, stok.stok from data_obat join stok on data_obat.id = stok.obat_id where data_obat.deleted_at is null");
            return DataTables::of($dataObat)
                ->addColumn(
                    'action',
                    function ($dataObat) {
                        $button = "<div class='btn-group'>";
                        $button .= "<a href='" . url('dataobat/' . $dataObat->id) . "' class='btn-sm btn btn-info'><i class='fa fa-search'></i></a>";
                        $button .= '<button type="button" class="btn btn-warning btn-sm btn-edit" id="' . $dataObat->id . '" data-nama="' . $dataObat->nama . '" data-pbf="' . $dataObat->pbf . '" data-kategori="' . $dataObat->kategori . '" data-toggle="tooltip" data-placement="bottom" title="Sunting data"><i class="fa fa-pencil-square-o"></i></button>';
                        $button .= '<button data-token="' . csrf_token() . '" data-id="' . $dataObat->id . '"  class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus data"><i class="fa fa-trash-o"></i></button>';
                        $button .= "</div>";

                        return $button;
                    }
                )
                ->addColumn(
                    'ketersediaan',
                    function ($dataObat) {
                        return $dataObat->stok;
                    }
                )
                ->addIndexColumn()
                ->rawColumns(['action', 'ketersediaan'])
                ->make(true);
        }
        return view('pages.dataobat.index', compact('pbf', 'satuanjenis', 'kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = dataObat::where('nama', '=', $request->nama)->count();
        if ($check == 0) {
            $dataObat = new dataObat();
            $dataObat->nama = $request->nama;
            $dataObat->pbf = $request->pbf;
            $dataObat->kategori = $request->kategori;
            $dataObat->save();
            $response = [
                'status' => true,
                'message' => 'Berhasil menambahkan data'
            ];
        } elseif ($check == 1) {
            $response = [
                'status' => false,
                'message' => 'Data sudah tersedia'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Gagal menambahkan data'
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = dataObat::where('nama', '=', $request->nama)->whereNotIn('id', [$id])->count();
        if ($check == 0) {
            $dataObat = dataObat::find($id);
            $dataObat->nama = $request->nama;
            $dataObat->pbf = $request->pbf;
            $dataObat->kategori = $request->kategori;
            $dataObat->save();
            $response = [
                'status' => true,
                'message' => 'Data berhasil diubah'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Data sudah tersedia'
            ];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataObat = dataObat::find($id);
        $dataObat->delete();
        return response()->json(['success' => 'Data berhasil dihapus']);
    }
}
