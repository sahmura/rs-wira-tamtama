<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\dataObat;
use App\GroupObat;
use Yajra\DataTables\Facades\DataTables;
use DB;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Group::all();
            return DataTables::of($data)
                ->addColumn(
                    'name',
                    function ($data) {
                        return $data->name;
                    }
                )
                ->addColumn(
                    'action',
                    function ($data) {
                        return "<div class='btn-group'>
                            <button class='btn btn-xs btn-success btn-detail'><i class='fa fa-search'></i></button>
                            <button class='btn btn-xs btn-warning btn-edit' data-id='$data->id'><i class='fa fa-pencil-square-o'></i></button>
                            <button class='btn btn-xs btn-danger btn-delete' data-id='$data->id'><i class='fa fa-trash-o'></i></button>
                        </div>";
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
        return view('group.index');
    }

    public function add()
    {
        $obat = dataObat::all();
        return view('group.add', compact('obat'));
    }

    public function edit($id)
    {
        $data = Group::where('id', $id)->where('deleted_at', null)->with('dataObat')->first();
        $obat = dataObat::all();
        return view('group.edit', compact('data', 'obat'));
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $data = Group::where('id', $request->id)->with('dataObat')->first();
            $group = $data->dataObat;
            return DataTables::of($group)
                ->addColumn(
                    'nama',
                    function ($data) {
                        return $group->nama;
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function addData(Request $request)
    {
        try {
            DB::beginTransaction();
            $group = Group::create(
                [
                    'name' => $request->name
                ]
            );
            for ($index = 0; $index < count($request->obat); $index++) {
                GroupObat::create(
                    [
                        'group_id' => $group->id,
                        'obat_id' => $request->obat[$index],
                    ]
                );
            }
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil menambahkan data',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal menambahkan data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function editData(Request $request)
    {
        try {
            DB::beginTransaction();

            if (Group::whereNotIn('id', [$request->id])->where('name', $request->name)->count() != 0) {
                $response = [
                    'title' => 'Group sudah tersedia',
                    'text' => 'Gunakan nama lain',
                    'icon' => 'error'
                ];

                return response()->json($response);
            }


            $group = Group::where('id', $request->id)->update(
                [
                    'name' => $request->name
                ]
            );

            $deleteData = GroupObat::where('group_id', $request->id)->delete();

            for ($index = 0; $index < count($request->obat); $index++) {
                GroupObat::create(
                    [
                        'group_id' => $request->id,
                        'obat_id' => $request->obat[$index],
                    ]
                );
            }
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil mengedit data',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal mengedit data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function deleteData(Request $request)
    {
        try {
            DB::beginTransaction();
            Group::where('id', $request->id)->delete();
            GroupObat::where('group_id', $request->id)->delete();
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil menghapus data',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal menghapus data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function getObat(Request $request)
    {
        $data = dataObat::where('nama', 'LIKE', '%' . $request->term . '%')
            ->get();

        $result = [];
        foreach ($data as $res) {
            $result[] = [
                'id' => $res->id,
                'text' => $res->nama
            ];
        }

        return response()->json($result);
    }
}
