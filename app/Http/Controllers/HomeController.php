<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dataObat;
use App\DataPemasukan;
use App\Obat;
use App\Pbf;
use App\Stok;
use App\StokApotekersModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dataObat = dataObat::get()->count();
        $pemasukanObat = DataPemasukan::get()->count();
        $totalPbf = Pbf::get()->count();
        $stokgudang = Stok::sum('stok');
        $stokapotek = StokApotekersModel::sum('stok');
        return view('dashboard', compact('dataObat', 'pemasukanObat', 'totalPbf', 'stokgudang', 'stokapotek'));
    }
}
