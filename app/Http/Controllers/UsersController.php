<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $user = User::whereIn('role', ['admin', 'apoteker', 'ruangoperasi', 'rawatinap'])->get();
            return DataTables::of($user)
                ->addColumn('action', function ($user) {
                    $button = "<div class='btn-group'>";
                    $button .= '<a role="button" href="'.route('users.edit', $user->id).'" class="btn btn-warning btn-sm btn-edit" id="' . $user->id . '" data-nama="' . $user->name . '" data-toggle="tooltip" data-placement="bottom" title="Sunting data"><i class="fa fa-pencil-square-o"></i></a>';
                    $button .= '<button data-token="' . csrf_token() . '" data-id="' . $user->id . '"  class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus data"><i class="fa fa-trash"></i></button></div>';

                    return $button;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.data_users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.register_users');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('users.edit_users', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required',
                'email' => 'required|email',
            ]
        );

        $save = User::find($id)
                ->update(
                    [
                        "name" => $request->name,
                        "email" => $request->email,
                    ]
                );

                return $save ? redirect()->route('users.index')->with('success', 'Berhasil menyunting data user') : redirect()->route('users.index')->with('failed', 'Gagal menyunting data user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(['success' => 'Data berhasil dihapus']);
    }
}
