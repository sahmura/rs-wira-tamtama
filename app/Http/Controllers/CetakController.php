<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Distribusi;
use App\LaporanOperasi;
use App\Requests;
use App\Stok;
use Spipu\Html2Pdf\Html2Pdf;
use Carbon\Carbon;
use DB;

class CetakController extends Controller
{
    public function cetakDistribusi(Request $request)
    {
        // $notin = ($request->notin != '') ? explode(',', $request->notin) : [];
        $notin = $request->notin;
        if ($request->jenis != '') {
            if ($request->jenis == 1) {
                $column = "tanggal_validasi";
            } else {
                $column = "created_at";
            }
            // $data['data'] = Requests::where(DB::raw("LEFT(requests.$column, 10)"), '>=', $request->awal)
            //     ->where(DB::raw("LEFT(requests.$column, 10)"), '<=', $request->akhir)
            //     ->where('is_done', $request->jenis)
            //     ->whereNotIn('id', $notin)
            //     ->with('dataObat.stok')
            //     ->with('user')
            //     ->latest()->get();
            $data['data'] = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where LEFT(requests.$column,10) >= '$request->tanggal_awal' and LEFT(requests.$column,10) <= '$request->tanggal_akhir' and requests.is_done = $request->jenis and requests.deleted_at is null and requests.id not in ($notin) order by requests.id DESC");
        } else {
            // $data['data'] = Requests::with('dataObat.stok')->whereNotIn('id', $notin)->with('user')->latest()->get();
            $data['data'] = DB::select("select data_obat.id as id_obat, requests.total, requests.keterangan, requests.is_done, requests.feedback, requests.id, data_obat.nama as nama_obat, stok.stok as stok_obat,users.name as nama_user,users.id as id_user from requests join users on users.id = requests.user_id join data_obat on data_obat.id = requests.obat_id join stok on stok.obat_id = data_obat.id
                where requests.deleted_at is null and requests.id not in ($notin) order by requests.id DESC");
        }

        if ($request->awal == $request->akhir) {
            $data['data']->tanggalkonfirmasi = Carbon::parse($request->awal)->locale('id')->isoFormat('Do MMMM YYYY');
        } else {
            $data['data']->tanggalkonfirmasi = Carbon::parse($request->awal)->locale('id')->isoFormat('Do MMMM YYYY') . " - " . Carbon::parse($request->akhir)->locale('id')->isoFormat('Do MMMM YYYY');
        }

        $filename = 'Laporan Distribusi _ ' . Carbon::now()->format('d-m-Y') . '.pdf';
        $template = 'laporan_distribusi_pdf';
        $data['data']->printDate = Carbon::now()->format('d-m-Y');
        $data['data']->printedBy = Auth()->user()->name;


        return $this->exportPdf($data, $filename, $template);
    }

    public function cetakLaporanPemakaian(Request $request)
    {
        $detail = LaporanOperasi::where('id', $request->id)->with('dataObats.obatOne')->first();
        $pemakaian = LaporanOperasi::where('id', $request->id)->with('dataObats:laporan_id,obat_id,pemakaian')->first();
        $data['detail'] = $detail;
        $data['data'] = [];
        $i = 0;

        foreach ($detail->dataObats as $obat) {
            $data['data'][$i] = ['id' => $obat->id, 'nama' => $obat->nama, 'harga_satuan_terakhir' => $obat->harga_satuan_terakhir, 'pemakaian' => null];
            foreach ($pemakaian->dataObats as $pakai) {
                if ($obat->id == $pakai->obat_id) {
                    $data['data'][$i]['pemakaian'] = $pakai->pemakaian;
                }
            }
            $i++;
        }

        // $data['data'] = LaporanOperasi::where('id', $request->id)->with('dataObats.obatOne')->first();
        $template = 'laporan_pemakaian_pdf';
        $filename = 'Laporan Pemakaian_' . $detail->name . '_' . now() . '.pdf';
        $data['printDate'] = Carbon::now()->format('d-m-Y');
        $data['printedBy'] = Auth()->user()->name;
        $data['tindakan'] = 'OPERASI';
        return $this->exportPdf($data, $filename, $template);
    }

    public function exportPdf($data, $filename, $template)
    {
        $content        = view('print.' . $template, $data)->render();
        $html2pdf       = new Html2Pdf('P', 'A4', 'en');
        ob_end_clean();
        $html2pdf->writeHTML($content);
        $html2pdf->output($filename);
    }
}
