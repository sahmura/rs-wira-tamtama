<?php

namespace App\Http\Controllers;

use App\DataPemasukan;
use Illuminate\Http\Request;
use App\Obat;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use DB;

class DashboardController extends Controller
{
    public function dataObat(Request $request)
    {
        if ($request->ajax()) {
            $dataObat = DB::select('SELECT data_pemasukan.tanggal_kadaluwarsa, data_obat.nama as nama_obat, satuan_jenis.nama as satuan_nama, satuan_jenis.id as satuan_id, stok.stok FROM `data_pemasukan` join data_obat on data_obat.id = data_pemasukan.obat_id join satuan_jenis on satuan_jenis.id = data_pemasukan.satuan_id join stok on stok.obat_id = data_pemasukan.obat_id where data_pemasukan.deleted_at is null');
            return DataTables::of($dataObat)
                ->addColumn(
                    'tanggal_kadaluwarsa',
                    function ($dataObat) {
                        return Carbon::parse($dataObat->tanggal_kadaluwarsa)->locale('id')->isoFormat('Do MMMM YYYY');
                    }
                )
                ->addColumn(
                    'satuan',
                    function ($dataObat) {
                        return ($dataObat->satuan_id != 0) ? $dataObat->satuan_nama : 'Belum ditentukan';
                    }
                )
                ->addColumn(
                    'kadaluwarsa',
                    function ($dataObat) {
                        return Carbon::parse($dataObat->tanggal_kadaluwarsa)->locale('id')->diffForHumans();
                    }
                )
                ->addColumn(
                    'stok',
                    function ($dataObat) {
                        return ($dataObat->stok == null) ? 'Belum tersedia' : $dataObat->stok;
                    }
                )
                ->rawColumns(['kadaluwarsa'])
                ->addIndexColumn()
                ->make(true);
        }
    }
}
