<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Distribusi extends Model
{
    use SoftDeletes;
    protected $table = 'distribusi';
    protected $fillable = [
        'id', 'user_id', 'obat_id', 'distribusi', 'total', 'keterangan', 'tanggal_request', 'tanggal_validasi'
    ];
    protected $hidden = [
        'created_at',
        'updateed_at',
        'deleted_at',
    ];
    protected $dates = ['deleted_at'];

    public function obat()
    {
        return $this->hasMany('App\Obat', 'id', 'obat_id');
    }

    public function dataObat()
    {
        return $this->belongsTo('App\dataObat', 'obat_id', 'id');
    }
}
