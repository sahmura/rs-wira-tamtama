<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pbf extends Model
{
    use SoftDeletes;
    protected $table = 'pbf';
    protected $fillable = [
        'id', 'nama'
    ];
    protected $hidden = [
        'created_at',
        'updateed_at',
        'deleted_at',
    ];
    protected $dates = ['deleted_at'];
}
