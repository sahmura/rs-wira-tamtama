<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPpnbrutoColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_pemasukan', function (Blueprint $table) {
            if (!Schema::hasColumn('data_pemasukan', 'ppn_netto')) {
                $table->float('ppn_netto');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_pemasukan', function (Blueprint $table) {
            if (Schema::hasColumn('data_pemasukan', 'ppn_netto')) {
                $table->dropColumn('ppn_netto');
            }
        });
    }
}
