<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDokterId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stok_opnames', function (Blueprint $table) {
            if (!Schema::hasColumn('stok_opanames', 'dokter_id')) {
                // $table->bigInteger('dokter_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stok_opnames', function (Blueprint $table) {
            //
        });
    }
}
