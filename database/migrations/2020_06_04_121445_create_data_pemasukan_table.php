<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPemasukanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('data_pemasukan')) {
            Schema::create('data_pemasukan', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('obat_id')->unsigned();
                $table->string('nomor_faktur');
                $table->date('tanggal_pemasukan');
                $table->bigInteger('pbf_id');
                $table->date('tanggal_kadaluwarsa');
                $table->integer('jumlah_kemasan');
                $table->integer('jumlah_satuan');
                $table->decimal('netto');
                $table->float('diskon');
                $table->decimal('harga_akhir_manual');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pemasukan');
    }
}
