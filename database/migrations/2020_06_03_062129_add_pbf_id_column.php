<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPbfIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('obat', function (Blueprint $table) {
            if (!Schema::hasColumn('obat', 'pbf_id')) {
                $table->bigInteger('pbf_id')->unsigned();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obat', function (Blueprint $table) {
            if (Schema::hasColumn('obat', 'pbf_id')) {
                $table->dropColumn('pbf_id');
            }
        });
    }
}
