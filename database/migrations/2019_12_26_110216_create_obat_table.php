<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('pbf', 250);
            $table->string('nomor_faktur', 250);
            $table->text('nama');
            $table->dateTime('tanggal_kadaluwarsa');
            $table->string('kategori', 250);
            $table->integer('jumlah_kemasan');
            $table->integer('jumlah_satuan');
            $table->integer('jumlah_obat');
            $table->string('satuan', 250);
            $table->integer('bruto');
            $table->integer('ppn_bruto');
            $table->integer('total_bruto');
            $table->integer('bruto_fix');
            $table->integer('diskon');
            $table->integer('total_diskon');
            $table->integer('harga_diskon');
            $table->integer('harga_satuan');
            $table->integer('ppn_harga');
            $table->integer('harga');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obat');
    }
}
