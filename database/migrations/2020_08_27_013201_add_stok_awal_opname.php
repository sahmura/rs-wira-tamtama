<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStokAwalOpname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stok_opnames', function (Blueprint $table) {
            if (!Schema::hasColumn('stok_opnames', 'stok_awal')) {
                $table->bigInteger('stok_awal');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stok_opnames', function (Blueprint $table) {
            if (Schema::hasColumn('stok_opnames', 'stok_awal')) {
                $table->dropColumn('stok_awal');
            }
        });
    }
}
