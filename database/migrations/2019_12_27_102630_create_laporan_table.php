<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('obat_id')->unsigned();
            $table->integer('persediaan_awal');
            $table->integer('penerimaan');
            $table->integer('nominal_awal');
            $table->integer('nominal_penerimaan');
            $table->integer('nominal_pemakaian');
            $table->integer('nominal_persediaan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan');
    }
}
