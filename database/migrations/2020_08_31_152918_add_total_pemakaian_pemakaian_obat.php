<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalPemakaianPemakaianObat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporanoperasi_obat', function (Blueprint $table) {
            if (!Schema::hasColumn('laporanoperasi_obat', 'pemakaian')) {
                $table->bigInteger('pemakaian')->nullable()->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporanoperasi_obat', function (Blueprint $table) {
            $table->dropColumn('pemakaian');
        });
    }
}
