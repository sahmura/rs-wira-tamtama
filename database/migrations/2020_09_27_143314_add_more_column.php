<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporan_operasis', function (Blueprint $table) {
            $table->bigInteger('dokter_id_bedah')->nullable();
            $table->bigInteger('dokter_id_anestesi')->nullable();
            $table->string('perawat_bedah')->nullable();
            $table->string('perawat_anestesi')->nullable();
            $table->string('sewa_alat')->nullable();
            $table->string('sewa_ok')->nullable();
            $table->bigInteger('biaya_lain')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->date('tanggal_lahir')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan_operasis', function (Blueprint $table) {
            $table->dropColumn('dokter_id_bedah');
            $table->dropColumn('dokter_id_anestesi');
            $table->dropColumn('perawat_bedah');
            $table->dropColumn('perawat_anestesi');
            $table->dropColumn('sewa_alat');
            $table->dropColumn('sewa_ok');
            $table->dropColumn('biaya_lain');
            $table->dropColumn('jenis_kelamin');
            $table->dropColumn('tanggal_lahir');
        });
    }
}
