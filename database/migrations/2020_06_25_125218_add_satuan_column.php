<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSatuanColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_pemasukan', function (Blueprint $table) {
            if (!Schema::hasColumn('data_pemasukan', 'satuan_id')) {
                $table->bigInteger('satuan_id')->unsigned();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_pemasukan', function (Blueprint $table) {
            if (Schema::hasColumn('data_pemasukan', 'satuan_id')) {
                $table->dropColumn('satuan_id');
            }
        });
    }
}
