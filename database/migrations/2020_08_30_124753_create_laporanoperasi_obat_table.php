<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporanoperasiObatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('laporanoperasi_obat')) {
            Schema::create('laporanoperasi_obat', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('laporan_id')->unsigned();
                $table->bigInteger('obat_id')->unsigned();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporanoperasi_obat');
    }
}
