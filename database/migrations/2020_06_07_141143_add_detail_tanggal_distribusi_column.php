<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailTanggalDistribusiColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('distribusi', function (Blueprint $table) {
            if (!Schema::hasColumn('distribusi', 'tanggal_request')) {
                $table->date('tanggal_request')->nullable();
            }
            if (!Schema::hasColumn('distribusi', 'tanggal_validasi')) {
                $table->date('tanggal_validasi')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('distribusi', function (Blueprint $table) {
            if (Schema::hasColumn('distribusi', 'tanggal_request')) {
                $table->dropColumn('tanggal_request');
            }
            if (Schema::hasColumn('distribusi', 'tanggal_validasi')) {
                $table->dropColumn('tanggal_validasi');
            }
        });
    }
}
