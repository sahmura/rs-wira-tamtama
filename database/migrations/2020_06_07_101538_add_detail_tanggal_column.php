<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailTanggalColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            if (!Schema::hasColumn('requests', 'tanggal_request')) {
                $table->date('tanggal_request');
            }
            if (!Schema::hasColumn('requests', 'tanggal_validasi')) {
                $table->date('tanggal_validasi');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            if (Schema::hasColumn('requests', 'tanggal_request')) {
                $table->dropColumn('tanggal_request');
            }
            if (Schema::hasColumn('requests', 'tanggal_validasi')) {
                $table->dropColumn('tanggal_validasi');
            }
        });
    }
}
