<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporanOperasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('laporan_operasis')) {
            Schema::create('laporan_operasis', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('nama')->nullable();
                $table->string('status')->nullable();
                $table->string('pangkat_pekerjaan')->nullable();
                $table->text('alamat')->nullable();
                $table->string('macam_operasi')->nullable();
                $table->string('golongan_operasi')->nullable();
                $table->date('tanggal_operasi')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_operasis');
    }
}
