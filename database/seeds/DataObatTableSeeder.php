<?php

use Illuminate\Database\Seeder;

class DataObatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\dataObat::insert([
            [
                'nama' => 'Obat Batuk',
                'pbf' => 'PT Jaya Farma',
                'kategori' => 'Obat Basah',
            ],
        ]);
    }
}
