<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Kategori::insert([
            [
                'nama' => 'Alkes',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'Hemodialisa',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'Laborat',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'Obat Basah',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'Obat Kering',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'Radiologi',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ]
        ]);
    }
}
