<?php

use Illuminate\Database\Seeder;

class PbfTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Pbf::insert([
            [
                'nama' => 'PT Jaya Abadi',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'PT Jaya Farma',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'PT Kimia Farma',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ]
        ]);
    }
}
