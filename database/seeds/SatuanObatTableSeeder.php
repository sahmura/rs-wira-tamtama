<?php

use Illuminate\Database\Seeder;

class SatuanObatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\SatuanJenis::insert([
            [
                'nama' => 'Kapsul',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'Pill',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'nama' => 'Tablet',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ]
        ]);
    }
}
