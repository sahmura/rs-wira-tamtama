<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert([
            [
                'role' => 'admin',
                'name' => 'Admin RS Bhakti Wiratamtama',
                'email' => 'admin@rsbhaktiwiratamtama.my.id',
                'password' => Hash::make('adminrs123'),
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'role' => 'apoteker',
                'name' => 'Apoteker RS Bhakti Wiratamtama',
                'email' => 'apoteker@rsbhaktiwiratamtama.my.id',
                'password' => Hash::make('apoteker2020'),
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'role' => 'system',
                'name' => 'System RSBW',
                'email' => 'system@rsbhaktiwiratamtama.my.id',
                'password' => Hash::make('system2020'),
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ]
        ]);
    }
}
