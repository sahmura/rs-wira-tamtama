(function ($) {
    //    "use strict";


    /*  Data Table
    -------------*/

    $('#pbfTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('pbf.listPbf') }}",
            method: 'POST',
        },
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'nama',
                name: 'nama'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'action',
            }
        ]
    });

})(jQuery);
